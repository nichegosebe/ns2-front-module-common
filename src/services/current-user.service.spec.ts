import {async, inject, TestBed} from "@angular/core/testing";
import {HttpModule, RequestOptions, Response, ResponseOptions} from "@angular/http";
import {HttpService} from "./http.service";
import {Injector} from "@angular/core";
import {MockBackend} from "@angular/http/testing";
import {CurrentUserService, STORAGE_TOKEN_KEY} from "./current-user.service";
import {LocalStorageService} from "./local-storage.service";
import {SessionStorageService} from "./session-storage.service";

describe('CurrentUserService', () => {

  beforeEach(async(() => {

    sessionStorage.clear();
    localStorage.clear();

    TestBed.configureTestingModule({
      imports: [HttpModule],
      providers: [
        {
          provide: HttpService,
          useFactory: (injector: Injector, mockBackend: MockBackend, requestOptions: RequestOptions) => {
            return new HttpService(injector, mockBackend, requestOptions);
          },
          deps: [Injector, MockBackend, RequestOptions]
        },
        MockBackend,
        LocalStorageService,
        SessionStorageService,
        {
          provide: CurrentUserService,
          useFactory: (injector: Injector, localStorage: LocalStorageService, sessionStorage: SessionStorageService) => {
            return new CurrentUserService(injector, localStorage, sessionStorage);
          },
          deps: [Injector, LocalStorageService, SessionStorageService]
        }
      ]
    });

  }));

  it('isAuth() true',
    inject([CurrentUserService], (currentUser) => {
      currentUser.setToken('test');

      expect(currentUser.isAuth()).toBeTruthy();
    })
  );

  it('isAuth() false',
    inject([CurrentUserService], (currentUser) => {
      expect(currentUser.isAuth()).toBeFalsy();
    })
  );

  it('login() with remember me',
    inject([CurrentUserService, MockBackend, LocalStorageService], (currentUser, mockBackend, persistStorage) => {
      const body = {
        login: 'test@example.com',
        password: 'test'
      };

      const response = {
        success: true,
        data: 'Token Here'
      };

      mockBackend.connections.subscribe((connection) => {
        connection.mockRespond(new Response(new ResponseOptions({
          body: JSON.stringify(response)
        })));
      });

      currentUser.login(body.login, body.password, true)
        .then(() => {
          expect(currentUser.getToken()).toEqual(response.data);
          persistStorage.get(STORAGE_TOKEN_KEY)
            .then((token) => {
              expect(token).toEqual(response.data);
            })
            .catch(() => {});
        });
    })
  );

  it('login() without rememberMe',
    inject([CurrentUserService, MockBackend, SessionStorageService], (currentUser, mockBackend, tempStorage) => {
      const body = {
        login: 'test@example.com',
        password: 'test'
      };

      const response = {
        success: true,
        data: 'Token Here'
      };

      mockBackend.connections.subscribe((connection) => {
        connection.mockRespond(new Response(new ResponseOptions({
          body: JSON.stringify(response)
        })));
      });

      currentUser.login(body.login, body.password, false)
        .then(() => {
          expect(currentUser.getToken()).toEqual(response.data);
          tempStorage.get(STORAGE_TOKEN_KEY)
            .then((token) => {
              expect(token).toEqual(response.data);
            })
            .catch(() => {});
        });
    })
  );

  it('register()',
    inject([CurrentUserService, MockBackend], (currentUser, mockBackend) => {
      const requestData = {
        email: 'homer@fox.com',
        password: 'hompass',
        password_repeat: 'homepass',
        firstname: 'Homer',
        lastname: 'Simpson',
        geo_guid: '842732a8-3da6-4c99-a780-a56b615b4c72',
        phone: '89992324343'
      };

      const responseData = {
        success: true,
        data: requestData
      };

      mockBackend.connections.subscribe((connection) => {
        connection.mockRespond(new Response(new ResponseOptions({
          body: JSON.stringify(responseData)
        })));
      });

      currentUser.register(
        requestData.email,
        requestData.password,
        requestData.password_repeat,
        requestData.firstname,
        requestData.lastname,
        requestData.geo_guid,
        requestData.phone
      )
        .then((data) => {
          expect(data).toEqual(responseData.data);
        });
    })
  );

  it('registerCheckEmail()',
    inject([CurrentUserService, MockBackend], (currentUser, mockBackend) => {
      const requestData = {
        email: 'test@example.com'
      };

      const responseData = {
        success: true,
        data: requestData.email
      };

      mockBackend.connections.subscribe((connection) => {
        connection.mockRespond(new Response(new ResponseOptions({
          body: JSON.stringify(responseData)
        })));
      });

      currentUser.registerCheckEmail(requestData.email)
        .then((validEmail: string) => {
          expect(validEmail).toEqual(responseData.data)
        });
    })
  );

  it('registerChangePhone()',
    inject([CurrentUserService, MockBackend], (currentUser, mockBackend) => {
      const requestData = {
        user_guid: 'f8ef1a68-6983-4955-9f6d-a642f6cefb13',
        phone: '89384345511'
      };

      const responseData = {
        success: true,
        data: requestData.phone
      };

      mockBackend.connections.subscribe((connection) => {
        expect(JSON.parse(connection.request.getBody())).toEqual(requestData);

        connection.mockRespond(new Response(new ResponseOptions({
          body: JSON.stringify(responseData)
        })));
      });

      currentUser.registerChangePhone(requestData.user_guid, requestData.phone)
        .then((newPhone: string) => {
          expect(newPhone).toEqual(requestData.phone);
        });
    })
  );

  it('confirm()',
    inject([CurrentUserService, MockBackend], (currentUser, mockBackend) => {
      const requestData = {
        user_guid: 'f8ef1a68-6983-4955-9f6d-a642f6cefb13',
        code: '987324'
      };

      const responseData = {
        success: true,
        data: {
          email: 'homer@fox.com',
          password: 'hompass',
          password_repeat: 'homepass',
          firstname: 'Homer',
          lastname: 'Simpson',
          geo_guid: '842732a8-3da6-4c99-a780-a56b615b4c72',
          phone: '89992324343'
        }
      };

      mockBackend.connections.subscribe((connection) => {
        expect(JSON.parse(connection.request.getBody())).toEqual(requestData);

        connection.mockRespond(new Response(new ResponseOptions({
          body: JSON.stringify(responseData)
        })));
      });

      currentUser.confirm(requestData.user_guid, requestData.code)
        .then((data) => {
          expect(data).toEqual(responseData.data);
        });
    })
  );

  it('reminderPassword()',
    inject([CurrentUserService, MockBackend], (currentUser, mockBackend) => {
      const requestData = {
        email: 'test@example.com'
      };

      const responseData = {
        success: true,
        data: {
          email: 'tester@example.com',
          firstname: 'Tester',
          lastname: 'Test'
        }
      };

      mockBackend.connections.subscribe((connection) => {
        expect(JSON.parse(connection.request.getBody())).toEqual(requestData);

        connection.mockRespond(new Response(new ResponseOptions({
          body: JSON.stringify(responseData)
        })));
      });

      currentUser.reminderPassword(requestData.email)
        .then((data) => {
          expect(data).toEqual(responseData.data);
        });
    })
  );

  it('restorePasswordCheck()',
    inject([CurrentUserService, MockBackend], (currentUser, mockBackend) => {
      const requestData = {
        token: '16adec91-5fb6-4b04-a954-c44e200984fb'
      };

      const responseData = {
        success: true,
        data: {
          email: 'tester@example.com',
          firstname: 'Tester',
          lastname: 'Test'
        }
      };

      mockBackend.connections.subscribe((connection) => {
        expect(connection.request.url).toEqual('undefinedusers/restore-password-check?token=' + requestData.token);

        connection.mockRespond(new Response(new ResponseOptions({
          body: JSON.stringify(responseData)
        })));
      });

      currentUser.restorePasswordCheck(requestData.token)
        .then((data) => {
          expect(data).toEqual(responseData.data);
        });
    })
  );

  it('restorePassword()',
    inject([CurrentUserService, MockBackend], (currentUser, mockBackend) => {
      const requestData = {
        token: '16adec91-5fb6-4b04-a954-c44e200984fb',
        password: '123',
        password_repeat: '123'
      };

      const responseData = {
        success: true,
        data: {
          email: 'tester@example.com',
          firstname: 'Tester',
          lastname: 'Test'
        }
      };

      mockBackend.connections.subscribe((connection) => {
        expect(JSON.parse(connection.request.getBody())).toEqual(requestData);

        connection.mockRespond(new Response(new ResponseOptions({
          body: JSON.stringify(responseData)
        })));
      });

      currentUser.restorePassword(requestData.token, requestData.password, requestData.password_repeat)
        .then((data) => {
          expect(data).toEqual(responseData.data);
        });
    })
  );

  it('setGeo()',
    inject([CurrentUserService, MockBackend], (currentUser, mockBackend) => {
      const requestData = {
        geo_guid: '16adec91-5fb6-4b04-a954-c44e200984fb'
      };

      const responseData = {
        success: true,
        data: requestData
      };

      mockBackend.connections.subscribe((connection) => {
        expect(JSON.parse(connection.request.getBody())).toEqual(requestData);

        connection.mockRespond(new Response(new ResponseOptions({
          body: JSON.stringify(responseData)
        })));
      });

      currentUser.setGeo(requestData.geo_guid)
        .then((data) => {
          expect(data).toEqual(responseData.data);
        });
    })
  );
});