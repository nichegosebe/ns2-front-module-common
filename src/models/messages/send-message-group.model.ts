import {MESSAGE_RCPT_GROUP} from "./abstract/const";
import {AbstractSendMessage} from "./abstract/send/abstract-send-message.model";

/**
 * Модель отправки сообщения в группу
 */
export class SendMessageGroup extends AbstractSendMessage {
    constructor() {
        super();
        this.header.rcpt = MESSAGE_RCPT_GROUP;
    }
}