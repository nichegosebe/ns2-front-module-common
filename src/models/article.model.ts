/**
 * Модель описывающая ответ от сервера при получении записи (артикла)
 */
export class ArticleModel {
    guid: string;               // Идентификатор топика
    created: string;            // Дата/Время создания
    updated: string;            // Дата/Время обновления
    creator_guid: string;       // Идентификатор создателя
    updater_guid: string;       // Идентификатор последнего редактора
    params: any;                // Дополнительные парамтеры
    name: string;               // Имя (латиницей) записи
    title: string;              // Заголовок топика
    pid: string;                // Идентификатор родительской записи
    trail: string;              // Путь к записи по дереву
    level: number;              // Уровень вложенности записи
    content: string;            // Тело записи
    abstract: string;           // Краткое описание записи
    sorder: number;             // Порядковый номер
    childs?: ArticleModel[];    // Вложенные записи
}
