/**
 * Модель найденного сообщения
 */
export class MessageSearchResultItemModel {
    guid: string;           // Идентификатор сообщения
    created: string;        // Дата/время создания сообщения
    highlight: string;      // Текст сообщения с подсветкой
}
