import {IRealtyFilter} from "../intefraces/realty-filter.interface";

export class RealtyFilterOperation implements IRealtyFilter {
    private operation: string;
    private field: string;
    private value: any;

    constructor(operation: string, field: string, value: any) {
        this.operation = operation;
        this.field = field;
        this.value = value;
    }

    getRequest(): any {
        return {
            operation: this.operation,
            field: this.field,
            value: this.value,
        };
    }
}