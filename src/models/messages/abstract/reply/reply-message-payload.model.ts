import {MessagePayload} from "../messages-payload.model";

/**
 * Модель полезной нагрузки сообщения: ответ на сообщение
 */
export class ReplyMessagePayload extends MessagePayload {
    message: {
        guid?: string,
        text: string,
        firstname: string,
        lastname: string,
        created?: number,
        updated?: number
    };
    replyOn: {
        guid: string,
        firstname: string,
        lastname: string,
        text?: string,
        realty?: string[],
    }
}