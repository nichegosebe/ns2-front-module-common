export class ResponseListModel<T> {
    page: number;
    pageSize: number;
    pages: number;
    totalCount: number;
    items: Array<T>;
}
