import {MessagePayload} from "../messages-payload.model";

/**
 * Модель полезной нагрузки сообщения: отправка сообщения
 */
export class SendMessagePayload extends MessagePayload {
    message: {
        guid?: string,
        created?: number,
        updated?: number,
        text: string,
        firstname: string,
        lastname: string,
        realty?: string[]
    };
}