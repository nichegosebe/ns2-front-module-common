/**
 * Модель полезной нагрузки сообщения
 */
export abstract class MessagePayload {
    from: string;
    to: string;
}