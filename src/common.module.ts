import {Injector, NgModule} from '@angular/core';
import {HttpModule, RequestOptions, XHRBackend} from '@angular/http';
import {HttpService, httpFactory} from './services/http.service';
import {WebsocketService} from './services/websocket.service';
import {HttpErrorService} from './services/http-error.service';
import {HelperService} from './services/helper.service';
import {CookieService} from 'ngx-cookie-service';

@NgModule({
    declarations: [],
    imports: [
        HttpModule
    ],
    providers: [
        WebsocketService,
        HttpErrorService,
        HelperService,
        CookieService,
        {
            provide: HttpService,
            useFactory: httpFactory,
            deps: [Injector, XHRBackend, RequestOptions]
        }
    ]
})
export class CommonModule {

}
