// Базовый URL
import { BasicUrls } from './basic-urls.model';

// URL's для работы с каналами
// @dynamic
export class ChannelsUrls extends BasicUrls {
	public static get chatInvite(): string {
		return `${ChannelsUrls.endPoint}/messaging/channels/invite`;
    }
}
