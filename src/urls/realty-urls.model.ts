import { BasicUrls } from './basic-urls.model';


/** GW для работы с объявлениями */
// @dynamic
export class RealtyUrls extends BasicUrls {

    /** Базовый URL */
    private static get baseUrl(): string {
        return `${RealtyUrls.endPoint}/realty`;
    }

    /** Список объявлений */
    public static get filter(): string {
        return `${RealtyUrls.baseUrl}/filter`;
    }

    /** Список избранных объявлений */
    public static get favorites(): string {
        return `${RealtyUrls.baseUrl}/favorites`;
    }

    /** Список моих объявлений */
    public static get myList(): string {
        return `${RealtyUrls.baseUrl}/get-my-realty`;
    }

    // Список всех динамических фильтров объявлений
    public static get getFiltersFullMeta(): string {
        return `${RealtyUrls.endPoint}/realty-filter/full-meta-info`;
    }

    // Метадата по объявлениям
    public static get metaFull(): string {
        return `${RealtyUrls.endPoint}/realty-form/full`;
    }

    /**
	 * Создание объявления
	 * @param actionGuid Идентификатор действия над объявления
	 * @param entityGuid Идентификатор типа недвижимости объявления
	 */
    public static create(actionGuid: string, entityGuid: string): string {
        return `${RealtyUrls.baseUrl}/${actionGuid}/${entityGuid}`;
    }

    /**
	 * Редактирование объявления
	 * @param actionGuid Идентификатор действия над объявления
	 * @param entityGuid Идентификатор типа недвижимости объявления
	 * @param realtyGuid Идентификатор объявления
	 */
    public static update(actionGuid: string, entityGuid: string, realtyGuid: string): string {
        return `${RealtyUrls.baseUrl}/${actionGuid}/${entityGuid}/${realtyGuid}`;
    }

    /**
	 * Публикация объявления текущего пользователя
	 * @param actionGuid Идентификатор действия над объявления
	 * @param entityGuid Идентификатор типа недвижимости объявления
	 * @param realtyGuid Идентификатор объявления
	 */
    public static publish(actionGuid: string, entityGuid: string, realtyGuid: string): string {
        return `${RealtyUrls.baseUrl}/${actionGuid}/${entityGuid}/${realtyGuid}/publish`;
    }

    /**
	 * Удаление объявления текущего пользователя
	 * @param actionGuid Идентификатор действия над объявления
	 * @param entityGuid Идентификатор типа недвижимости объявления
	 * @param realtyGuid Идентификатор объявления
	 */
    public static remove(actionGuid: string, entityGuid: string, realtyGuid: string): string {
        return `${RealtyUrls.baseUrl}/${actionGuid}/${entityGuid}/${realtyGuid}`;
    }

    /**
	 * Добавление/удаление из избранного
	 * @param realtyGuid GUID объявления
	 */
    public static favorite(realtyGuid: string): string {
        return `${RealtyUrls.baseUrl}/${realtyGuid}/favorites`;
    }

    /**
	 * Подробная информация об объявлении
	 * @param realtyGuid GUID объявления
	 */
    public static about(realtyGuid: string): string {
        return `${RealtyUrls.baseUrl}/${realtyGuid}`;
    }

    // Изменение справочников для формы создания/редактирования
    public static get getFormLastModified(): string {
        return `${RealtyUrls.endPoint}/realty-form/last-modified`;
    }

    // Получение объявлений по гуидам
    public static get getByGuids(): string {
        return `${RealtyUrls.endPoint}/get-by-guids`;
    }

    // Публикация объявления на продающем сайте
    public static salePublish(actionGuid: string, entityGuid: string, guid: string): string {
        return `${RealtyUrls.baseUrl}/${actionGuid}/${entityGuid}/${guid}/sale-publish`;
    }

    // Снятие публикации объявления на продающем сайте
    public static saleUnPublish(actionGuid: string, entityGuid: string, guid: string): string {
        return `${RealtyUrls.baseUrl}/${actionGuid}/${entityGuid}/${guid}/sale-unpublish`;
    }

    // Отмена модерации объявления на продающем сайте
    public static cancelSaleModeration(actionGuid: string, entityGuid: string, guid: string): string {
        return `${RealtyUrls.baseUrl}/${actionGuid}/${entityGuid}/${guid}/sale-cancel`;
    }
}
