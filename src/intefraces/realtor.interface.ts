import { PhoneInterface } from "./phone.interface";
import { SocialModel } from '../models/social.model';

export interface RealtorInterface {
    // GUID
    guid: string;

    // Имя
    firstname: string;

    // Фамилия
    lastname: string;

    // Email
    email: string;

    // Опыт работы
    experience: number;

    // GUID аватара
    avatar_guid: string;

    // GUID гео-расположения
    geo_guid: string;

    // О себе
    description: string;

    // Статус онлайна
    is_online: boolean;

    // Список номеров телефона
    phones: Array<PhoneInterface>;

    // Специализации
    specializations: string[];

    // Районы, в которых работает риелтор
    districts: string[];

    // Является ли риелтор партнером текущего пользователя
    is_partner?: boolean;

    realty_count: any;

    // Работает в агенстве
    is_agency: boolean;

    // Должность
    agency_position: string;

    // Название агенства
    agency_title: string;

    // Может работать в другом городе
    can_work_other_city: boolean;

    // День рождения
    birthday: string | number;

    // Личный сайт
    site_url: string;

    // Список социальных сетей
    socials: SocialModel[];

    // Ранг на английском
    rank: string;

    // Ранг на русском
    rank_title: string;

    // Информация для покупателя
    customer_info: string;

    // На модерации
    on_moderate: boolean;
}
