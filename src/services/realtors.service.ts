import {Response, URLSearchParams} from '@angular/http';
import {Injectable} from '@angular/core';
import {RealtorModel} from '../models/realtor.model';
import {HttpService} from './http.service';
import {ResponseListModel} from '../models/response-list.model';
import {RealtorInterface} from '../intefraces/realtor.interface';
import {RealtorsFilterInterface} from '../intefraces/realtors-filter.interface';
import { RealtorsUrls } from '../urls/realtors-urls.model';

/**
 * Сервис риэлтеров
 */
@Injectable()
export class RealtorsService {
    public static ESB_REALTORS_CHANNEL_NAME = 'realtors';
    public static ESB_REALTORS_SHOW_INFO = 'info.show';

    /**
     * Base URL (точка входа) в микросервис пользователей (риэлтеров)
     */
    private usersEndPoint: string;

    constructor(private httpService: HttpService) {
    }

    /**
     * Поиск риэлтеров
     *
     * @param query строка поиска
     * @param params дополнительные параметры для запроса
     */
    public search(query: string, params: any): Promise<ResponseListModel<RealtorModel>> {
        return new Promise((resolve, reject) => {
            let options = {search: new URLSearchParams()};

            Object.keys(params).forEach((key: any) => {
                options.search.set(key, params[key]);
            });

            options.search.set('query', query);

            this.httpService.get(RealtorsUrls.search, options)
                .map((response: Response) => response.json())
                .map((response: any) => response.data)
                .subscribe(resolve, reject);
        })
    }

    /**
     * Получить список всех риелторов пользователя
     *
     * @param userGuid
     * @returns {Promise<T>}
     */
    public getPartners(userGuid: string): Promise<ResponseListModel<RealtorInterface>> {
        return new Promise((resolve, reject) => {
            const url = this.usersEndPoint + 'users/' + userGuid + '/partners';

            this.httpService.get(url)
                .map((response: Response) => response.json())
                .map((response: any) => response.data)
                .subscribe(resolve, reject);
        });
    }

    /**
     * Добавить избранных риелторов пользователю
     *
     * @param userGuid
     * @param realtorsGuids
     * @returns {Promise<T>}
     */
    public addPartners(userGuid: string, realtorsGuids: string[]): Promise<boolean> {
        return new Promise((resolve, reject) => {
            const url = this.usersEndPoint + 'users/' + userGuid + '/partners';

            this.httpService.put(url, realtorsGuids)
                .map((response: Response) => response.json())
                .map((response: any) => response.data)
                .subscribe(resolve, reject);
        });
    }

    /**
     * Установить избранных риелторов пользователю
     *
     * @param userGuid
     * @param realtors
     * @returns {Promise<T>}
     */
    public setPartners(userGuid: string, realtors: string[]): Promise<boolean> {
        return new Promise((resolve, reject) => {
            const url = this.usersEndPoint + 'users/' + userGuid + '/partners';

            this.httpService.post(url, realtors)
                .map((response: Response) => response.json())
                .map((response: any) => response.data)
                .subscribe(resolve, reject);
        });
    }

    /**
     * Удалить избранных риелторов
     *
     * @param userGuid
     * @param realtorGuid
     * @returns {Promise<T>}
     */
    public deletePartner(userGuid: string, realtorGuid: string): Promise<boolean> {
        return new Promise((resolve, reject) => {
            const url = this.usersEndPoint + 'users/' + userGuid + '/partner/' + realtorGuid;
            this.httpService.delete(url)
                .map((response: Response) => response.json())
                .map((response: any) => response.data)
                .subscribe(resolve, reject);
        });
    }

    /**
     * Фильтр по риелторам
     *
     * @returns {Promise<T>}
     * @param body
     * @param query
     */
    public filter(body: RealtorsFilterInterface, query?: any): Promise<ResponseListModel<RealtorModel>> {
        return new Promise((resolve, reject) => {
            let search = new URLSearchParams();

            if (query) {
                let keys = Object.keys(query);

                for (let i = 0; i < keys.length; i++) {
                    let key = keys[i];
                    search.set(key, query[key]);
                }
            }

            this.httpService.post(RealtorsUrls.list, body, {search: search})
                .map((response: Response) => response.json())
                .map((response: any) => response.data)
                .subscribe(resolve, reject);
        });
    }

    /**
     * Фильтр по риелторам
     * @returns {Promise<ResponseListModel<RealtorModel>>}
     * @param body
     * @param query
     */
    public filterMembers(body: RealtorsFilterInterface, query?: any): Promise<ResponseListModel<RealtorModel>> {
        return new Promise((resolve, reject) => {
            const url = `${this.usersEndPoint}users/filter-members`;
            let search = new URLSearchParams();

            if (query) {
                let keys = Object.keys(query);

                for (let i = 0; i < keys.length; i++) {
                    let key = keys[i];
                    search.set(key, query[key]);
                }
            }

            this.httpService.post(url, body, {search: search})
                .map((response: Response) => response.json())
                .map((response: any) => response.data)
                .subscribe(resolve, reject);
        });
    }

    /**
     * Получает информацию о риелторе по его идентификатору
     * @param {string} guid Идентификатор риелтора
     * @returns {Promise<T>}
     */
    public load(guid: string): Promise<RealtorModel> {
        return new Promise((resolve, reject) => {
            const url = `${this.usersEndPoint}users/${guid}`;

            this.httpService.get(url)
                .map((response: Response) => response.json())
                .map((response: any) => response.data)
                .subscribe(resolve, reject);
        });
    }

    /**
     * Получение моделей пользователей по массиву идентификаторов
     * @param {string[]} guids Массив идентификаторов
     * @returns {Promise<any>}
     */
    public getUsersByGuids(guids: string[]): Promise<any> {
        return new Promise((resolve, reject) => {
            const url = `${this.usersEndPoint}users/get-by-guids`;

            this.httpService.post(url, guids)
                .map((response: Response) => response.json())
                .map((response: any) => response.data)
                .subscribe(resolve, reject);
        });
    }

    /**
     * Установка URL до сервиса users
     *
     * @param usersEndPoint
     */
    public setUsersEndPoint(usersEndPoint: string) {
        this.usersEndPoint = usersEndPoint;
    }
}
