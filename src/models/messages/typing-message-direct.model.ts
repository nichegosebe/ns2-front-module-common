import {MESSAGE_RCPT_DIRECT} from "./abstract/const";
import {AbstractTypingMessage} from "./abstract/typing/abstract-typing-message.model";

/**
 * Модель уведомления о начале/окончании набора сообщения в чате
 */
export class TypingMessageDirect extends AbstractTypingMessage {
    constructor() {
        super();
        this.header.rcpt = MESSAGE_RCPT_DIRECT;
    }
}