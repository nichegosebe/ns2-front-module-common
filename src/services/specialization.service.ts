import {AbstractRefsService} from "./abstract-refs.service";
import {Injectable} from "@angular/core";
import {SpecializationModel} from "../models/specialization.model";

@Injectable()
export class SpecialziationService extends AbstractRefsService {
    refName: string = 'spec';

    public getSpecializations(): Promise<SpecializationModel[]> {
        return this.getListAll({}, 'sorder,title');
    }
}