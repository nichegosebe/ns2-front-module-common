import {MESSAGE_RCPT_GROUP} from "./abstract/const";
import {AbstractEditMessage} from "./abstract/edit/abstract-edit-message.model";

/**
 * Модель редактирования сообщения в группе
 */
export class EditMessageGroup extends AbstractEditMessage {
    constructor() {
        super();
        this.header.rcpt = MESSAGE_RCPT_GROUP;
    }
}