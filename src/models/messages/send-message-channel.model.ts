import {MESSAGE_RCPT_CHANNEL} from "./abstract/const";
import {AbstractSendMessage} from "./abstract/send/abstract-send-message.model";

/**
 * Модель отправки сообщения в канал
 */
export class SendMessageChannel extends AbstractSendMessage {
    constructor() {
        super();
        this.header.rcpt = MESSAGE_RCPT_CHANNEL;
    }
}