import {AbstractMessage} from "../abstract-message.model";
import {MESSAGE_HEADER_TYPE_MESSAGE, MESSAGE_HEADER_ACTION_TYPING} from "../const";
import {TypingMessagePayload} from "./typing-message-payload.model";

/**
 * Базовая модель сообщения о наборе текста
 */
export abstract class AbstractTypingMessage extends AbstractMessage {
    payload: TypingMessagePayload = new TypingMessagePayload();

    constructor() {
        super();
        this.header.type = MESSAGE_HEADER_TYPE_MESSAGE;
        this.header.action = MESSAGE_HEADER_ACTION_TYPING;
    }
}