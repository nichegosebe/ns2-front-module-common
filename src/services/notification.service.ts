import { Injectable } from '@angular/core';
import { Response, URLSearchParams } from '@angular/http';

// Services
import { HttpService } from './http.service';

// Models
import { NotifyInfoModel } from '../models/notify-info.model';
import { NotificationItemModel } from '../models/invite-item.model';
import { ResponseListModel } from '../models/response-list.model';
import { NotificationUrls } from '../urls/notification-urls.model';

// RxJS
import { Observable, Observer } from 'rxjs';

// Interfaces
import { IResponse } from '../intefraces/response.interface';

@Injectable()
export class NotificationService {
    constructor(private httpService: HttpService) {
    }

    public getInfo(): Promise<NotifyInfoModel> {
        return new Promise<NotifyInfoModel>((resolve, reject) => {
            const url = NotificationUrls.messages;

            this.httpService.get(url)
                .map((response: Response) => response.json())
                .map((response: any) => response.data)
                .subscribe(resolve, reject);
        });
    }

    /**
     * Получает список обработанных уведомлений
     * @param {any} query Параметры запроса
     * @returns {Promise<ResponseListModel<NotificationItemModel>>}
     */
    public getProcessedList(query?: any): Promise<ResponseListModel<NotificationItemModel>> {
        return new Promise<ResponseListModel<NotificationItemModel>>((resolve, reject) => {
            const url = NotificationUrls.processedList;

            let params = new URLSearchParams();

            if (query) {
                let keys = Object.keys(query);

                for (let i = 0; i < keys.length; i++) {
                    const key = keys[i];
                    params.set(key, query[key]);
                }
            }

            this.httpService.get(url, { params: params })
                .map((response: Response) => response.json())
                .map((response: any) => response.data)
                .subscribe(resolve, reject);
        });
    }

    /**
     * Получает список необработанных уведомлений
     * @param {any} query Параметры запроса
     * @returns {Promise<ResponseListModel<NotificationItemModel>>}
     */
    public getUnProcessedList(query?: any): Promise<ResponseListModel<NotificationItemModel>> {
        return new Promise<ResponseListModel<NotificationItemModel>>((resolve, reject) => {
            const url = NotificationUrls.unProcessedList;

            let params = new URLSearchParams();

            if (query) {
                let keys = Object.keys(query);

                for (let i = 0; i < keys.length; i++) {
                    const key = keys[i];
                    params.set(key, query[key]);
                }
            }

            this.httpService.get(url, { params: params })
                .map((response: Response) => response.json())
                .map((response: any) => response.data)
                .subscribe(resolve, reject);
        });
    }

    /**
     * Принять уведомление
     * @param {string} operation_guid Guid операции
     * @returns {Observable<IResponse<any>>}
     */
    public acceptNotification(operation_guid: string): Observable<IResponse<any>> {
        const url = `${NotificationUrls.actionWithNotification}/${operation_guid}`
        const params = new URLSearchParams();

        return new Observable((observer: Observer<IResponse<any>>) => {
            this.httpService.post(url, { params })
                .map((response: Response) => response.json())
                .subscribe(
                    (response: IResponse<any>) => {
                        observer.next(response.data);
                        observer.complete();
                    },
                    (err: any) => {
                        observer.error(err)
                    }
                );
        })
    }

    /**
     * Отклонить уведомление
     * @param {string} operation_guid Guid операции
     * @returns {Observable<IResponse<any>>}
     */
    public declineNotification(operation_guid: string): Observable<IResponse<any>> {
        const url = `${NotificationUrls.actionWithNotification}/${operation_guid}`

        return new Observable((observer: Observer<IResponse<any>>) => {
            this.httpService.delete(url)
                .map((response: Response) => response.json())
                .subscribe(
                    (response: IResponse<any>) => {
                        observer.next(response.data);
                        observer.complete();
                    },
                    (err: any) => {
                        observer.error(err)
                    }
                );
        })
    }
}
