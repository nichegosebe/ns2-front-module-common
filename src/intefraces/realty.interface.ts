// Интерфейс объявления
import { RealtorInterface } from './realtor.interface';

// Интерфейс объявления
export interface IRealty {
    // Уникальный идентификатор
    guid: string;

    // Дата / время создания (timestamp)
    created: number;

    // Дата / время последней модификации
    updated: number;

    // Идентификатор пользователя создавшего объявление
    creator_guid: string;

    // Идентификатор пользователя изменившего объявление
    updater_guid: string;

    // Номер объявления
    id: number;

    // Флаг определяющий опубликованно ли объявление
    is_published: boolean;

    // Флаг определяющий опубликованно ли объявление на НС
    is_sale_published: boolean

    // Счетчик публикаций
    count_published: number;

    // Дата последней публикации объявления
    data_published: number;

    // Уникальный идентификатор источника объявления
    advert_source_guid: string;

    // Идентификатор действия
    action_guid: string;

    // Идентификатор сущности
    entity_guid: string;

    // Идентификатор города в котором размещено объявление
    city: string;

    // Объявление является эксклюзивным
    is_exclusive: boolean;

    // Профиль риелтора
    author: RealtorInterface;

    // Объявление добавлено в избранное
    is_favorite_realty: boolean;

    // Дополнительные динамические поля
    [name: string]: any;
}
