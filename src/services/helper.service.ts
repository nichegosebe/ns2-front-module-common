import {Injectable} from "@angular/core";
import {CurrentUserService} from "./current-user.service";
import {RealtorModel} from "../models/realtor.model";
import {CurrentUserInfoInterface} from "../intefraces/current-user-info.interface";

@Injectable()
export class HelperService {

    constructor(private currentUser: CurrentUserService) {
    }

    /**
     * Plural forms
     *
     * Use:
     *
     * declOfNum(count, ['найдена', 'найдено', 'найдены']);
     *
     * @see https://gist.github.com/realmyst/1262561#gistcomment-2032406
     *
     * @param n
     * @param titles
     * @returns {string}
     */
    public declOfNum(n: number, titles: string[]) {
        const cases = [2, 0, 1, 1, 1, 2];
        return titles[(n % 100 > 4 && n % 100 < 20) ? 2 : cases[(n % 10 < 5) ? n % 10 : 5]];
    }

    /**
     * Возвращает выделенный текст
     *
     * @returns {string}
     */
    public getSelectedText(): string {
        let result = "";

        if (window.getSelection) {
            result = window.getSelection().toString();
        }

        return result;
    }

    /**
     * Форматирует телефонный номер
     * @param prefix Префикс
     * @param phone Номер телефона
     * @returns {string}
     */
    public maskPhone(prefix: string, phone: string): string {
        return prefix + ' ' +
            phone.substr(0, 3) + ' ' +
            phone.substr(3, 3) + '-' +
            phone.substr(6, 2) + '-' +
            phone.substr(8, 2);
    }

    /**
     * Возвращает количество опубликованных объявлений риелтором в текущем городе
     * @param {RealtorModel} realtorInfo Модель риелтора
     * @returns {Number}
     */
    public getPublishedRealty(realtorInfo: RealtorModel): number {
        const realtyInfo = this.getRealtyInfo(realtorInfo);
        return realtyInfo.hasOwnProperty('published') ? Number(realtyInfo['published']) : 0;
    }

    /**
     * Возвращает количество опубликованных эксклюзивных объявлений риелтором в текущем городе
     * @param {RealtorModel} realtorInfo Модель риелтора
     * @returns {Number}
     */
    public getExclusiveRealty(realtorInfo: RealtorModel): number {
        const realtyInfo = this.getRealtyInfo(realtorInfo);
        return realtyInfo.hasOwnProperty('exclusive') ? Number(realtyInfo['exclusive']) : 0;
    }

    /**
     * Возвращает информацию о количестве объявлений риелтора в текущем городе
     * @param {RealtorModel} realtorInfo Модель риелтора
     * @returns {{}}
     */
    private getRealtyInfo(realtorInfo: RealtorModel): any {
        const userInfo: CurrentUserInfoInterface = this.currentUser.getInfo(),
            sessionInfo: any = userInfo ? userInfo.session : null,
            geoGuid: string = sessionInfo ? sessionInfo.geo_guid : '';
        return realtorInfo.realty_count && realtorInfo.realty_count.hasOwnProperty(geoGuid) ? realtorInfo.realty_count[geoGuid] : {};
    }
}
