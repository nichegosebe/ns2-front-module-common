export interface PhoneInterface {
    phone: string;
    is_primary: boolean;
}