import {PhoneInterface} from "../intefraces/phone.interface";

export class PhoneModel implements PhoneInterface {
    phone: string;
    is_primary: boolean = false;
}