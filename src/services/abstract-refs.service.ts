import {URLSearchParams, Response} from '@angular/http';
import {HttpService} from './http.service';
import 'rxjs/operator/map';
import {Injectable, Inject} from '@angular/core';
import {HttpErrorInterface} from '../intefraces/http-error.interface';
import {ResponseListModel} from '../models/response-list.model';
import {StorageInterface} from '../intefraces/storage.interface';

@Injectable()
export abstract class AbstractRefsService {

    protected static MAX_ITEMS: number = 1000;
    public static PER_PAGE_FIELD: string = 'per-page';
    public static PAGE_FIELD: string = 'page';
    public static SORT_FIELD = 'sort';

    public abstract refName: string;

    protected refsEndPoint: string;

    constructor(protected httpService: HttpService, @Inject('StorageInterface') private cache: StorageInterface) {
    }

    public setRefsEndPoint(_refsEndPoint: string) {
        this.refsEndPoint = _refsEndPoint;
    }

    /**
     * Получить информацию об одной записе
     *
     * @param guid
     * @returns {Promise<T>}
     */
    public getOne(guid: string): Promise<any> {
        return new Promise((resolve, reject) => {
            let cacheKey = this.cache.getCacheKey(this.refName, guid);

            this.cache.get(cacheKey).then(resolve).catch(() => {
                let url = this.refsEndPoint + 'data/' + this.refName + '/' + guid;

                this.httpService.get(url)
                    .map((response: Response) => response.json())
                    .map((response: any) => response.data)
                    .subscribe(
                        (data: any) => {
                            let doResolve = () => {
                                resolve(data);
                            };
                            this.cache.set(cacheKey, data, 0, [data.guid, this.refName])
                                .then(doResolve)
                                .catch(doResolve);
                        },
                        (err: HttpErrorInterface) => {
                            reject(err);
                        }
                    );
            });
        });
    }

    public treeUp(guid: string, params?: any): Promise<any> {
        return new Promise((resolve, reject) => {
            let cacheKey = this.cache.getCacheKey('tree', this.refName, guid, JSON.stringify(params));

            this.cache.get(cacheKey)
                .then(resolve)
                .catch(() => {
                    const url = this.refsEndPoint + 'tree/' + this.refName + '/up/' + guid;
                    const search = params || {};

                    this.httpService.get(url, {search})
                        .map((response: Response) => response.json())
                        .map((response: any) => response.data)
                        .subscribe(
                            (data: any) => {
                                let doResolve = () => {
                                    resolve(data);
                                };

                                this.cache.set(cacheKey, data, 0, [data.guid, this.refName])
                                    .then(doResolve)
                                    .catch(doResolve);
                            },
                            (err: HttpErrorInterface) => {
                                reject(err);
                            }
                        );
                });
        });
    }

    public treeDown(guid: string, params?: any): Promise<any> {
        return new Promise((resolve, reject) => {
            let cacheKey = this.cache.getCacheKey('tree', this.refName, guid, JSON.stringify(params));

            this.cache.get(cacheKey)
                .then(resolve)
                .catch(() => {
                    const url = this.refsEndPoint + 'tree/' + this.refName + '/down/' + guid;
                    const search = params || {};

                    this.httpService.get(url, {search})
                        .map((response: Response) => response.json())
                        .map((response: any) => response.data)
                        .subscribe(
                            (data: any) => {
                                let doResolve = () => {
                                    resolve(data);
                                };

                                this.cache.set(cacheKey, data, 0, [data.guid, this.refName])
                                    .then(doResolve)
                                    .catch(doResolve);
                            },
                            (err: HttpErrorInterface) => {
                                reject(err);
                            }
                        );
                });
        });
    }

    public getList(params: any): Promise<ResponseListModel<any>> {
        return new Promise((resolve, reject) => {
            let url = this.refsEndPoint + 'data/' + this.refName;
            let keys = Object.keys(params);
            let _filter = new URLSearchParams();

            for (let i = 0; i < keys.length; i++) {
                let key = keys[i];
                _filter.set(key, params[key]);
            }

            let cacheKey = this.cache.getCacheKey(this.refName, _filter.toString());

            this.cache.get(cacheKey)
                .then(resolve)
                .catch(() => {
                    this.httpService
                        .get(url, {
                            search: _filter
                        })
                        .map((response: Response) => response.json())
                        .map((response: any) => response.data)
                        .subscribe(
                            (data: any) => {
                                let doResolve = () => {
                                    resolve(data);
                                };

                                let tags: string[] = [this.refName];
                                let items = data ? data.items || [] : [];
                                items.map((v: any) => {
                                    if (v && v.guid) {
                                        tags.push(v.guid);
                                    }
                                });

                                this.cache.set(cacheKey, data, 0, tags)
                                    .then(doResolve)
                                    .catch(doResolve);
                            },
                            (err: HttpErrorInterface) => {
                                reject(err);
                            }
                        );
                });
        });
    }

    /**
     * Загружает весь справочник
     * @param params
     * @param sort
     * @returns {Promise<T>}
     */
    public getListAll(params: any, sort: string = ''): Promise<any> {
        return new Promise((resolve, reject) => {
            let url = this.refsEndPoint + 'data/' + this.refName + '/full';
            let keys = Object.keys(params);
            let _filter = new URLSearchParams();

            for (let i = 0; i < keys.length; i++) {
                let key = keys[i];
                _filter.set(key, params[key]);
            }

            if (sort != '') {
                _filter.set(AbstractRefsService.SORT_FIELD, sort);
            }

            let cacheKey = this.cache.getCacheKey(this.refName, _filter.toString());

            this.cache.get(cacheKey)
                .then(resolve)
                .catch(() => {
                    this.httpService
                        .get(url, {
                            search: _filter
                        })
                        .map((response: Response) => response.json())
                        .map((response: any) => response.data)
                        .subscribe(
                            (data: any) => {
                                let doResolve = () => {
                                    resolve(data);
                                };

                                let tags: string[] = [this.refName];
                                data.map((v: any) => {
                                    if (v && v.guid) {
                                        tags.push(v.guid);
                                    }
                                });

                                this.cache.set(cacheKey, data, 0, tags)
                                    .then(doResolve)
                                    .catch(doResolve);
                            },
                            (err: HttpErrorInterface) => {
                                reject(err);
                            }
                        );
                });
        });
    }
}