// Интерфейс ответа с сервера
export interface IResponse<T> {
    data: T;
    errno: number;
    error: string;
    success: boolean;
}
