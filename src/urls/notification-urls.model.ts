// Базовый URL
import { BasicUrls } from './basic-urls.model';

// URL's для работы с уведомлениями
// @dynamic
export class NotificationUrls extends BasicUrls {
	// Список необработанных уведомлений
	public static get unProcessedList(): string {
		return `${NotificationUrls.endPoint}/notifications`;
    }

    // Информация о количестве непрочитанных сообщений и наличие уведомлений у текущего пользователя
	public static get messages(): string {
		return `${NotificationUrls.endPoint}/messaging/notify-info`;
    }

	// Список обработанных уведомлений
	public static get processedList(): string {
		return `${NotificationUrls.unProcessedList}/history`;
    }

    // Действия с уведомлением
	public static get actionWithNotification(): string {
		return `${NotificationUrls.unProcessedList}/operation`;
    }
}
