export interface RegisterInfoInterface {
    guid: string;
    email: string;
    password: string;
    password_repeat: string;
    firstname: string;
    lastname: string;
    geo_guid: string;
    phone: string;
}