import {AbstractRefModel} from "./abstract-ref.model";
export abstract class LinkRefModel extends AbstractRefModel {
    src: string;
    dst: string;
}