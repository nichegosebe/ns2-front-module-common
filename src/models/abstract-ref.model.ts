export abstract class AbstractRefModel {
    relname: string;
    guid: string;
    created: number;
    updated: number;
    name: string;
    title: string;
}