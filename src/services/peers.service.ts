import {Injectable} from "@angular/core";
import {Response, URLSearchParams} from '@angular/http';
import {HttpService} from "./http.service";
import {ResponseListModel} from "../models/response-list.model";
import {ChatMessageModel} from "../models/chat-message.model";
import {MessageSearchResultModel} from "../models/message-search-result.model";
import {Observable} from "rxjs";
import {ChatInfoModel} from "../models/chat-info.model";

@Injectable()
export class PeersService {

    /**
     * Base URL (точка входа) в микросервис сообщений
     */
    private messagesEndPoint: string;

    constructor(private httpService: HttpService) {
    }

    /**
     * Создание диалога между текущим пользователем и userGuid
     * @param {string} userGuid С кем создать диалог
     * @returns {Promise<ChatInfoModel>}
     */
    public createPeer(userGuid: string): Promise<ChatInfoModel> {
        return new Promise<ChatInfoModel>((resolve, reject) => {
            const url = `${this.messagesEndPoint}peers/${userGuid}`;

            this.httpService.post(url, null)
                .map((response: Response) => response.json())
                .map((response: any) => response.data)
                .subscribe(resolve, reject);
        });
    }

    /**
     * Получение диалогов текущего пользователя
     * @param {string} lastPeerGuid Идентификатор диалога начиная с которого надо возвращать чаты
     * @param {string} stopPeerGuid Идентификатор диалога, до которого нужно получить чаты
     * @param {any} query           Дополнительные условия
     * @returns {Promise<ResponseListModel<ChatInfoModel>>}
     */
    public getPeers(lastPeerGuid?: string, stopPeerGuid?: string, query?: any): Promise<ResponseListModel<ChatInfoModel>> {
        return new Promise<ResponseListModel<ChatInfoModel>>((resolve, reject) => {
            let url = `${this.messagesEndPoint}peers/list`;

            if (lastPeerGuid) {
                url += `/${lastPeerGuid}`;

                if (stopPeerGuid) {
                    url += `/${stopPeerGuid}`;
                }
            }

            let params = new URLSearchParams();

            if (query) {
                let keys = Object.keys(query);

                for (let i = 0; i < keys.length; i++) {
                    const key = keys[i];
                    params.set(key, query[key]);
                }
            }

            this.httpService.get(url, {params: params})
                .map((response: Response) => response.json())
                .map((response: any) => response.data)
                .subscribe(resolve, reject);
        });
    }

    /**
     * Возвращает диалог
     * @param {string} peerGuid Идентификатор диалога
     * @returns {Promise<ChatInfoModel>}
     */
    public getPeer(peerGuid: string): Promise<ChatInfoModel> {
        return new Promise((resolve, reject) => {
            const url = `${this.messagesEndPoint}peers/${peerGuid}`;

            this.httpService.get(url)
                .map((response: Response) => response.json())
                .map((response: any) => response.data)
                .subscribe(resolve, reject);
        });
    }

    /**
     * Получение истории сообщений диалога с пользователем
     * @param {string} peerGuid     Идентификатор диалога
     * @param {string} messageGuid  Идентификатор сообщения до которого необходимо получить записи истории
     * @param startMessageGuid      Идентификатор сообщения с которого необходимо получить записи истории
     * @param numBefore             Количество сообщений которые надо добавить до начального сообщения
     * @param query                 Дополнительные параметры
     * @returns {Promise<ResponseListModel<ChatMessageModel>>}
     */
    public getHistory(peerGuid: string, messageGuid?: string, startMessageGuid?: string, numBefore?: number, query?: any): Promise<ResponseListModel<ChatMessageModel>> {
        return new Promise((resolve, reject) => {
            let url = `${this.messagesEndPoint}peers/${peerGuid}/history`;

            if (messageGuid) {
                url += `/${messageGuid}`;
                if (startMessageGuid) {
                    url += `/${startMessageGuid}`;
                    if (numBefore) {
                        url += `/${numBefore}`;
                    }
                }
            }

            let params = new URLSearchParams();

            if (query) {
                let keys = Object.keys(query);

                for (let i = 0; i < keys.length; i++) {
                    const key = keys[i];
                    params.set(key, query[key]);
                }
            }

            this.httpService.get(url, {params: params})
                .map((response: Response) => response.json())
                .map((response: any) => response.data)
                .subscribe(resolve, reject);
        });
    }

    /**
     * Метод для поиска (чатов)
     * @param {string} queryText Строка поиска
     * @param {any} query Дополнительные параметры запроса
     * @returns {Promise<ResponseListModel<ChatInfoModel>>}
     */
    public search(queryText: string, query?: any): Promise<ResponseListModel<ChatInfoModel>> {
        return new Promise<ResponseListModel<ChatInfoModel>>((resolve, reject) => {
            const url: string = `${this.messagesEndPoint}peers/search`;

            let params = new URLSearchParams();

            params.set('q', queryText);
            if (query) {
                let keys = Object.keys(query);

                for (let i = 0; i < keys.length; i++) {
                    const key = keys[i];
                    params.set(key, query[key]);
                }
            }

            return this.httpService.get(url, {params: params})
                .map((response: Response) => response.json())
                .map((response: any) => response.data)
                .subscribe(resolve, reject);
        });
    }

    /**
     * Метод для поиска сообщений
     * @param {string} peerGuid Идентификатор чата
     * @param {string} query Строка для поиска
     * @param {string} messageGuid Идентификатор сообщения до которого необходимо искать
     * @returns {Observable<MessageSearchResultModel>}
     */
    public searchMessages(peerGuid: string, query: string, messageGuid?: string): Observable<MessageSearchResultModel> {
        let url = `${this.messagesEndPoint}peers/${peerGuid}/search-messages`;
        if (messageGuid) {
            url += '/' + messageGuid;
        }
        url += '?q=' + query;

        return this.httpService.get(url)
            .map((response: Response) => response.json())
            .map((response: any) => response.data);
    }


    /**
     * Установка URL до сервиса messaging
     *
     * @param messagesEndPoint
     */
    public setMessagesEndPoint(messagesEndPoint: string) {
        this.messagesEndPoint = messagesEndPoint;
    }
}
