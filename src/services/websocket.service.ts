import {Injectable} from '@angular/core';
import Rxmq from 'rxmq';
import {Subscription} from "rxjs";

@Injectable()
/**
 * Сервис для работы с WebScoket-ами
 */
export class WebsocketService {

    // Имя канала
    public static readonly CHANNEL_NAME: string = 'ws';

    // Сообщения о открытии сокета
    public static readonly SOCKET_OPEN_TOPIC_NAME: string = 'socket_open';

    // Сообщения о поступлении сообщения в сокет
    public static readonly SOCKET_MESSAGE_TOPIC_NAME: string = 'socket_message';

    // Сообщения о закрытии сокета
    public static readonly SOCKET_CLOSE_TOPIC_NAME: string = 'socket_close';

    // Сообщения о ошибке в сокете
    public static readonly SOCKET_ERROR_TOPIC_NAME: string = 'socket_error';

    // Код нормального закрытия сокета (без дальнейшей попытки переустановки соединения)
    public static readonly SOCKET_CLOSE_NORMAL = 1000;

    // Код ненормального закрытия сокета (после такого закрытия будут предприняты попытки по переустановке соединения)
    public static readonly SOCKET_CLOSE_ABNORMAL = 3000;

    // Объект канала в который отправляются сообщения из сокета
    private channel: any = Rxmq.channel(WebsocketService.CHANNEL_NAME);

    // Объект сокета
    private ws: WebSocket;

    // Точка обмена с сервером
    private endPoint: string;

    // Время между попытками восстановления соединения
    private reconnectTimeout: number = 1000;

    // Признак, установлено ли соединения или нет
    private isConnected: boolean = false;

    constructor() {
    }

    /**
     * Устанавливает точку обмена с сервером
     * @param {string} endPoint
     */
    public setEndPoint(endPoint: string) {
        this.endPoint = endPoint;
    }

    /**
     * Устанавливает время между попытками восстановления соединения
     * @param {number} connectTimeout
     */
    public setReconnectTimeout(connectTimeout: number) {
        this.reconnectTimeout = connectTimeout;
    }

    /**
     * Отправляет данные в сокет
     * @param {any} data Данные
     */
    public send(data: any): void {
        this.ws.send(JSON.stringify(data));
    }
    
    /**
     * Подписка на установление соединения
     * @param {Function} then Функция-подписчик
     * @returns {Subscription}
     */
    public onOpen(then: (value: Event) => void): Subscription {
        return this.channel.observe(WebsocketService.SOCKET_OPEN_TOPIC_NAME).subscribe(then);
    }

    /**
     * Подписка на получение сообщения
     * @param {Function} then Функция-подписчик
     * @returns {Subscription}
     */
    public onMessage(then: (value: MessageEvent) => void): Subscription {
        return this.channel.observe(WebsocketService.SOCKET_MESSAGE_TOPIC_NAME).subscribe(then);
    }

    /**
     * Подписка на закрытие сокета
     * @param {Function} then Функция-подписчик
     * @returns {Subscription}
     */
    public onClose(then: (value: CloseEvent) => void): Subscription {
        return this.channel.observe(WebsocketService.SOCKET_CLOSE_TOPIC_NAME).subscribe(then);
    }

    /**
     * Подписка на ошибки сокета
     * @param {Function} then Функция-подписчик
     * @returns {Subscription}
     */
    public onError(then: (value: ErrorEvent) => void): Subscription {
        return this.channel.observe(WebsocketService.SOCKET_ERROR_TOPIC_NAME).subscribe(then);
    }

    /**
     * Установить соединение с сервером
     */
    public connect(params: any): void {
        if (!this.isConnected) {
            this.openConnection(params);
            this.isConnected = true;
        }
    }

    /**
     * Закрыть соединение с сервером
     */
    public disconnect(): void {
        if (this.isConnected) {
            this.ws.close(WebsocketService.SOCKET_CLOSE_NORMAL);
            this.isConnected = false;
        }
    }

    /**
     * Открытие соединения с WebSocket и подписка на события
     */
    private openConnection(params: any): void {
        let url = this.endPoint,
            paramsArray: string[] = [];

        for (let pName in params) {
            if (params.hasOwnProperty(pName)) {
                paramsArray.push(pName + '=' + params[pName]);
            }
        }
        if (paramsArray.length > 0) {
            url += '?' + paramsArray.join('&');
        }

        this.ws = new WebSocket(url);

        this.ws.onopen = (ev: Event) => {
            this.channel.subject(WebsocketService.SOCKET_OPEN_TOPIC_NAME).next(ev);
        };

        this.ws.onmessage = (response: MessageEvent) => {
            const data = JSON.parse(response.data) || {};
            this.channel.subject(WebsocketService.SOCKET_MESSAGE_TOPIC_NAME).next(data);
        };

        this.ws.onerror = (err: ErrorEvent) => {
            this.channel.subject(WebsocketService.SOCKET_ERROR_TOPIC_NAME).next(err);
            this.ws.close(WebsocketService.SOCKET_CLOSE_ABNORMAL);
        };

        this.ws.onclose = (ev: CloseEvent) => {
            this.channel.subject(WebsocketService.SOCKET_CLOSE_TOPIC_NAME).next(ev);
            if (ev.code !== WebsocketService.SOCKET_CLOSE_NORMAL) {
                setTimeout(() => {
                    this.openConnection(params);
                }, this.reconnectTimeout);
            }
        };
    }
}