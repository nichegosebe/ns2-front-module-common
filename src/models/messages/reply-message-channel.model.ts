import {MESSAGE_RCPT_CHANNEL} from "./abstract/const";
import {AbstractReplyMessage} from "./abstract/reply/abstract-reply-message.model";

/**
 * Модель ответа на сообщение в канале
 */
export class ReplyMessageChannel extends AbstractReplyMessage {
    constructor() {
        super();
        this.header.rcpt = MESSAGE_RCPT_CHANNEL;
    }
}