import {MESSAGE_RCPT_DIRECT} from "./abstract/const";
import {AbstractEditMessage} from "./abstract/edit/abstract-edit-message.model";

/**
 * Модель редактирования сообщения в чате
 */
export class EditMessageDirect extends AbstractEditMessage {
    constructor() {
        super();
        this.header.rcpt = MESSAGE_RCPT_DIRECT;
    }
}