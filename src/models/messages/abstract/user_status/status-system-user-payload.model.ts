import {MessagePayload} from "../messages-payload.model";

/**
 * Модель полезной нагрузки сообщения: сообщение о смене статуса пользователя
 */
export class StatusSystemUserPayload extends MessagePayload {
    status: {
        isOnline: boolean;
    };
}