// URL's для работы.
import { BasicUrls } from './basic-urls.model';


/** GW для работы с риэлторами */
// @dynamic
export class RealtorsUrls extends BasicUrls {

    // Базовый URL
    public static get baseUrl(): string {
        return `${RealtorsUrls.endPoint}/realtors`;
    }

    // Список рилеторов
    public static get list(): string {
        return `${RealtorsUrls.baseUrl}`;
    }

    // Поиск риелторов
    public static get search(): string {
        return `${RealtorsUrls.baseUrl}/search`;
    }
}
