import {IRealtyFilter} from "../intefraces/realty-filter.interface";

export class RealtyFilterLogicalOperation implements IRealtyFilter {
    private operation: string;
    private value: IRealtyFilter|IRealtyFilter[];

    constructor(operation: string, value: IRealtyFilter|IRealtyFilter[]) {
        this.operation = operation;
        this.value = value;
    }

    getRequest(): any {
        let result: any = {};
        if (Array.isArray(this.value)) {
            let items: any = [];
            for (let i = 0; i < this.value.length; i++) {
                items[i] = this.value[i].getRequest();
            }
            result[this.operation] = items;
        } else {
            result[this.operation] = this.value.getRequest();
        }

        return result;
    }
}