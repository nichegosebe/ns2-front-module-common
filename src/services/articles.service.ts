import {Injectable} from '@angular/core';
import {Response} from '@angular/http';
import {HttpService} from './http.service';
import {ResponseListModel} from '../models/response-list.model';
import {ArticleModel} from '../models/article.model';

@Injectable()
export class ArticlesService {
    /**
     * Base URL до микросервиса content
     */
    private contentEndPoint: string;

    constructor(private httpService: HttpService) {
    }

    /**
     * Получение статей по трейлу топика
     * @param {string} topicTrail Полное имя топика
     * @returns {Promise<ArticleModel[]>}
     */
    public getArticlesByTopicTrail(topicTrail: string): Promise<ArticleModel[]> {
        return new Promise<ArticleModel[]>((resolve, reject) => {
            const url = `${this.contentEndPoint}article/tree-by-trail/${topicTrail}`;

            this.httpService.get(url)
                .map((response: Response) => response.json())
                .map((response: any) => response.data)
                .subscribe(resolve, reject);
        });
    }

    /**
     * Установка base url до микросервиса контент
     * @param {string} endPoint
     */
    public setContentEndPoint(endPoint: string) {
        this.contentEndPoint = endPoint;
    }
}
