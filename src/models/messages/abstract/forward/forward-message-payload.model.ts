import {MessagePayload} from "../messages-payload.model";

/**
 * Модель полезной нагрузки сообщения: пересылка сообщения
 */
export class ForwardMessagePayload extends MessagePayload {
    message: {
        guid?: string,
        created?: number,
        updated?: number,
        firstname: string,
        lastname: string
    };
    forward: {
        text?: string,
        firstname: string,
        lastname: string,
        guid: string,
        realty?: string[]
    }
}
