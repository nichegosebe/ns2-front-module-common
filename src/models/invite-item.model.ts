/**
 * Модель информации о приглашении
 */
export class NotificationItemModel {
    guid: string;                   // Идентификатор приглашения
    created: number;                // Дата/время создания
    updated: number;                // Дата/время модификации
    creator_guid: string;           // Идентификатор создателся приглашения
    updater_guid: string;           // Идентификатор пользователя изменившего приглашение
    user_guid: string;              // Идентификатор пользователя которого приглашают
    is_accepted: string;            // Принял ли пользователь приглашение
    decision_date: number;          // Дата/время принятия/отказа от приглашения
    is_locked: string;              // Заблокирован ли пользователь в чате
    lock_date: number;              // Дата блокировки
    unlooked: string;               // Количество непросмотренных сообщений
    is_online: string;              // Онлайн пользователь или нет
    inviter_guid: string;           // Идентификатор пользователя который приглашает
    chat_guid: string;              // Идентификатор чата
    chat_type: string;              // Тип чата(Deprecated)
    title: string;                  // Название чата
    logo_guid: string;              // Идентификатор изображения чата
    operation_guid: string;         // Идентификатор события
    type: string;                   // Тип чата
}
