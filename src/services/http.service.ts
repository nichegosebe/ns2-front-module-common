import {
    Http as NgHttp, RequestOptionsArgs, Request, Response, ConnectionBackend, RequestOptions, Headers, XHRBackend,
    BrowserXhr
} from "@angular/http";
import {Observable} from "rxjs/Observable";
import {CurrentUserService} from "./current-user.service";
import {Injectable, Injector, ReflectiveInjector} from "@angular/core";
import {HttpErrorInterface} from "../intefraces/http-error.interface";
import {HttpErrorService} from "./http-error.service";
import {HttpClient, HttpHandler, HttpXhrBackend, XhrFactory} from "@angular/common/http";

export function httpFactory(injector: Injector, backend: XHRBackend, defaultOptions: RequestOptions): HttpService {
    return new HttpService(injector, backend, defaultOptions);
}

@Injectable()
export class HttpService extends NgHttp {

    private injector: Injector;

    private defaultHttpClient: HttpClient;

    constructor(injector: Injector, backend: ConnectionBackend, defaultOptions: RequestOptions) {
        super(backend, defaultOptions);

        this.injector = injector;
    }

    /**
     * Возвращает стандартный HTTP-клиент
     * @returns {HttpClient}
     */
    getDefaultHttpClient(): HttpClient {
        if (!this.defaultHttpClient) {
            const injector = ReflectiveInjector.resolveAndCreate(
                [
                    HttpClient,
                    {provide: HttpHandler, useClass: HttpXhrBackend},
                    {provide: XhrFactory, useClass: BrowserXhr}
                ]
            );
            this.defaultHttpClient = injector.get(HttpClient);
        }
        return this.defaultHttpClient;
    }

    /**
     * Переопределяем стандартный метод request(). Добавляем заголовок с токеном
     *
     * @param url
     * @param options
     * @returns {Observable<Response>}
     */
    request(url: string | Request, options?: RequestOptionsArgs): Observable<Response> {
        const userService: CurrentUserService = this.injector.get(CurrentUserService),
            token = userService.getToken();

        if (typeof url === 'string') {
            const headers = new Headers();

            headers.append(CurrentUserService.USER_TOKEN_NAME, token);

            if (!(options instanceof RequestOptions)) {
                options = new RequestOptions();
            }

            options.headers = headers;
        } else if (url instanceof Request) {
            if (typeof url.headers === 'undefined') {
                url.headers = new Headers();
            }

            url.headers.append(CurrentUserService.USER_TOKEN_NAME, token);
        }

        return Observable.fromPromise(new Promise((resolve, reject) => {
            super.request(url, options)
                .subscribe(
                    (response: Response) => resolve(response),
                    (err: Response) => {
                        reject(this.handleHttpError(err));
                    }
                );
        }));
    }

    private handleHttpError(err: Response) {
        let message: string | null;
        let errNo: number;
        try {
            let json = err.json();
            message = json.hasOwnProperty('error') ? json.error : err.statusText;
            errNo = json.hasOwnProperty('errno') ? json.errno : 0;
        } catch (error) {
            message = err.statusText;
            errNo = 0;
        }

        const error = <HttpErrorInterface> {
            message: message,
            statusCode: err.status,
            errNo: errNo,
            errorResponse: err
        };

        this.injector.get(HttpErrorService).handler.emit(error);

        return error;
    }
}