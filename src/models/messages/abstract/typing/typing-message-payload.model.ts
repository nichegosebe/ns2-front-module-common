import {MessagePayload} from "../messages-payload.model";

/**
 * Модель полезной нагрузки сообщения: начало/окончание набора сообщения
 */
export class TypingMessagePayload extends MessagePayload {
    typing: {
        status: boolean;
    };
}