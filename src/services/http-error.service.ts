import {Injectable, EventEmitter} from "@angular/core";
import {HttpErrorInterface} from "../intefraces/http-error.interface";

@Injectable()
export class HttpErrorService {

    private _handler: EventEmitter<HttpErrorInterface>;

    constructor() {
        this._handler = new EventEmitter<HttpErrorInterface>();
    }

    get handler(): EventEmitter<HttpErrorInterface> {
        return this._handler;
    }
}