import {Response} from "@angular/http";

export interface HttpErrorInterface {
    message: string;
    statusCode: number;
    errNo?: number;
    errorResponse: Response
}