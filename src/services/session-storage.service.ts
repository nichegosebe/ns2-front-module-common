import {Injectable} from "@angular/core";
import {StorageInterface} from "../intefraces/storage.interface";
import {AbstractKeyValueStorage} from "./abstract-key-value-storage.service";

@Injectable()
/**
 * Сервис для работы с sessionStorage
 */
export class SessionStorageService extends AbstractKeyValueStorage implements StorageInterface {

    /**
     * Установка значения по ключу
     * @param {string} key Ключ, по которому необходимо сохранить данные
     * @param {any} value Данные для сохранения
     * @returns {Promise<T>}
     * @protected
     */
    protected _set(key: string, value: any): Promise<any> {
        return new Promise((resolve, reject) => {
            try {
                this._getStorageInstance()
                    .then((storage) => {
                        storage.setItem(key, value);
                        resolve();
                    })
                    .catch(reject);
            } catch (err) {
                reject(err);
            }
        });
    }

    /**
     * Получение значения по ключу
     * @param {string} key Ключ, по которому необходимо получить значение
     * @returns {Promise<T>}
     * @protected
     */
    protected _get(key: string): Promise<any> {
        return new Promise((resolve, reject) => {
            try {
                this._getStorageInstance()
                    .then((storage) => {
                        let value = storage.getItem(key);
                        (value === null) ? reject() : resolve(value);
                    })
                    .catch(reject);
            } catch (err) {
                reject(err);
            }
        });
    }

    /**
     * Удаление значения по ключу
     * @param {string} key Ключ по которым необходимо удалить данные
     * @returns {Promise<T>}
     * @protected
     */
    protected _remove(key: string): Promise<any> {
        return new Promise((resolve, reject) => {
            try {
                this._getStorageInstance()
                    .then((storage) => {
                        storage.removeItem(key);
                        resolve();
                    })
                    .catch(reject);
            } catch (err) {
                reject(err);
            }
        });
    }

    /**
     * Полная очистка хранилища
     * @returns {Promise<T>}
     * @protected
     */
    protected _clear(): Promise<any> {
        return new Promise((resolve, reject) => {
            try {
                this._getStorageInstance()
                    .then((storage) => {
                        storage.clear();
                        resolve();
                    })
                    .catch(reject);
            } catch (err) {
                reject(err);
            }
        });
    }

    /**
     * Возвращает экземпляр хранилища
     * @returns {Promise<T>}
     * @protected
     */
    protected _getStorageInstance(): Promise<any> {
        return new Promise((resolve, reject) => {
            try {
                resolve(sessionStorage);
            } catch (err) {
                reject(err);
            }
        });
    }
}