import {MESSAGE_RCPT_GROUP} from "./abstract/const";
import {AbstractReplyMessage} from "./abstract/reply/abstract-reply-message.model";

/**
 * Модель ответа на сообщение в группе
 */
export class ReplyMessageGroup extends AbstractReplyMessage {
    constructor() {
        super();
        this.header.rcpt = MESSAGE_RCPT_GROUP;
    }
}