export interface SelfInfoInterface {
    email: string;
    password: string;
    firstname: string;
    lastname: string;
    experience: string;
    avatar_guid: string;
    geo_guid: string;
    description: string;
}