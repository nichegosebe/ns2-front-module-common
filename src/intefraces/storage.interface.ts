/**
 * Интерфейс для локальных хранилищ
 */
export interface StorageInterface {

    /**
     * Получение значения по ключу
     * @param {string} key Ключ, по которому необходимо получить значение
     */
    get(key: string): Promise<any>;

    /**
     * Сохранение данных по ключу на указанное время, с учетом переданных тегов
     * @param {string} key Ключ, по которому необходимо сохранить данные
     * @param {any} value Данные для сохранения
     * @param {number} expired Количество секунд в течение которых данные будут храниться
     * @param {string[]} tags Массив тегов, которыми необходимо пометить набор данных
     */
    set(key: string, value: any, expired?: number, tags?: string[]): Promise<any>;

    /**
     * Удаление данных из хранилища по ключу
     * @param {string} key Ключ по которым необходимо удалить данные
     */
    remove(key: string): Promise<any>;

    /**
     * Удаление данных из хранилища по указанному тегу
     * @param {string} tag Имя тега по которому необходимо удалить данные
     */
    removeByTag(tag: string): Promise<any>;

    /**
     * Очистка неиспользуемых/редко используемых данных, освобождение места
     */
    maintenance(): Promise<any>;

    /**
     * Очистка всего хранилища
     */
    clear(): Promise<any>;

    /**
     * Генерация ключа по параметрам
     */
    getCacheKey(...args: any[]): string;
}