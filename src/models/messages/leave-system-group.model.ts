import {AbstractMessage} from "./abstract/abstract-message.model";
import {
    MESSAGE_HEADER_TYPE_SYSTEM,
    MESSAGE_HEADER_ACTION_LEAVE,
    MESSAGE_RCPT_GROUP
} from "./abstract/const";

/**
 * Модель уведомления о выходе пользователя из группы
 */
export class LeaveSystemGroup extends AbstractMessage {
    payload: {
        from: string,
        to: string
    };

    constructor() {
        super();
        this.header.type = MESSAGE_HEADER_TYPE_SYSTEM;
        this.header.action = MESSAGE_HEADER_ACTION_LEAVE;
        this.header.rcpt = MESSAGE_RCPT_GROUP;
    }
}