import {AbstractRefModel} from "./abstract-ref.model";

export class SpecializationModel extends AbstractRefModel {
    icon_guid: string;
    icon_disabled_guid: string;
    publish_title: string;
    profile_title: string;
    filter_title: string;
}