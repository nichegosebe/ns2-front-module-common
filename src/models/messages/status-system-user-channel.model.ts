import {MESSAGE_RCPT_CHANNEL} from "./abstract/const";
import {AbstractStatusSystemUser} from "./abstract/user_status/abstract-status-system-user.model";

/**
 * Модель сообщения смены пользователя в канале
 */
export class StatusSystemUserChannel extends AbstractStatusSystemUser {
    constructor() {
        super();
        this.header.rcpt = MESSAGE_RCPT_CHANNEL;
    }
}