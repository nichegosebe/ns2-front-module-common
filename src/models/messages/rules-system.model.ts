import {AbstractMessage} from "./abstract/abstract-message.model";
import {MESSAGE_HEADER_TYPE_SYSTEM, MESSAGE_HEADER_ACTION_RULES} from "./abstract/const";

/**
 * Модель уведомления о нарушении пункта правил
 */
export class RulesSystem extends AbstractMessage {
    payload: {
        rule: {
            guid: string
        }
    };

    constructor() {
        super();
        this.header.type = MESSAGE_HEADER_TYPE_SYSTEM;
        this.header.action = MESSAGE_HEADER_ACTION_RULES;
    }
}