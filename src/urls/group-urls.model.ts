// Базовый URL
import { BasicUrls } from './basic-urls.model';

// URL's для работы с группами
// @dynamic
export class GroupsUrls extends BasicUrls {
	public static get groupInvite(): string {
		return `${GroupsUrls.endPoint}/messaging/groups/invite`;
    }
}
