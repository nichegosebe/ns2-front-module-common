import {MessagesHeader} from "../models/messages/abstract/header.model";

export interface SocketMessageInterface {
    header: MessagesHeader;
    payload: any;
}