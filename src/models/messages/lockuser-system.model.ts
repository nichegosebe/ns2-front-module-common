import {AbstractMessage} from "./abstract/abstract-message.model";
import {
    MESSAGE_HEADER_TYPE_SYSTEM,
    MESSAGE_HEADER_ACTION_LOCK_USER
} from "./abstract/const";

/**
 * Модель уведомления о блокировании пользователя
 */
export class LockuserSystem extends AbstractMessage {
    constructor() {
        super();
        this.header.type = MESSAGE_HEADER_TYPE_SYSTEM;
        this.header.action = MESSAGE_HEADER_ACTION_LOCK_USER;
    }
}