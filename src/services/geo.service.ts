import { AbstractRefsService } from './abstract-refs.service';
import { Injectable } from '@angular/core';
import { GeoModel } from '../models/geo.model';
import { ResponseListModel } from '../models/response-list.model';

/**
 * Сервис для работы с географическими данными
 */
@Injectable()
export class GeoService extends AbstractRefsService {
	refName: string = 'geo';

	/**
	 * Возвращает все страны
	 * @returns {Promise<GeoModel[]>}
	 */
	public getAllCountries(): Promise<GeoModel[]> {
		return this.getListAll({ level: 1 }, 'sorder,title');
	}

	/**
	 * Возвращает все города в указанной стране
	 * @param countryGuid Идентификатор страны
	 * @returns {Promise<GeoModel[]>}
	 */
	public getAllCities(countryGuid: string): Promise<GeoModel[]> {
		return this.getListAll({ level: 2, pid: countryGuid }, 'sorder,title');
	}

	/**
	 * Возвращает все районы в указанном городе
	 * @param cityGuid Идентификатор города
	 * @returns {Promise<GeoModel[]>}
	 */
	public getAllDistricts(cityGuid: string): Promise<GeoModel[]> {
		return this.getListAll({ level: 3, pid: cityGuid }, 'sorder,title');
	}

	/**
	 * Возвращает страны по указанным параметрам
	 * @param params Параметры для загрузки
	 * @returns {Promise<ResponseListModel<GeoModel>>}
	 */
	public getCountries(params: any): Promise<ResponseListModel<GeoModel>> {
		params = params || {};
		params.level = 1;
		params[ GeoService.SORT_FIELD ] = 'sorder,title';
		return this.getList(params);
	}

	/**
	 * Возвращает города страны по указанным параметрам
	 * @param countryGuid Идентификатор страны
	 * @param params Параметры для загрузки
	 * @returns {Promise<ResponseListModel<GeoModel>>}
	 */
	public getCities(countryGuid: string, params: any): Promise<ResponseListModel<GeoModel>> {
		params = params || {};
		params.level = 2;
		params.pid = countryGuid;
		params[ GeoService.SORT_FIELD ] = 'sorder,title';
		return this.getList(params);
	}

	/**
	 * Возвращает районы города по указанным параметрам
	 * @param cityGuid Идентификатор города
	 * @param params Параметры для загрузки
	 * @returns {Promise<ResponseListModel<any>>}
	 */
	public getDistricts(cityGuid: string, params: any): Promise<ResponseListModel<GeoModel>> {
		params = params || {};
		params.level = 3;
		params.pid = cityGuid;
		params[ GeoService.SORT_FIELD ] = 'sorder,title';
		return this.getList(params);
	}

	/**
	 * Возвращает информацию о городе по его идентификатору
	 * @param guid Идентификатор города
	 * @returns {Promise<GeoModel>}
	 */
	public getCity(guid: string): Promise<GeoModel> {
		return this.getOne(guid);
	}

	/**
	 * Возвращает все записи из дерева на шаг выше
	 * @param guid
	 * @param params
	 * @returns {Promise<any>}
	 */
	public getTreeUp(guid: string, params?: any): Promise<any> {
		return this.treeUp(guid, params);
	}

	/**
	 * Возвращает все записи из дерева на шаг ниже
	 * @param guid
	 * @param params
	 * @returns {Promise<any>}
	 */
	public getTreeDown(guid: string, params?: any): Promise<any> {
		return this.treeDown(guid, params);
	}

	/**
	 * Возвращает информацию о стране по ее идентификатору
	 * @param {string} guid Идентификатор страны
	 * @returns {Promise<GeoModel>}
	 */
	public getCountry(guid: string): Promise<GeoModel> {
		return this.getOne(guid);
	}

	/**
	 * Загружает весь справочник
	 * @param params
	 * @param sort
	 * @returns {Promise<T>}
	 */
	public getListAll(params: any, sort: string = ''): Promise<any> {
		params = params || {};
		params.visible = true;
		return super.getListAll(params, sort);
	}

	/**
	 * Возвращает список только отображаемых объектов
	 * @param {any} params Параметры запроса
	 * @returns {Promise<ResponseListModel<any>>}
	 */
	public getList(params: any): Promise<ResponseListModel<any>> {
		params = params || {};
		params.visible = true;
		return super.getList(params);
	}

	/**
	 * Возвращает часть дерева от заданного узла до корня
	 * @param {string} guid Идентификатор узла
	 * @param {any} params Параметры запроса
	 * @returns {Promise<any>}
	 */
	public treeUp(guid: string, params?: any): Promise<any> {
		params = params || {};
		params.visible = true;
		return super.treeUp(guid, params);
	}

	/**
	 * Возвращает поддерево, начиная от заданного узла
	 * @param {string} guid Идентификатор узла
	 * @param {any} params Параметры запроса
	 * @returns {Promise<any>}
	 */
	public treeDown(guid: string, params?: any): Promise<any> {
		params = params || {};
		params.visible = true;
		return super.treeDown(guid, params);
	}
}