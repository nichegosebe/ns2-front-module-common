import {AbstractMessage} from "../abstract-message.model";
import {MESSAGE_HEADER_TYPE_MESSAGE, MESSAGE_HEADER_ACTION_FORWARD} from "../const";
import {ForwardMessagePayload} from "./forward-message-payload.model";

/**
 * Базовая модель пересылки сообщения
 */
export abstract class AbstractForwardMessage extends AbstractMessage {
    payload: ForwardMessagePayload = new ForwardMessagePayload();

    constructor() {
        super();
        this.header.type = MESSAGE_HEADER_TYPE_MESSAGE;
        this.header.action = MESSAGE_HEADER_ACTION_FORWARD;
    }
}