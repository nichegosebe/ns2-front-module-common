import {AbstractMessage} from "./abstract/abstract-message.model";
import {
    MESSAGE_HEADER_TYPE_SYSTEM,
    MESSAGE_HEADER_ACTION_JOIN,
    MESSAGE_RCPT_CHANNEL
} from "./abstract/const";

/**
 * Модель уведомления о вступлении пользователя в канал
 */
export class JoinSystemChannel extends AbstractMessage {
    payload: {
        from: string,
        to: string
    };

    constructor() {
        super();
        this.header.type = MESSAGE_HEADER_TYPE_SYSTEM;
        this.header.action = MESSAGE_HEADER_ACTION_JOIN;
        this.header.rcpt = MESSAGE_RCPT_CHANNEL;
    }
}