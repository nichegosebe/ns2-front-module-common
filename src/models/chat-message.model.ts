/**
 * Модель сообщения в чате (канале\группе\диалоге)
 */
export abstract class ChatMessageModel {
    guid: string;
    created: number;
    updated: number;
    creator_guid: string;
    updater_guid: string;
    user_peer_guid: string;
    payload: any;
    is_delivered: boolean;
    delivery_date: number;
    is_seen: boolean;
    seen_date: number;
    action: string;
    is_edited: boolean;
    edit_date: number;
}
