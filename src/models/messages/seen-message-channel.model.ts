import {MESSAGE_RCPT_CHANNEL} from "./abstract/const";
import {AbstractSeenMessage} from "./abstract/seen/abstract-seen-message.model";

/**
 * Модель уведомления о просмотре сообщения в канале
 */
export class SeenMessageChannel extends AbstractSeenMessage {
    constructor() {
        super();
        this.header.rcpt = MESSAGE_RCPT_CHANNEL;
    }
}