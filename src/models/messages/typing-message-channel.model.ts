import {MESSAGE_RCPT_CHANNEL} from "./abstract/const";
import {AbstractTypingMessage} from "./abstract/typing/abstract-typing-message.model";

/**
 * Модель уведомления о начале/окончании набора сообщения в канале
 */
export class TypingMessageChannel extends AbstractTypingMessage {
    constructor() {
        super();
        this.header.rcpt = MESSAGE_RCPT_CHANNEL;
    }
}