import {MessageSearchResultItemModel} from "./message-search-result-item.model";

/**
 * Модель результата поиска сообщений
 */
export class MessageSearchResultModel {
    pageSize: number;                                   // Количество записей на данной странице
    totalCount: number;                                 // Общее количество записей удовлетворяющих условию
    items: MessageSearchResultItemModel[];              // Массив найденых записей
}
