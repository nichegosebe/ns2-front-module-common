import { AbstractRefsService } from "./abstract-refs.service";

import { Injectable } from "@angular/core";
import { RealtyDealModel } from "../models/realty-deal.model";

@Injectable()
export class RealtyDealService extends AbstractRefsService {
    refName: string = 'realty_deal';

    /**
     * Получить список всех сделок
     *
     * @returns {Promise<any>}
     */
    public getAllDeals(params?: {}, sort?: string): Promise<RealtyDealModel[]> {
        return this.getListAll(params || {}, sort || 'sorder,title');
    }
}
