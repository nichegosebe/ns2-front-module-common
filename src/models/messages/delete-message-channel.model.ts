import {MESSAGE_RCPT_CHANNEL} from "./abstract/const";
import {AbstractDeleteMessage} from "./abstract/delete/abstract-delete-message.model";

/**
 * Модель удаления сообщения из канала
 */
export class DeleteMessageChannel extends AbstractDeleteMessage {
    constructor() {
        super();
        this.header.rcpt = MESSAGE_RCPT_CHANNEL;
    }
}