import {AbstractMessage} from "./abstract/abstract-message.model";
import {
    MESSAGE_HEADER_TYPE_SYSTEM,
    MESSAGE_HEADER_ACTION_LOCK_REALTY
} from "./abstract/const";

/**
 * Модель уведомления о блокировании объявления
 */
export class LockrealtySystem extends AbstractMessage {
    payload: {
        realty: {
            guid: string
        }
    };

    constructor() {
        super();
        this.header.type = MESSAGE_HEADER_TYPE_SYSTEM;
        this.header.action = MESSAGE_HEADER_ACTION_LOCK_REALTY;
    }
}