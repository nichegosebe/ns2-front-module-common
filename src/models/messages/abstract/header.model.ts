/**
 * Модель заголовка сообщения
 */
export class MessagesHeader {
    type: string;
    action: string;
    rcpt?: string;
    localMessageGuid?: string;
    deviceGuid?: string;
}