export interface RealtorsFilterInterface {
    district_guids?: string[];
    district_type?: string;
    specialization_guids?: string[];
    specialization_type?: string;
    only_partners_for?: string;
    check_partner_for?: string;
    geo_guid?: string;
    hide_current_user: boolean;
}