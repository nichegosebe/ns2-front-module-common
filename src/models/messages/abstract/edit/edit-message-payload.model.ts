import {MessagePayload} from "../messages-payload.model";

/**
 * Модель полезной нагрузки сообщения: редактирование сообщения
 */
export class EditMessagePayload extends MessagePayload {
    message: {
        text: string,
        firstname: string,
        lastname: string,
        guid: string,
        created?: number,
        updated?: number,
        realty?: string[]
    }
}