import {Injectable} from '@angular/core';
import {Response, URLSearchParams} from '@angular/http';

// Services
import {HttpService} from './http.service';

// Models
import {ResponseListModel} from '../models/response-list.model';
import {ChatCreateInfoModel} from '../models/chat-create-info.model';
import {MessageSearchResultModel} from '../models/message-search-result.model';
import {ChatMessageModel} from '../models/chat-message.model';
import {ChatInfoModel} from '../models/chat-info.model';
import {MemberInfoModel} from '../models/member-info.model';

// Interfaces
import {ChatsServiceInterface} from '../intefraces/chats-service.interface';

// RxJs
import {Observable} from 'rxjs/Observable';

// Url's
import { ChannelsUrls } from '../urls/channel-urls.model';

@Injectable()
export class ChannelsService implements ChatsServiceInterface {

    /**
     * Base URL (точка входа) в микросервис сообщений
     */
    private messagesEndPoint: string;

    constructor(private httpService: HttpService) {
    }

    /**
     * Создание чата (канала\группы)
     * @param {ChatCreateInfoModel} chatData Информация для создания чата
     * @returns {Promise<ChatInfoModel>}
     */
    public create(chatData: ChatCreateInfoModel): Promise<ChatInfoModel> {
        return new Promise<ChatInfoModel>((resolve, reject) => {
            const url = `${this.messagesEndPoint}channels`;

            this.httpService.post(url, chatData)
                .map((response: Response) => response.json())
                .map((response: any) => response.data)
                .subscribe(resolve, reject);
        });
    }

    /**
     * Получение списка чатов пользователя (каналов\групп)
     * @param {boolean} onlyMy Показывать только мои чаты
     * @param {string} lastChatGuid Идентфиикатор последнего чата
     * @param {string} stopChatGuid Идентификатор чата, до которого получить информацию
     * @param {any} query Дополнительные параметры
     * @returns {Promise<ResponseListModel<ChatInfoModel>>}
     */
    public getList(onlyMy: boolean, lastChatGuid?: string, stopChatGuid?: string, query?: any): Promise<ResponseListModel<ChatInfoModel>> {
        return new Promise<ResponseListModel<ChatInfoModel>>((resolve, reject) => {
            let url = `${this.messagesEndPoint}channels/list`;

            if (lastChatGuid) {
                url += `/${lastChatGuid}`;
            }

            if (lastChatGuid && stopChatGuid) {
                url += `/${stopChatGuid}`;
            }

            if (onlyMy) {
                url += `?only-my`;
            }

            let params = new URLSearchParams();

            if (query) {
                let keys = Object.keys(query);

                for (let i = 0; i < keys.length; i++) {
                    const key = keys[i];
                    params.set(key, query[key]);
                }
            }

            this.httpService.get(url, {params: params})
                .map((response: Response) => response.json())
                .map((response: any) => response.data)
                .subscribe(resolve, reject);
        });
    }

    /**
     * Просмотр информации о чате (канала группы)
     * @param {string} chatGuid Идентфикатор чата (канала\группы)
     * @returns {Promise<ChatInfoModel>}
     */
    public getInfo(chatGuid: string): Promise<ChatInfoModel> {
        return new Promise<ChatInfoModel>((resolve, reject) => {
            const url = `${this.messagesEndPoint}channels/${chatGuid}`;

            this.httpService.get(url)
                .map((response: Response) => response.json())
                .map((response: any) => response.data)
                .subscribe(resolve, reject);
        });
    }

    /**
     * Обновление информации о чате (канале\группе)
     * @param {string} chatGuid Идентификатор чата (канала\группы)
     * @param {ChatCreateInfoModel} params Параметры для обновления чата (канала\группы)
     * @returns {Promise<ChatInfoModel>}
     */
    public update(chatGuid: string, params: ChatCreateInfoModel): Promise<ChatInfoModel> {
        return new Promise<ChatInfoModel>((resolve, reject) => {
            const url = `${this.messagesEndPoint}channels/${chatGuid}`;

            this.httpService.put(url, params)
                .map((response: Response) => response.json())
                .map((response: any) => response.data)
                .subscribe(resolve, reject);
        });
    }

    /**
     * Удаление чата (канала\группы)
     * @param {string} chatGuid Идентификатор чата (канала\группы)
     * @returns {Promise<boolean>}
     */
    public remove(chatGuid: string): Promise<boolean> {
        return new Promise<boolean>((resolve, reject) => {
            const url = `${this.messagesEndPoint}channels/${chatGuid}`;

            this.httpService.delete(url)
                .map((response: Response) => response.json())
                .map((response: any) => response.data)
                .subscribe(resolve, reject);
        });
    }

    /**
     * Присоединиться к публичному чату (канала\группы)
     * @param {string} chatGuid Идентификатор чата (канала\группы)
     * @returns {Promise<boolean>}
     */
    public join(chatGuid: string): Promise<boolean> {
        return new Promise<boolean>((resolve, reject) => {
            const url = `${this.messagesEndPoint}channels/${chatGuid}/join`;

            this.httpService.post(url, {})
                .map((response: Response) => response.json())
                .map((response: any) => response.data)
                .subscribe(resolve, reject);
        })
    }

    /**
     * Выход из чата (канала\группы)
     * @param {string} chatGuid Идентификатор чата (канала\группы)
     * @returns {Promise<boolean>}
     */
    public leave(chatGuid: string): Promise<boolean> {
        return new Promise<boolean>((resolve, reject) => {
            const url = `${this.messagesEndPoint}channels/${chatGuid}/leave`;

            this.httpService.delete(url, {})
                .map((response: Response) => response.json())
                .map((response: any) => response.data)
                .subscribe(resolve, reject);
        });
    }

    /**
     * Принять приглашение на вступление в чат
     * @param {string} inviteGuid Идентификатор приглашения
     */
    public accept(inviteGuid: string): Promise<boolean> {
        return new Promise<boolean>((resolve, reject) => {
            const url = `${ChannelsUrls.chatInvite}/${inviteGuid}`;

            this.httpService.post(url, {})
                .map((response: Response) => response.json())
                .map((response: any) => response.data)
                .subscribe(resolve, reject);
        });
    }

    /**
     * Отклонить приглашение на вступление в чат
     * @param {string} inviteGuid Идентификатор приглашения
     */
    public refuse(inviteGuid: string): Promise<boolean> {
        return new Promise<boolean>((resolve, reject) => {
            const url = `${ChannelsUrls.chatInvite}/${inviteGuid}`;

            this.httpService.delete(url, {})
                .map((response: Response) => response.json())
                .map((response: any) => response.data)
                .subscribe(resolve, reject);
        });
    }

    /**
     * Приглашение пользователя в канал
     * @param {string} channelGuid Идентификатор канала
     * @param {string} userGuid Идентификатор пользователя
     * @returns {Promise<MemberInfoModel>}
     */
    public invite(channelGuid: string, userGuid: string): Promise<MemberInfoModel> {
        return new Promise<MemberInfoModel>((resolve, reject) => {
            const url = `${this.messagesEndPoint}channels/${channelGuid}/invite/${userGuid}`;

            this.httpService.post(url, null)
                .map((response: Response) => response.json())
                .map((response: any) => response.data)
                .subscribe(resolve, reject);
        });
    }

    /**
     * Поиск сообщений в чате
     * @param {string} chatGuid Идентификтаор чата
     * @param {string} query Строка поиска
     * @param {string} messageGuid Идентификатор сообщения до которого необходимо искать
     * @returns {Observable<MessageSearchResultModel>}
     */
    public searchMessages(chatGuid: string, query: string, messageGuid?: string): Observable<MessageSearchResultModel> {
        let url = `${this.messagesEndPoint}channels/${chatGuid}/search-messages`;
        if (messageGuid) {
            url += '/' + messageGuid;
        }
        url += '?q=' + query;

        return this.httpService.get(url)
            .map((response: Response) => response.json())
            .map((response: any) => response.data);
    }

    /**
     * Поиск по участникам канала
     * @param channelGuid   Индификатор канала в по списку участников которой производим поиск
     * @param queryText     Текст по которому ищем
     * @param query         Значение по которому производим поиск участников
     */
    public searchMembers(channelGuid: string, queryText: string, query?: any): Promise<ResponseListModel<string>> {
        return new Promise<ResponseListModel<string>>((resolve, reject) => {
            const url: string = `${this.messagesEndPoint}channels/${channelGuid}/members/search`;

            let params = new URLSearchParams();

            params.set('q', queryText);
            if (query) {
                let keys = Object.keys(query);

                for (let i = 0; i < keys.length; i++) {
                    const key = keys[i];
                    params.set(key, query[key]);
                }
            }

            return this.httpService.get(url, {params: params})
                .map((response: Response) => response.json())
                .map((response: any) => response.data)
                .subscribe(resolve, reject);
        });
    }

    /**
     * Поиск по чатам
     * @param {string} queryText Текст для поиска
     * @param {any} query Дополнительные параметры запроса
     */
    public search(queryText: string, query?: any): Promise<ResponseListModel<ChatInfoModel>> {
        return new Promise<ResponseListModel<ChatInfoModel>>((resolve, reject) => {
            const url: string = `${this.messagesEndPoint}channels/search`;

            let params = new URLSearchParams();

            params.set('q', queryText);
            if (query) {
                let keys = Object.keys(query);

                for (let i = 0; i < keys.length; i++) {
                    const key = keys[i];
                    params.set(key, query[key]);
                }
            }

            return this.httpService.get(url, {params: params})
                .map((response: Response) => response.json())
                .map((response: any) => response.data)
                .subscribe(resolve, reject);
        });
    }

    /**
     * Получение истории сообщений
     * @param {string} chatGuid Идентификатор чата (канала\группы)
     * @param {string} messageGuid Идентификатор сообщения, от которого получать историю
     * @param {string} startMessageGuid Идентификатор сообщения, до которого получать историю
     * @param {number} numBefore Количество сообщений которые надо добавить до начального сообщения
     * @param {any} query Дополнительные параметры
     * @returns {Promise<ResponseListModel<ChatMessageModel>>}
     */
    public getHistory(chatGuid: string, messageGuid?: string, startMessageGuid?: string, numBefore?: number, query?: any): Promise<ResponseListModel<ChatMessageModel>> {
        return new Promise((resolve, reject) => {
            let url = `${this.messagesEndPoint}channels/${chatGuid}/history`;

            if (messageGuid) {
                url += `/${messageGuid}`;
                if (startMessageGuid) {
                    url += `/${startMessageGuid}`;
                    if (numBefore) {
                        url += `/${numBefore}`;
                    }
                }
            }

            let params = new URLSearchParams();

            if (query) {
                let keys = Object.keys(query);

                for (let i = 0; i < keys.length; i++) {
                    const key = keys[i];
                    params.set(key, query[key]);
                }
            }

            this.httpService.get(url, {params: params})
                .map((response: Response) => response.json())
                .map((response: any) => response.data)
                .subscribe(resolve, reject);
        });
    }

    /**
     * Получаем список участников канала
     *
     * @param {string} channelGuid  Индификатор канала
     * @param query                 Дополнительные параметры
     */
    public getMembers(channelGuid: string, query?: any): Promise<ResponseListModel<MemberInfoModel>> {
        return new Promise<ResponseListModel<MemberInfoModel>>((resolve, reject) => {
            const url = `${this.messagesEndPoint}channels/${channelGuid}/members`;

            let params = new URLSearchParams();

            if (query) {
                let keys = Object.keys(query);

                for (let i = 0; i < keys.length; i++) {
                    const key = keys[i];
                    params.set(key, query[key]);
                }
            }

            this.httpService.get(url, {params: params})
                .map((response: Response) => response.json())
                .map((response: any) => response.data)
                .subscribe(resolve, reject);
        });
    }

    /**
     * Удаление участника канала
     * @param {string} channelGuid
     * @param {string} userGuid
     * @returns {Promise<boolean>}
     */
    public removeMember(channelGuid: string, userGuid: string): Promise<boolean> {
        return new Promise<boolean>((resolve, reject) => {
            const url = `${this.messagesEndPoint}channels/${channelGuid}/kickoff/${userGuid}`;

            this.httpService.delete(url)
                .map((response: Response) => response.json())
                .map((response: any) => response.data)
                .subscribe(resolve, reject);
        });
    }

    /**
     * Получение информации о статусе приглашения для пользователей канала
     * @param  {string} channelGuid Идентификатор канала
     * @param  {string[]} userGuids Массив идентификаторов пользователей канала
     * @return {Promise<MemberInfoModel[]>}
     */
    public getMembersInfo(channelGuid: string, userGuids: string[]): Promise<MemberInfoModel[]> {
        return new Promise<MemberInfoModel[]>((resolve, reject) => {
            const url = `${this.messagesEndPoint}channels/${channelGuid}/members-info`;

            this.httpService.post(url, userGuids)
                .map((response: Response) => response.json())
                .map((response: any) => response.data)
                .subscribe(resolve, reject);
        });
    }

    /**
     * Установка URL для сервиса сообщений
     * @param messagesEndPoint
     */
    public setMessagesEndPoint(messagesEndPoint: string): void {
        this.messagesEndPoint = messagesEndPoint;
    }

}
