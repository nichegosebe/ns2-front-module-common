import {MessagesHeader} from "./header.model";
import {SocketMessageInterface} from "../../../intefraces/socket-message.interface";

/**
 * Абстрактная модель сообщения
 */
export abstract class AbstractMessage implements SocketMessageInterface {
    header: MessagesHeader = new MessagesHeader();
    payload: any;
}