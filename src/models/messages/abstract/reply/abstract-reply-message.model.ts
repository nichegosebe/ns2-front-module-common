import {AbstractMessage} from "../abstract-message.model";
import {MESSAGE_HEADER_TYPE_MESSAGE, MESSAGE_HEADER_ACTION_REPLY} from "../const";
import {ReplyMessagePayload} from "./reply-message-payload.model";

/**
 * Базовая модель ответа на сообщение
 */
export abstract class AbstractReplyMessage extends AbstractMessage {
    payload: ReplyMessagePayload = new ReplyMessagePayload();

    constructor() {
        super();
        this.header.type = MESSAGE_HEADER_TYPE_MESSAGE;
        this.header.action = MESSAGE_HEADER_ACTION_REPLY;
    }
}