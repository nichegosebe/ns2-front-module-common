/**
 * Модель информации об участнике чата/канала/группы
 */

export class MemberInfoModel {
    guid: string;           // Уникальный идентификатор записи
    created: number;        // Дата/время создания записи
    updated: number;        // Дата/время последней модификации записи
    creator_guid: string;   // Идентификатор создателя
    updater_guid: string;   // Идентификатор обновителя
    chat_guid: string;      // Идентификатор чата
    chat_type: string;      // Тип чата (пир, канал, группа)
    user_guid: string;      // Идентификатор пользователя
    is_accepted: boolean;   // Признак, что приглашение принято
    decision_date: number;  // Дата вступления в группу
    is_locked: boolean;     // Признак заблокированного пользователя в группе
    lock_date: number;      // Дата блокировки пользователя
    unlooked: number;       // Количество не просмотренных сообщений
    is_online: boolean;     // Признак онлайн у пользователя в группе
    inviter_guid: string;   // Идентификатор пользователя, создавшего приглашение
}
