import { Inject, Injectable, Injector } from "@angular/core";
import { Response, URLSearchParams } from "@angular/http";
import { UUID } from "angular2-uuid";

// Services
import { CookieService } from "ngx-cookie-service";
import { HttpService } from "./http.service";

// Models
import { SocialModel } from '../models/social.model';
import { PhoneModel } from "../models/phone.model";
import { ResponseListModel } from "../models/response-list.model";

// Interfaces
import { RealtorInterface } from "../intefraces/realtor.interface";
import { StorageInterface } from "../intefraces/storage.interface";
import { CurrentUserInfoInterface } from "../intefraces/current-user-info.interface";
import { RegisterInfoInterface } from "../intefraces/register-info.interface";
import { HttpErrorInterface } from "../intefraces/http-error.interface";

// Urls
import { CurrentUserUrls } from '../urls/current-user-urls.model';

// RxJs
import { Subscription } from "rxjs";
import { Observable } from 'rxjs/Observable';
import { Observer } from 'rxjs/Observer';
import "rxjs/operator/map";

// RxMq
import Rxmq from "rxmq";

// Имя ключа в хранилище (localStorage или sessionStorage)
export const STORAGE_TOKEN_KEY = 'token';

@Injectable()
export class CurrentUserService {
    public static ESB_CHANNEL_NAME = 'current-user';
    public static ESB_SESSION_CHANGE_CITY = 'session.change-city';
    public static ESB_INFO_UPDATE = 'info.update';
    public static ESB_USER_LOGIN = 'user.login';
    public static ESB_USER_LOGOUT = 'user.logout';
    public static USER_TOKEN_NAME = 'X-User-Token';

    private token: string = '';
    private info!: CurrentUserInfoInterface;
    private usersEndPoint: string = '';
    private ESBChannel: any;

    private temporaryDeviceUUID: string = '';

    constructor(private injector: Injector,
        @Inject('StorageInterface') private persistStorage: StorageInterface,
        @Inject('StorageInterface') private tempStorage: StorageInterface,
        private cookiesService: CookieService) {
        this.ESBChannel = Rxmq.channel(CurrentUserService.ESB_CHANNEL_NAME);
    }

    // Получение соц сетей
    public getSocials(): Promise<SocialModel[]> {
        return new Promise((resolve, reject) => {
            let url = this.usersEndPoint + 'self/socials';

            this.injector.get(HttpService).get(url)
                .map((response: Response) => response.json())
                .map((response: any) => response.data)
                .subscribe((data: SocialModel[]) => {
                    resolve(data);
                }, (err: HttpErrorInterface) => {
                    reject(err);
                });
        })
    }

    /**
     * Предзагрузка
     *
     * @returns {Promise<string[]>}
     */
    public load(): Promise<any> {
        return new Promise((resolve, reject) => {
            this.restoreToken()
                .then((token) => {
                    let tokenResolve = () => {
                        resolve(token);
                    };
                    this.loadInfoRemote().then(tokenResolve)
                        .catch((err: HttpErrorInterface) => {
                            (err.statusCode == 401) ? tokenResolve() : reject(err);
                        });
                })
                .catch(resolve);
        });
    }

    /**
     * Установка URL до сервиса users
     *
     * @param usersEndPoint
     */
    public setUsersEndPoint(usersEndPoint: string) {
        this.usersEndPoint = usersEndPoint;
    }

    /**
     * Проверка текущей сессии пользователя
     *
     * @returns {Promise<T>}
     */
    public isAuth(): boolean {
        return !!this.getToken();
    }

    /**
     * Авторизация в системе
     *
     * @param email
     * @param password
     * @param rememberMe
     */
    public login(email: string, password: string, rememberMe: boolean): Promise<any> {
        return new Promise((resolve, reject) => {
            let url = this.usersEndPoint + 'users/login';
            let body = { email, password };

            this.injector.get(HttpService).post(url, body)
                .map((response: Response) => response.json())
                .map((response: any) => response.data)
                .subscribe(
                    (token: string) => {
                        this.storeToken(token, rememberMe)
                            .then((token) => {
                                this.token = token;
                                this.loadInfoRemote()
                                    .then(() => {
                                        resolve();
                                    })
                                    .catch((err: HttpErrorInterface) => {
                                        reject(err);
                                    });
                            })
                            .catch(() => {
                                this.token = '';
                                reject()
                            });
                    },
                    (err: HttpErrorInterface) => {
                        reject(err)
                    }
                );
        });
    }

    /**
     * Регистрация нового пользователя
     *
     * @param email
     * @param password
     * @param password_repeat
     * @param firstname
     * @param lastname
     * @param geo_guid
     * @param phone
     */
    public register(email: string,
        password: string,
        password_repeat: string,
        firstname: string,
        lastname: string,
        geo_guid: string,
        phone: string): Promise<RegisterInfoInterface> {
        return new Promise((resolve, reject) => {
            let url = this.usersEndPoint + 'users/register';
            let body = { email, password, password_repeat, firstname, lastname, geo_guid, phone };

            this.injector.get(HttpService).post(url, body)
                .map((response: Response) => response.json())
                .map((response: any) => response.data)
                .subscribe(
                    (data: RegisterInfoInterface) => {
                        resolve(data);
                    },
                    (err: HttpErrorInterface) => {
                        reject(err)
                    }
                )
        });
    }

    /**
     * Проверка email адреса на первом этапе регистрации
     *
     * @param email
     * @returns {Promise<T>}
     */
    public registerCheckEmail(email: string): Promise<string> {
        return new Promise((resolve, reject) => {
            let url = this.usersEndPoint + 'users/register-check-email';
            let params = new URLSearchParams();
            params.set('email', email);

            this.injector.get(HttpService).get(url, {
                search: params
            })
                .map((response: Response) => response.json())
                .map((response: any) => response.data)
                .subscribe(
                    (correctEmail: string) => {
                        resolve(correctEmail)
                    },
                    (err: HttpErrorInterface) => {
                        reject(err)
                    }
                )
        });
    }

    /**
     * Замена номера телефона при переходе с третьего шага на второй
     *
     * @param user_guid
     * @param phone
     * @returns {Promise<T>}
     */
    public registerChangePhone(user_guid: string, phone: string): Promise<string> {
        return new Promise((resolve, reject) => {
            let url = this.usersEndPoint + 'users/register-change-phone';
            let body = { user_guid, phone };

            this.injector.get(HttpService).put(url, body)
                .map((response: Response) => response.json())
                .map((response: any) => response.data)
                .subscribe(
                    (newPhone: string) => {
                        resolve(newPhone)
                    },
                    (err: HttpErrorInterface) => {
                        reject(err)
                    }
                );
        })
    }

    /**
     * Проверка кода подтверждения из СМС
     *
     * @param user_guid
     * @param code
     * @returns {Promise<T>}
     */
    public confirm(user_guid: string, code: string): Promise<RegisterInfoInterface> {
        return new Promise((resolve, reject) => {
            let url = this.usersEndPoint + 'users/confirm';
            let body = { user_guid, code };

            this.injector.get(HttpService).post(url, body)
                .map((response: Response) => response.json())
                .map((response: any) => response.data)
                .subscribe(
                    (data: RegisterInfoInterface) => {
                        resolve(data)
                    },
                    (err: HttpErrorInterface) => {
                        reject(err)
                    }
                );
        });
    }

    // Загрузка информации о текущем пользователе
    public loadInfoRemote(): Promise<CurrentUserInfoInterface> {
        return new Promise((resolve, reject) => {
            let url = CurrentUserUrls.self;

            this.injector.get(HttpService).get(url)
                .map((response: Response) => response.json())
                .map((response: any) => response.data)
                .subscribe(
                    (info: CurrentUserInfoInterface) => {
                        this.info = info;
                        this.ESBChannel.subject(CurrentUserService.ESB_USER_LOGIN).next(info);
                        resolve(info);
                    },
                    (err: HttpErrorInterface) => {
                        reject(err)
                    }
                )
        });
    }

    /**
     * Запрос ссылки сброса пароля на почту
     *
     * @param email
     * @returns {Promise<T>}
     */
    public reminderPassword(email: string): Promise<any> {
        return new Promise((resolve, reject) => {
            let url = this.usersEndPoint + 'users/reminder-password';
            let body = { email };

            this.injector.get(HttpService).post(url, body)
                .map((response: Response) => response.json())
                .map((response: any) => response.data)
                .subscribe(
                    (result: any) => {
                        resolve(result);
                    },
                    (err: HttpErrorInterface) => {
                        reject(err)
                    }
                );
        });
    }

    /**
     * Проверка токена восстановления пароля. Используется при открытии страницы восстановления, токен передается
     * в параметре урла
     *
     * @param token
     * @returns {Promise<T>}
     */
    public restorePasswordCheck(token: string): Promise<any> {
        return new Promise((resolve, reject) => {
            let url = this.usersEndPoint + 'users/restore-password-check';
            let params = new URLSearchParams();

            params.set('token', token);

            this.injector.get(HttpService).get(url, { search: params })
                .map((response: Response) => response.json())
                .map((response: any) => response.data)
                .subscribe(
                    (result: any) => {
                        resolve(result);
                    },
                    (err: HttpErrorInterface) => {
                        reject(err)
                    }
                );
        });
    }

    /**
     * Восстановление пароля
     *
     * @param token
     * @param password
     * @param password_repeat
     * @returns {Promise<T>}
     */
    public restorePassword(token: string, password: string, password_repeat: string): Promise<any> {
        return new Promise((resolve, reject) => {
            let url = this.usersEndPoint + 'users/restore-password';
            let body = { token, password, password_repeat };

            this.injector.get(HttpService).put(url, body)
                .map((response: Response) => response.json())
                .map((response: any) => response.data)
                .subscribe(
                    (result: any) => {
                        resolve(result);
                    },
                    (err: HttpErrorInterface) => {
                        reject(err)
                    }
                );
        });
    }

    /**
     * Установка города
     *
     * @param geo_guid
     * @returns {Promise<T>}
     */
    public setGeo(geo_guid: string): Promise<any> {
        return new Promise((resolve, reject) => {
            let url = this.usersEndPoint + 'self/geo';
            let body = { geo_guid };

            this.injector.get(HttpService).put(url, body)
                .map((response: Response) => response.json())
                .map((response: any) => response.data)
                .subscribe(
                    (result: any) => {
                        this.info.session.geo_guid = result.geo_guid;
                        this.ESBChannel.subject(CurrentUserService.ESB_SESSION_CHANGE_CITY).next({
                            oldGeoGuid: this.info.session.geo_guid,
                            newGeoGuid: result.geo_guid
                        });
                        resolve(result);
                    },
                    (err: HttpErrorInterface) => {
                        reject(err)
                    }
                );
        })
    }

    /**
     * Добавляет подписчика на смену текущего города
     * @param {Function} then Функция-подписчик
     * @returns {Subscription}
     */
    public onGeoChange(then: (oldGeoGuid: string, newGeoGuid: string) => void): Subscription {
        return this.ESBChannel.observe(CurrentUserService.ESB_SESSION_CHANGE_CITY).subscribe(then);
    }

    /**
     * Добавляет подписчика на авторизацию пользователя
     * @param {Function} then Функция-подписчик
     * @returns {Subscription}
     */
    public onLogin(then: (value: CurrentUserInfoInterface) => void): Subscription {
        return this.ESBChannel.observe(CurrentUserService.ESB_USER_LOGIN).subscribe(then);
    }

    /**
     * Добавляет подписчика на выход пользователя из системы
     * @param {Function} then Функция-подписчик
     * @returns {Subscription}
     */
    public onLogout(then: () => void): Subscription {
        return this.ESBChannel.observe(CurrentUserService.ESB_USER_LOGOUT).subscribe(then);
    }

    /**
     * Выход из аккаунта
     *
     * @returns {Promise<T>}
     */
    public logout(): Promise<any> {
        return new Promise((resolve, reject) => {
            let url = this.usersEndPoint + 'self/logout';
            let handler = () => {
                this.ESBChannel.subject(CurrentUserService.ESB_USER_LOGOUT).next();
                this.localLogout().then(resolve).catch(reject);
            };

            this.injector.get(HttpService).delete(url).subscribe(handler, handler)
        });
    }

    public localLogout(): Promise<any> {
        this.token = '';
        this.cookiesService.delete(CurrentUserService.USER_TOKEN_NAME);
        return Promise.race([
            this.persistStorage.remove(STORAGE_TOKEN_KEY),
            this.persistStorage.removeByTag(STORAGE_TOKEN_KEY),
            this.tempStorage.remove(STORAGE_TOKEN_KEY),
            this.tempStorage.removeByTag(STORAGE_TOKEN_KEY)
        ]);
    }

    /**
     * Обновить профиль текущего пользователя
     * @param newUserData Новые данные пользователя
     */
    public saveProfile(newUserData: FormData): Observable<CurrentUserInfoInterface> {
        return new Observable((observer: Observer<CurrentUserInfoInterface>): void => {
            this.injector.get(HttpService)
                .post(CurrentUserUrls.self, newUserData)
                .map((response: Response) => response.json())
                .map((response: any) => response.data)
                .subscribe(
                    (response: CurrentUserInfoInterface) => {
                        this.info = response;

                        observer.next(response);
                        observer.complete();
                    },
                    (error: HttpErrorInterface) => {
                        observer.error(error);
                        observer.complete();
                    }
                );
        });
    }

    /**
     * Отправить данные пользователя на модерацию
     * @param userData Данные пользователя
     */
    public sendOnModeration(userData: FormData): Observable<CurrentUserInfoInterface> {
        return new Observable((observer: Observer<CurrentUserInfoInterface>): void => {
            this.injector.get(HttpService)
                .post(CurrentUserUrls.moderation, userData)
                .map((response: Response) => response.json())
                .map((response: any) => response.data)
                .subscribe(
                    (response: CurrentUserInfoInterface) => {
                        this.info = response;

                        observer.next(response);
                        observer.complete();
                    },
                    (error: HttpErrorInterface) => {
                        observer.error(error);
                        observer.complete();
                    }
                );
        });
    }

    /**
     * Сохранить токен в локал сторадж
     *
     * @param token
     * @param rememberMe
     */
    private storeToken(token: string, rememberMe: boolean): Promise<string> {
        if (rememberMe) {
            return this.persistStorage.set(STORAGE_TOKEN_KEY, token);
        } else {
            return this.tempStorage.set(STORAGE_TOKEN_KEY, token);
        }
    }

    /**
     * Восстанавливает значение токена из хранилища
     */
    private restoreToken(): Promise<string> {
        return new Promise((resolve, reject) => {
            const cookieToken = this.cookiesService.get(CurrentUserService.USER_TOKEN_NAME);

            if (cookieToken) {
                this.token = cookieToken;
                this.storeToken(this.token, true)
                    .then(() => {
                        resolve(cookieToken);
                        this.cookiesService.delete(CurrentUserService.USER_TOKEN_NAME);
                    });
            } else {
                this.persistStorage.get(STORAGE_TOKEN_KEY)
                    .then((token) => {
                        this.token = token;
                        resolve(token);
                    })
                    .catch(() => {
                        this.tempStorage.get(STORAGE_TOKEN_KEY)
                            .then((token) => {
                                this.token = token;
                                resolve(token);
                            })
                            .catch(() => {
                                reject();
                            });
                    });
            }
        });
    }

    // Установить причину отказа модерации
    public setRejectionReason(reason: string): void {
        this.info.reject_reason = reason;
    }

    /**
     * Получить токен текущего пользователя
     *
     * @returns {string}
     */
    public getToken(): string {
        return this.token;
    }

    /**
     * Установить токен пользователю
     *
     * @param token
     */
    public setToken(token: string): void {
        this.token = token;
    }

    /**
     * Получить информацию о текущем пользователе
     *
     * @returns {CurrentUserInfoInterface}
     */
    public getInfo(): CurrentUserInfoInterface {
        return this.info;
    }

    /**
     * Метод отправки приглашения пользователю
     * @param {string} email E-mail пользователя которого необходимо пригласить
     * @returns {Promise<T>}
     */
    public invite(email: string): Promise<any> {
        return new Promise((resolve, reject) => {
            let url = this.usersEndPoint + 'self/invite';

            this.injector.get(HttpService).post(url, { email: email })
                .map((response: Response) => response.json())
                .map((response: any) => response.data)
                .subscribe(() => {
                    resolve();
                }, (err: HttpErrorInterface) => {
                    reject(err);
                });
        });
    }

    /**
     * Получить всех партнеров текущего пользователя
     *
     * @returns {Promise<T>}
     */
    public getPartners(): Promise<ResponseListModel<RealtorInterface>> {
        return new Promise((resolve, reject) => {
            let url = this.usersEndPoint + 'self/partners';

            this.injector.get(HttpService).get(url)
                .map((response: Response) => response.json())
                .map((response: any) => response.data)
                .subscribe(resolve, reject);
        });
    }

    /**
     * Добавить избранных партнеров текущему пользователю
     *
     * @returns {Promise<T>}
     */
    public addPartners(partnersGuid: string[]): Promise<boolean> {
        return new Promise((resolve, reject) => {
            let url = this.usersEndPoint + 'self/partners';

            this.injector.get(HttpService).put(url, partnersGuid)
                .map((response: Response) => response.json())
                .map((response: any) => response.data)
                .subscribe(resolve, reject);
        });
    }

    /**
     * Установить избранных партнеров текущему пользователю
     *
     * @returns {Promise<T>}
     */
    public setPartners(partnersGuid: string[]): Promise<boolean> {
        return new Promise((resolve, reject) => {
            let url = this.usersEndPoint + 'self/partners';

            this.injector.get(HttpService).post(url, partnersGuid)
                .map((response: Response) => response.json())
                .map((response: any) => response.data)
                .subscribe(resolve, reject);
        });
    }

    /**
     * Удалить избранного пользователя
     *
     * @returns {Promise<T>}
     */
    public deletePartner(partnerGuid: string): Promise<boolean> {
        return new Promise((resolve, reject) => {
            let url = this.usersEndPoint + 'self/partner/' + partnerGuid;

            this.injector.get(HttpService).delete(url)
                .map((response: Response) => response.json())
                .map((response: any) => response.data)
                .subscribe(resolve, reject);
        });
    }

    /**
     * Возвращает временный уникальный идентификатор устройства
     * @returns {string}
     */
    public getTemporaryDeviceUUID(): string {
        if (!this.temporaryDeviceUUID) {
            this.temporaryDeviceUUID = UUID.UUID();
        }
        return this.temporaryDeviceUUID;
    }

    // Пользователь является кандидатом
    public get isCandidate(): boolean {
        if (!this.info){
            return true;
        }
        return this.info.rank === 'candidate';
    }
}
