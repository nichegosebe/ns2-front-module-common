import { CommonConfigService } from '../services/common-config.service';

// Базовый класс для URL
// @dynamic
export abstract class BasicUrls {
	// Endpoint
	public static get endPoint(): string {
		return CommonConfigService.gw;
	}
}
