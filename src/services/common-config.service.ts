// Сервис конфигурации
// @dynamic
export class CommonConfigService {

	private static _gw = '';

	private static _storage = '';

	public static get gw(): string {
		return CommonConfigService._gw;
	}

	public static set gw(value: string) {
		CommonConfigService._gw = value;
	}

	public static get storage(): string {
		return CommonConfigService._storage;
	}

	public static set storage(value: string) {
		CommonConfigService._storage = value;
	}
}
