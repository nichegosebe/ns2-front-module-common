import {async, TestBed, inject} from "@angular/core/testing";
import {HttpModule, RequestOptions, Response, ResponseOptions} from "@angular/http";
import {HttpService} from "./http.service";
import {Injector} from "@angular/core";
import {MockBackend} from "@angular/http/testing";
import {GeoService} from "./geo.service";
import {CurrentUserService} from "./current-user.service";
import {LocalStorageService} from "./local-storage.service";
import {SessionStorageService} from "./session-storage.service";

describe('GeoService', () => {

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [HttpModule],
            providers: [
                {
                    provide: HttpService,
                    useFactory: (injector: Injector, mockBackend: MockBackend, requestOptions: RequestOptions) => {
                        return new HttpService(injector, mockBackend, requestOptions);
                    },
                    deps: [Injector, MockBackend, RequestOptions]
                },
                MockBackend,
                LocalStorageService,
                SessionStorageService,
                {
                    provide: CurrentUserService,
                    useFactory: (injector: Injector, localStorage: LocalStorageService, sessionStorage: SessionStorageService) => {
                        return new CurrentUserService(injector, localStorage, sessionStorage);
                    },
                    deps: [Injector, LocalStorageService, SessionStorageService]
                },
                {
                    provide: GeoService,
                    useFactory: (httpService: HttpService) => {
                        return new GeoService(httpService);
                    },
                    deps: [HttpService]
                }
            ]
        });
    }));


    it('refName defined',
        inject([GeoService], (geoService) => {
            expect(geoService.refName == 'geo').toBeTruthy();
        })
    );

    it('getCountries request is ok',
        inject([GeoService, MockBackend], (geoService, mockBackend) => {
            mockBackend.connections.subscribe((connection) => {
                expect(connection.request.url).toContain('level=1');
                expect(connection.request.url).toContain('sort=title');
            });

            geoService.getCountries();
        })
    );

    it('getCities request is ok',
        inject([GeoService, MockBackend], (geoService, mockBackend) => {
            mockBackend.connections.subscribe((connection) => {
                expect(connection.request.url).toContain('level=2');
                expect(connection.request.url).toContain('sort=title');
            });

            geoService.getCities('25392d76-53bd-448a-976a-0ecebdba2dcc');
        })
    );

    it('getList return normal data',
        inject([GeoService, MockBackend], (geoService, mockBackend) => {
            let response = {
                data: {
                    items: [{fieldName: "fieldValue"}]
                }
            };

            mockBackend.connections.subscribe((connection) => {
                connection.mockRespond(new Response(new ResponseOptions({
                    body: JSON.stringify(response)
                })));
            });

            geoService.getCountries().then((data) => {
                expect(data).toEqual(response.data.items);
            });
        })
    );
});