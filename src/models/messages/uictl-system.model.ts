import {AbstractMessage} from "./abstract/abstract-message.model";
import {MESSAGE_HEADER_TYPE_SYSTEM, MESSAGE_HEADER_ACTION_UICTL} from "./abstract/const";

/**
 * Модель уведомления о необходимости выполнить команду на фронтенде
 */
export class UictlSystem extends AbstractMessage {
    payload: {
        action: string,
        params: {
            name: string
        }
    };

    constructor() {
        super();
        this.header.type = MESSAGE_HEADER_TYPE_SYSTEM;
        this.header.action = MESSAGE_HEADER_ACTION_UICTL;
    }
}