import {MESSAGE_RCPT_GROUP} from "./abstract/const";
import {AbstractSeenMessage} from "./abstract/seen/abstract-seen-message.model";

/**
 * Модель уведомления о просмотре сообщения в группе
 */
export class SeenMessageGroup extends AbstractSeenMessage {
    constructor() {
        super();
        this.header.rcpt = MESSAGE_RCPT_GROUP;
    }
}