import {AbstractMessage} from "../abstract-message.model";
import {MESSAGE_HEADER_TYPE_SYSTEM_USER, MESSAGE_HEADER_ACTION_STATUS} from "../const";
import {StatusSystemUserPayload} from "./status-system-user-payload.model";

/**
 * Базовая модель смены статуса пользователя
 */
export abstract class AbstractStatusSystemUser extends AbstractMessage {
    payload: StatusSystemUserPayload = new StatusSystemUserPayload();

    constructor() {
        super();
        this.header.type = MESSAGE_HEADER_TYPE_SYSTEM_USER;
        this.header.action = MESSAGE_HEADER_ACTION_STATUS;
    }
}