import {AbstractMessage} from "../abstract-message.model";
import {MESSAGE_HEADER_TYPE_MESSAGE, MESSAGE_HEADER_ACTION_DELETE} from "../const";
import {DeleteMessagePayload} from "./delete-message-payload.model";

/**
 * Базовая модель удаления сообщения
 */
export abstract class AbstractDeleteMessage extends AbstractMessage {
    payload: DeleteMessagePayload = new DeleteMessagePayload();

    constructor() {
        super();
        this.header.type = MESSAGE_HEADER_TYPE_MESSAGE;
        this.header.action = MESSAGE_HEADER_ACTION_DELETE;
    }
}