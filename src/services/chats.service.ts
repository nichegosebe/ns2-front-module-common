import {Injectable} from '@angular/core';
import {Response, URLSearchParams} from '@angular/http';
import {HttpService} from './http.service';
import {ChatInfoModel} from '../models/chat-info.model';
import {ResponseListModel} from '../models/response-list.model';

@Injectable()
export class ChatsService {
    /**
     * Base URL (точка входа) в микросервис сообщений
     */
    private messagesEndPoint: string;

    constructor(private httpService: HttpService) {
    }

    /**
     * Получение всех чатов для текущего пользователя
     * @param {string} lastChatGuid Идентфиикатор последнего чата
     * @param {string} stopChatGuid Идентификатор чата, до которого получить информацию
     * @param {any} query Дополнительные параметры
     * @returns {Promise<ResponseListModel<ChatInfoModel>>}
     */
    public getAllChats(lastChatGuid?: string, stopChatGuid?: string, query?: any): Promise<ResponseListModel<ChatInfoModel>> {
        return new Promise<ResponseListModel<ChatInfoModel>>((resolve, reject) => {
            let url = `${this.messagesEndPoint}chats/get-all-chats`;

            if (lastChatGuid) {
                url += `/${lastChatGuid}`;

                if (stopChatGuid) {
                    url += `/${stopChatGuid}`;
                }
            }

            let params: URLSearchParams = new URLSearchParams();

            if (query) {
                let keys = Object.keys(query);

                for (let i = 0; i < keys.length; i++) {
                    const key = keys[i];
                    params.set(key, query[key]);
                }
            }

            this.httpService.get(url, {params: params})
                .map((response: Response) => response.json())
                .map((response: any) => response.data)
                .subscribe(resolve, reject);
        });
    }

    /**
     * Получение всех чатов для пересылки сообщений
     * @param {string} lastChatGuid Идентификатор последнего чата
     * @param {string} stopChatGuid Идентификатор чата, до которого получить информацию
     * @param {any} query Дополнительные параметры
     * @returns {Promise<ResponseListModel<ChatInfoModel>>}
     */
    public getForwardedChats(lastChatGuid?: string, stopChatGuid?: string, query?: any): Promise<ResponseListModel<ChatInfoModel>> {
        return new Promise<ResponseListModel<ChatInfoModel>>((resolve, reject) => {
            let url = `${this.messagesEndPoint}chats/get-forwarded-chats`;

            if (lastChatGuid) {
                url += `/${lastChatGuid}`;

                if (stopChatGuid) {
                    url += `/${stopChatGuid}`;
                }
            }

            let params: URLSearchParams = new URLSearchParams();

            if (query) {
                let keys = Object.keys(query);

                for (let i = 0; i < keys.length; i++) {
                    const key = keys[i];
                    params.set(key, query[key]);
                }
            }

            this.httpService.get(url, {params: params})
                .map((response: Response) => response.json())
                .map((response: any) => response.data)
                .subscribe(resolve, reject);
        });
    }

    /**
     * Поиск по всем чатам текущего пользователя
     * @param {string} queryText Поисковой запрос
     * @param {any} query Дополнительные параметры запроса
     * @returns {Promise<ChatInfoModel[]>}
     */
    public searchAllChats(queryText: string, query?: any): Promise<ResponseListModel<ChatInfoModel>> {
        return new Promise<ResponseListModel<ChatInfoModel>>((resolve, reject) => {
            const url = `${this.messagesEndPoint}chats/search?q=${queryText}`;

            let params = new URLSearchParams();

            if (query) {
                let keys = Object.keys(query);

                for (let i = 0; i < keys.length; i++) {
                    const key = keys[i];
                    params.set(key, query[key]);
                }
            }

            return this.httpService.get(url, {params: params})
                .map((response: Response) => response.json())
                .map((response: any) => response.data)
                .subscribe(resolve, reject);
        });
    }

    /**
     * Поиск по всем чатам текущего пользователя
     * @param {string} queryText Поисковой запрос
     * @param {any} query Дополнительные параметры запроса
     * @returns {Promise<ChatInfoModel[]>}
     */
    public searchForwardedChats(queryText: string, query?: any): Promise<ResponseListModel<ChatInfoModel>> {
        return new Promise<ResponseListModel<ChatInfoModel>>((resolve, reject) => {
            const url = `${this.messagesEndPoint}chats/forwarded-search?q=${queryText}`;

            let params = new URLSearchParams();

            if (query) {
                Object.keys(query).forEach(key => {
                    params.set(key, query[key]);
                });
            }

            return this.httpService.get(url, {params: params})
                .map((response: Response) => response.json())
                .map((response: any) => response.data)
                .subscribe(resolve, reject);
        });
    }

    /**
     * Установка точки входа в микросервис сообщений
     * @param {string} endPoint
     */
    public setMessagesEndPoint(endPoint: string): void {
        this.messagesEndPoint = endPoint;
    }
}
