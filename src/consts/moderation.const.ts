// Статусы модерации
// В процессе
export const MODERATION_PROCESSING = 'processing';

// Отменено
export const MODERATION_CANCELED = 'canceled';

// Принято
export const MODERATION_ACCEPTED = 'accepted';

// Отказано
export const MODERATION_DECLINED = 'declined';
