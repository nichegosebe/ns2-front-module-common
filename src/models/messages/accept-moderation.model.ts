import { AbstractMessage } from './abstract/abstract-message.model';
import {
    MESSAGE_HEADER_TYPE_MODERATION,
    MESSAGE_HEADER_ACTION_ACCEPT
} from './abstract/const';

// Модель уведомления об успешной модерации профиля
export class AcceptModeration extends AbstractMessage {
    payload: {
        operation_guid: string,
    };

    constructor() {
        super();
        this.header.type = MESSAGE_HEADER_TYPE_MODERATION;
        this.header.action = MESSAGE_HEADER_ACTION_ACCEPT;
    }
}
