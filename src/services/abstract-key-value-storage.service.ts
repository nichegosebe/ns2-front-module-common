import {StorageInterface} from "../intefraces/storage.interface";
import {KeyValueCacheStructure} from "../models/key-value-cache-structure.model";
import {Injectable} from "@angular/core";
import {Md5} from 'ts-md5/dist/md5';
import {STORAGE_TOKEN_KEY} from "./current-user.service";

@Injectable()
/**
 * Базовый сервис для key-value хранилищ
 */
export abstract class AbstractKeyValueStorage implements StorageInterface {

    // Количество наименее редко используемых записей (в процентах) которые будут удалены при очередной очистке
    public static readonly CLEAN_RECORDS_PERCENT = 30;

    // Префикс ключей при хранении тегов
    public static readonly TAGS_PREFIX: string = 'T_';

    // Префикс ключей при хранении данных
    public static readonly DATA_PREFIX: string = 'D_';

    // Флаг, обозначающий что процесс очистки выполняется
    protected maintenanceProcessing: boolean = false;

    // Список ключей которые нельзя удалять при процедуре очистки
    protected maintenanceExceptKeys: string[] = [STORAGE_TOKEN_KEY];

    /**
     * Сохранение данных по ключу на указанное время, с учетом переданных тегов.
     *      При ошибке сохранения запускается метод очистки хранилища (т.к. ошибка могла произойти из-за нехватки места)
     *      и после очистки сохранение пытается выполниться еще раз.
     *
     * @param {string} key Ключ, по которому необходимо сохранить данные
     * @param {any} value Данные для сохранения
     * @param {number} expired Количество секунд в течение которых данные будут храниться
     * @param {string[]} tags Массив тегов, которыми необходимо пометить набор данных
     * @returns {Promise<T>}
     */
    public set(key: string, value: any, expired?: number, tags?: string[]): Promise<any> {
        let item = new KeyValueCacheStructure(expired, tags, value);
        return new Promise((resolve, reject) => {
            let res = () => {
                resolve(value);
            };
            this.setInternal(key, item)
                .then(res)
                .catch(() => {
                    this.maintenance()
                        .then(() => {
                            this.setInternal(key, item).then(res).catch(reject);
                        })
                        .catch(reject);
                });
        });
    }

    /**
     * Получение значения по ключу.
     * Если значение существует, но истек срок его хранения - удаляем эту запись и все с ней связанное из хранилища.
     *
     * @param {string} key Ключ, по которому необходимо получить значение
     * @returns {Promise<T>}
     */
    public get(key: string): Promise<any> {
        return new Promise((resolve, reject) => {
            this._get(this.prepareKey(key)).then((value) => {
                try {
                    let item = KeyValueCacheStructure.unserialize(value);
                    if (item.isExpired()) {
                        this.removeInternal(key, item.tags).then(reject).catch(reject);
                    } else {
                        item.hits = item.hits + 1;
                        this.setInternal(key, item).then(() => {
                            resolve(item.value || null);
                        }).catch(() => {
                            resolve(item.value || null);
                        })
                    }
                } catch (err) {
                    reject(err);
                }
            }).catch(reject);
        });
    }

    /**
     * Удаление данных из хранилища по указанному тегу.
     *
     * @param {string} tag Имя тега по которому необходимо удалить данные
     * @returns {Promise<T>}
     */
    public removeByTag(tag: string): Promise<any> {
        return new Promise((resolve, reject) => {
            let depends: Promise<any>[] = [];
            let realTagName = this.prepareTag(tag);
            this._get(realTagName).then((keysListJson: string) => {
                let keysList: string[] = JSON.parse(keysListJson);
                depends = keysList.map((keyName: string) => {
                    return new Promise((res, rej) => {
                        let realKeyName = this.prepareKey(keyName);
                        this._get(realKeyName).then((value) => {
                            let item = KeyValueCacheStructure.unserialize(value);
                            this.removeInternal(keyName, item.tags).then(res).catch(rej);
                        }).catch(rej);
                    });
                });
                let rmDepends = () => {
                    Promise.all(depends).then(resolve).catch(resolve);
                };
                this._remove(realTagName).then(rmDepends).catch(rmDepends);
            }).catch(reject);
        });
    }

    /**
     * Удаление данных из хранилища по ключу.
     *
     * @param {string} key Ключ по которым необходимо удалить данные
     * @returns {Promise<T>}
     */
    public remove(key: string): Promise<any> {
        return new Promise((resolve, reject) => {
            let realKeyName = this.prepareKey(key);
            this._get(realKeyName).then((jsonItem) => {
                let item = KeyValueCacheStructure.unserialize(jsonItem);
                this.removeInternal(key, item.tags).then(resolve).catch(reject);
            }).catch(reject);
        });
    }

    /**
     * Очистка всего хранилища.
     *
     * @returns {Promise<any>}
     */
    public clear(): Promise<any> {
        return this._clear();
    }

    /**
     * Очистка неиспользуемых/редко используемых данных, освобождение места.
     *
     * @returns {Promise<any>}
     */
    public maintenance(): Promise<any> {
        return new Promise((resolve, reject) => {
            if (!this.maintenanceProcessing) {
                this._getStorageInstance().then((storage) => {
                    this.maintenanceProcessing = true;
                    let keys: string[] = Object.keys(storage);
                    const CLEAN_RECORDS = Math.floor(keys.length * AbstractKeyValueStorage.CLEAN_RECORDS_PERCENT / 100);
                    let list2Clean: any = {};
                    let toCleanLength: number = 0;
                    let minKeyName = '';
                    let minHits = 0;
                    let cleanOnlyEmpty = true;
                    for (let i = 0; i < keys.length; i++) {
                        let keyName = keys[i];
                        if (keyName.indexOf(AbstractKeyValueStorage.DATA_PREFIX) === 0) {
                            let originalName = keyName.substring(AbstractKeyValueStorage.DATA_PREFIX.length),
                                skipKey = (this.maintenanceExceptKeys.indexOf(originalName) > -1);
                            let jsonInfo = storage.getItem(keyName);
                            if ((jsonInfo !== null) && !skipKey) {
                                let item = KeyValueCacheStructure.unserialize(jsonInfo);
                                if (toCleanLength < CLEAN_RECORDS) {
                                    list2Clean[keyName] = item.hits;
                                    toCleanLength++;
                                    if ((minKeyName == '') || (minHits > item.hits)) {
                                        minKeyName = keyName;
                                        minHits = item.hits;
                                    }
                                    if (item.hits > 0) {
                                        cleanOnlyEmpty = false;
                                    }
                                } else if (cleanOnlyEmpty && (toCleanLength >= CLEAN_RECORDS)) {
                                    break;
                                } else if (item.hits < minHits) {
                                    delete list2Clean[minKeyName];
                                    list2Clean[keyName] = item.hits;
                                    if (item.hits > 0) {
                                        cleanOnlyEmpty = false;
                                    }
                                }
                            }
                        }
                    }

                    let depends: Promise<any>[] = [];
                    for (let kName in list2Clean) {
                        if (list2Clean.hasOwnProperty(kName)) {
                            let name = kName.substring(AbstractKeyValueStorage.DATA_PREFIX.length);
                            depends.push(this.remove(name));
                        }
                    }

                    Promise.all(depends).then(() => {
                        this.maintenanceProcessing = false;
                        resolve();
                    }).catch(() => {
                        this.maintenanceProcessing = false;
                        resolve();
                    });
                }).catch(reject);
            } else {
                resolve();
            }
        });
    }

    /**
     * Генерирует имя для использования в качестве ключа
     *
     * @param args Аргументы участвующие в формировании ключа
     * @returns {string}
     */
    public getCacheKey(...args: any[]): string {
        let result = JSON.stringify(args);
        return String(Md5.hashStr(result));
    }

    /**
     * Установка значения в хранилище и помечание его тегами
     *
     * @param {string} key Ключ по которому надо сохранить значение
     * @param {KeyValueCacheStructure} item Структура элемента который надо сохранить
     * @returns {Promise<T>}
     */
    protected setInternal(key: string, item: KeyValueCacheStructure): Promise<any> {
        return new Promise((resolve, reject) => {
            let depends: Promise<any>[] = item.tags.map((tagKey) => {
                return new Promise((res, rej) => {
                    let realTagKey = this.prepareTag(tagKey);
                    this._get(realTagKey).then((tagValue) => {
                        let iTags: string[] = JSON.parse(tagValue);
                        let sIndex = iTags.indexOf(key);
                        if (sIndex === -1) {
                            iTags.push(key);
                            this._set(realTagKey, JSON.stringify(iTags)).then(res).catch(rej);
                        } else {
                            res();
                        }
                    }).catch(() => {
                        this._set(realTagKey, JSON.stringify([key])).then(res).catch(rej);
                    })
                });

            });
            depends.push(this._set(this.prepareKey(key), item.serialize()));
            Promise.all(depends).then(resolve).catch(reject);
        });
    }

    /**
     * Удаление значения по указанному ключу, помеченное переданными тегами.
     *      Удаляется ключ с его значением, из тегов которыми он был помечен удаляется этот ключ
     *          (если тег после удаления ключа становится пустым - он удаляется также)
     *
     * @param {string} key Ключ по которому надо удалить данные
     * @param {string[]} tags Массив тегов которыми помечена удаляемая запись
     * @returns {Promise<T>}
     */
    protected removeInternal(key: string, tags: string[]): Promise<any> {
        return new Promise((resolve, reject) => {
            let depends: Promise<any>[] = tags.map((tagKey) => {
                return new Promise((res, rej) => {
                    tagKey = this.prepareTag(tagKey);
                    this._get(tagKey)
                        .then((tagValue) => {
                            let iTags: string[] = JSON.parse(tagValue);
                            let sIndex = iTags.indexOf(key);
                            if (sIndex > -1) {
                                iTags.splice(sIndex, 1);
                                if (iTags.length > 0) {
                                    this._set(tagKey, JSON.stringify(iTags)).then(res).catch(rej);
                                } else {
                                    this._remove(tagKey).then(res).catch(rej);
                                }
                            } else {
                                rej();
                            }
                        })
                        .catch(rej);
                });
            });
            depends.push(this._remove(this.prepareKey(key)));
            Promise.all(depends).then(resolve).catch(reject);
        });
    }

    /**
     * Формирует имя тега для сохранения в хранилище
     *
     * @param {string} tag Оригинальное имя тега (которое указал программист)
     * @returns {string}
     */
    protected prepareTag(tag: string): string {
        return AbstractKeyValueStorage.TAGS_PREFIX + tag.trim().toLowerCase();
    };

    /**
     * Формирует имя ключа для сохранения в хранилище
     *
     * @param {string} key Оригинальное имя ключа (которое указал программист)
     * @returns {string}
     */
    protected prepareKey(key: string): string {
        return AbstractKeyValueStorage.DATA_PREFIX + key.trim().toLowerCase();
    }

    protected abstract _set(key: string, value: any): Promise<any>;

    protected abstract _get(key: string): Promise<any>;

    protected abstract _remove(key: string): Promise<any>;

    protected abstract _clear(): Promise<any>;

    protected abstract _getStorageInstance(): Promise<any>;
}