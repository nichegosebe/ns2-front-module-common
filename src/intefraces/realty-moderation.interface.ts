// Интерфейс модерации
export interface RealtyModerationInterface {
    // Дата создания запроса
    created: number;

    // Дата ответа на модерацию
    decision_date: number;

    // Guid запроса
    guid: string;

    // Guid объявления
    realty_guid: string;

    // Статус модерации
    status: string;

    // Комментарий отклонения
    reason: string | null;
}
