import {MESSAGE_RCPT_GROUP} from "./abstract/const";
import {AbstractForwardMessage} from "./abstract/forward/abstract-forward-message.model";

/**
 * Модель пересылки сообщения в группу
 */
export class ForwardMessageGroup extends AbstractForwardMessage {
    constructor() {
        super();
        this.header.rcpt = MESSAGE_RCPT_GROUP;
    }
}