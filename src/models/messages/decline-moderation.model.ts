import { AbstractMessage } from './abstract/abstract-message.model';
import {
    MESSAGE_HEADER_TYPE_MODERATION,
    MESSAGE_HEADER_ACTION_DECLINE
} from './abstract/const';

// Модель уведомления об отклоненной модерации профиля
export class DeclineModeration extends AbstractMessage {
    payload: {
        operation_guid: string,
        reason: string
    };

    constructor() {
        super();
        this.header.type = MESSAGE_HEADER_TYPE_MODERATION;
        this.header.action = MESSAGE_HEADER_ACTION_DECLINE;
    }
}
