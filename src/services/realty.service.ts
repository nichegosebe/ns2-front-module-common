import { Injectable, Inject } from "@angular/core";
import { Response, URLSearchParams } from "@angular/http";

// Services
import { HttpService } from "./http.service";
import { CurrentUserService } from "./current-user.service";

// Urls
import { RealtyUrls } from '../urls/realty-urls.model'

// Models
import { ResponseListModel } from "../models/response-list.model";

// Interfaces
import { IRealty } from "../intefraces/realty.interface";
import { IResponse } from "../intefraces/response.interface";
import { StorageInterface } from "../intefraces/storage.interface";

// RxJs
import { Subscription } from "rxjs/Subscription";

// Libs
import Rxmq from "rxmq";

@Injectable()
export class RealtyService {
    public static readonly LAST_MODIFIED_INFO_KEY = 'realty_forms_last_modified_info';
    public static readonly FULL_FORM_METADATA_KEY = 'full_form_metadata_info';

    public static ESB_REALTY_CHANNEL_NAME = 'realty';
    public static ESB_REALTY_FAVORITES_CHANGE = 'favorite.change';
    public static ESB_REALTY_PUBLISHED_CHANGE = 'published.change';
    public static ESB_REALTY_SALE_PUBLISHED_CHANGE = 'sale_published.change';
    public static ESB_REALTY_REMOVE = 'remove';

    private realtyChannel: any = Rxmq.channel(RealtyService.ESB_REALTY_CHANNEL_NAME);

    constructor(private httpService: HttpService,
        private currentUser: CurrentUserService,
        @Inject('StorageInterface') private cache: StorageInterface) {

        this.currentUser.onGeoChange((geo: any) => {
            geo = geo || {};
            this.cache.removeByTag(geo.oldGeoGuid || '');
        });

    }

    public getAllFormMetadata(): Promise<any> {
        return new Promise((resolve, reject) => {

            const globalCacheKey = this.cache.getCacheKey(RealtyService.FULL_FORM_METADATA_KEY);

            this.cache.get(globalCacheKey)
                .then((data) => {
                    resolve(data);
                })
                .catch(() => {
                    const url = RealtyUrls.metaFull;

                    this.httpService.get(url)
                        .map((response: Response) => response.json())
                        .map((response: any) => response.data)
                        .subscribe((data) => {
                            let allTagsObject: any = {},
                                depends: Promise<any>[] = [],
                                el: any,
                                tags: string[],
                                cacheKey: string;
                            for (let i = 0; i < data.length; i++) {
                                el = data[i];
                                tags = [el.action_guid, el.entity_guid, el.action_guid + '_' + el.entity_guid, this.currentUser.getInfo().session.geo_guid];
                                cacheKey = this.cache.getCacheKey(el.action_guid, el.entity_guid);
                                for (let i = 0; i < tags.length; i++) {
                                    if (!allTagsObject.hasOwnProperty(tags[i])) {
                                        allTagsObject[tags[i]] = true;
                                    }
                                }
                                depends.push(this.cache.set(cacheKey, el.meta, 0, tags));
                            }
                            let allTags = Object.keys(allTagsObject);
                            allTags.push(this.currentUser.getInfo().session.geo_guid);
                            depends.push(this.cache.set(globalCacheKey, data, 0, allTags));
                            Promise.all(depends)
                                .then(() => {
                                    resolve(data);
                                })
                                .catch((err) => {
                                    reject(err);
                                });
                        }, reject);
                });
        });
    }

    /**
     * Получить метаданные для формы фильтрации объявлений
     *
     * @param actionGuid тип действия/сделки (куплю, продам, сниму и сдам)
     */
    public getFilterMetaData(sessionGeoGuid: string): Promise<any> {
        const params = { currentUserSessionGeoGuid: sessionGeoGuid };
        const url = RealtyUrls.getFiltersFullMeta;

        return new Promise((resolve, reject) => {
            this.httpService.post(url, params)
                .map((response: Response) => response.json())
                .map((response: any) => response.data)
                .subscribe((data) => {
                    resolve(data);
                }, reject);
        });
    }

    /**
     * Создать объявление
     *
     * @param {string} actionGuid тип сделки
     * @param {string} entityGuid тип объекта недвижимости
     * @param data данные для сохранения
     * @returns {Promise<any>}
     */
    public create(action: string, entity: string, data: any): Promise<any> {
        return new Promise((resolve, reject) => {
            const url = RealtyUrls.create(action, entity);

            this.httpService.post(url, data)
                .map((response: Response) => response.json())
                .map((response: any) => response.data)
                .subscribe(resolve, reject);
        });
    }

    /**
     * Обновить объявление
     * @param {string} actionGuid Тип сделки
     * @param {string} entityGuid Тип объекта недвижимости
     * @param {string} guid Идентификатор объявления
     * @param data Данные объявления
     * @returns {Promise<any>}
     */
    public update(action: string, entity: string, guid: string, data: any): Promise<any> {
        return new Promise((resolve, reject) => {
            const url = RealtyUrls.update(action, entity, guid);

            this.httpService.post(url, data)
                .map((response: Response) => response.json())
                .map((response: any) => response.data)
                .subscribe(resolve, reject);
        });
    }

    /**
     * Возвращает список объявлений
     * @param body Тело запроса (@todo: разработать формат для передачи фильтров и передавать эту модель)
     * @param query Параметры запроса
     * @returns {Promise<ResponseListModel<any>>}
     */
    public filter(body: any, query?: any): Promise<ResponseListModel<any>> {
        return this.loadList(RealtyUrls.filter, body, query);
    }

    /**
     * Возвращает список объявлений пользователя
     * @param body Тело запроса (@todo: разработать формат для передачи фильтров и передавать эту модель)
     * @param query Параметры запроса
     * @returns {Promise<ResponseListModel<any>>}
     */
    public getMyRealty(body: any, query?: any): Promise<ResponseListModel<any>> {
        return this.loadList(RealtyUrls.myList, body, query);
    }

    /**
     * Возвращает список объявлений добавленных текущим пользователем в избранное
     * @param body Тело запроса (@todo: разработать формат для передачи фильтров и передавать эту модель)
     * @param query Параметры запроса
     * @returns {Promise<ResponseListModel<any>>}
     */
    public getFavoritesRealty(body: any, query?: any): Promise<ResponseListModel<any>> {
        return this.loadList(RealtyUrls.favorites, body, query);
    }

    /**
     * Публикует объявление
     * @param actionGuid Идентификатор действия
     * @param entityGuid Идентификатор типа недвижимости
     * @param guid Идентификатор объявления
     */
    public publish(actionGuid: string, entityGuid: string, guid: string): Promise<any> {
        return new Promise((resolve, reject) => {
            const url = RealtyUrls.publish(actionGuid, entityGuid, guid);

            this.httpService.put(url, {})
                .map((response: Response) => response.json())
                .map((response: any) => response.data)
                .subscribe(
                    (data) => {
                        this.realtyChannel.subject(RealtyService.ESB_REALTY_PUBLISHED_CHANGE).next({
                            actionGuid: actionGuid,
                            entityGuid: entityGuid,
                            guid: guid,
                            state: true
                        });
                        resolve(data);
                    },
                    reject
                );
        });
    }

    /**
     * Снимает объявление с публикации
     * @param actionGuid Идентификатор действия
     * @param entityGuid Идентификатор типа недвижимости
     * @param guid Идентификатор объявления
     */
    public unPublish(actionGuid: string, entityGuid: string, guid: string): Promise<any> {
        return new Promise((resolve, reject) => {
            const url = RealtyUrls.publish(actionGuid, entityGuid, guid);

            this.httpService.delete(url)
                .map((response: Response) => response.json())
                .map((response: any) => response.data)
                .subscribe(
                    (data) => {
                        this.realtyChannel.subject(RealtyService.ESB_REALTY_PUBLISHED_CHANGE).next({
                            actionGuid: actionGuid,
                            entityGuid: entityGuid,
                            guid: guid,
                            state: false
                        });
                        resolve(data);
                    },
                    reject
                );
        });
    }

    /**
     * Добавляет подписчика на публикацию/снятие с публикации объявления
     * @param {Function} then Функция-подписчик
     * @returns {Subscription}
     */
    public onPublishedChange(then: (info: { actionGuid: string, entityGuid: boolean, guid: string, state: boolean }) => void): Subscription {
        return this.realtyChannel.observe(RealtyService.ESB_REALTY_PUBLISHED_CHANGE).subscribe(then);
    }

    public onSalePublishedChange(then: (info: { actionGuid: string, entityGuid: boolean, guid: string, state: boolean }) => void): Subscription {
        return this.realtyChannel.observe(RealtyService.ESB_REALTY_SALE_PUBLISHED_CHANGE).subscribe(then);
    }

    /**
     * Удаляет объявление
     * @param actionGuid Идентификатор действия
     * @param entityGuid Идентификатор типа недвижимости
     * @param guid Идентификатор объявления
     */
    public remove(actionGuid: string, entityGuid: string, guid: string): Promise<any> {
        return new Promise((resolve, reject) => {
            const url = RealtyUrls.remove(actionGuid, entityGuid, guid);

            this.httpService.delete(url)
                .map((response: Response) => response.json())
                .map((response: any) => response.data)
                .subscribe(
                    (data) => {
                        this.realtyChannel.subject(RealtyService.ESB_REALTY_REMOVE).next({
                            actionGuid: actionGuid,
                            entityGuid: entityGuid,
                            guid: guid
                        });
                        resolve(data);
                    },
                    reject
                );
        });
    }

    /**
     * Добавляет подписчика на удаление объявления
     * @param {Function} then Функция-подписчик
     * @returns {Subscription}
     */
    public onRemove(then: (info: { actionGuid: string, entityGuid: boolean, guid: string }) => void): Subscription {
        return this.realtyChannel.observe(RealtyService.ESB_REALTY_REMOVE).subscribe(then);
    }

    /**
     * Добавляет объявление в избранное текущего пользователя
     *
     * @param {string} realtyGuid Идентификатор объявления
     * @returns {Promise<ResponseListModel<any>>}
     */
    public addRealtyToFavorite(realtyGuid: string): Promise<ResponseListModel<any>> {
        return new Promise((resolve, reject) => {
            const url = RealtyUrls.favorite(realtyGuid);

            this.httpService.post(url, null)
                .map((response: Response) => response.json())
                .map((response: any) => response.data)
                .subscribe(
                    (data) => {
                        this.realtyChannel.subject(RealtyService.ESB_REALTY_FAVORITES_CHANGE).next({
                            guid: realtyGuid,
                            state: true
                        });
                        resolve(data);
                    },
                    reject
                );
        });
    }

    /**
     * Удалляет объявление из избранного текущего пользователя
     *
     * @param {string} realtyGuid Идентификатор объявления
     * @returns {Promise<ResponseListModel<any>>}
     */
    public removeRealtyFromFavorite(realtyGuid: string): Promise<ResponseListModel<any>> {
        return new Promise((resolve, reject) => {
            const url = RealtyUrls.favorite(realtyGuid);

            this.httpService.delete(url)
                .map((response: Response) => response.json())
                .map((response: any) => response.data)
                .subscribe(
                    (data) => {
                        this.realtyChannel.subject(RealtyService.ESB_REALTY_FAVORITES_CHANGE).next({
                            guid: realtyGuid,
                            state: false
                        });
                        resolve(data);
                    },
                    reject
                );
        });
    }

    /**
     * Добавляет подписчика на добавление/удаление объявления из избранного
     * @param {Function} then Функция-подписчик
     * @returns {Subscription}
     */
    public onFavoriteChange(then: (info: { guid: string, state: boolean }) => void): Subscription {
        return this.realtyChannel.observe(RealtyService.ESB_REALTY_FAVORITES_CHANGE).subscribe(then);
    }

    /**
     * Возвращает данные о указанном объявлении
     * @param guid Идентификатор объявления
     * @returns {Promise<any>}
     */
    public load(guid: string): Promise<any> {
        return new Promise((resolve, reject) => {
            let url = RealtyUrls.about(guid);

            this.httpService.get(url)
                .map((response: Response) => response.json())
                .map((response: any) => response.data)
                .subscribe(resolve, reject);
        });
    }

    /**
     * Синхронизирует метаданные формы с бэкендом
     * @returns {Promise<any>}
     */
    public synchronizeFormMetaInfo(): Promise<any> {
        return new Promise((resolve) => {
            let url = RealtyUrls.getFormLastModified;

            this.httpService.get(url)
                .map((response: Response) => response.json())
                .map((response: any) => response.data)
                .subscribe(
                    (data: any) => {
                        let process = (localInfo: any) => {
                            this.processFormMetaInfo(localInfo, data).then(resolve).catch(resolve);
                        };
                        this.cache.get(RealtyService.LAST_MODIFIED_INFO_KEY)
                            .then((localInfo) => {
                                process(localInfo);
                            })
                            .catch(() => {
                                process({});
                            });
                    },
                    () => {
                        resolve();
                    }
                )
        });
    }

    /**
     * Получение объявлений по списку идентификаторов
     * @param {string[]} realtyGuids
     * @returns {Promise<any>}
     */
    public getRealtyByGuids(realtyGuids: string[]): Promise<any> {
        return new Promise((resolve, reject) => {
            const url = RealtyUrls.getByGuids;

            this.httpService.post(url, realtyGuids)
                .map((response: Response) => response.json())
                .map((response): any => response.data)
                .subscribe(resolve, reject);
        });
    }

    private loadList(url: string, body: any, query?: any): Promise<ResponseListModel<any>> {
        return new Promise((resolve, reject) => {
            let params = new URLSearchParams();

            if (query) {
                let keys = Object.keys(query);

                for (let i = 0; i < keys.length; i++) {
                    let key = keys[i];
                    params.set(key, query[key]);
                }
            }

            this.httpService.post(url, body, { params: params })
                .map((response: Response) => response.json())
                .map((response: any) => response.data)
                .subscribe(resolve, reject);
        })
    }

    /**
     * Сравнивает локальные метаданные и удаленные.
     *      При обнаружении измененных, добавленных или удаленных метаданных - удаляет все записи из кеша относящиеся к этим метаданным
     *
     * @param {any} localInfo Объект с локальными метаданными, где ключ - результат конкатенации actionGuid & entityGuid, значение - timestamp последнего изменения
     * @param {any[]} remoteInfo Массив объектов метаинформации на сервере
     * @returns {Promise<[]>}
     */
    private processFormMetaInfo(localInfo: any, remoteInfo: any[]): Promise<any> {
        let depends: Promise<any>[] = [];
        let resultLocalInfo: any = {};
        let updateLocalInfo: boolean = false;
        localInfo = localInfo || {};

        // Проверяем измененные и добавленные записи
        for (let remoteItem of remoteInfo) {
            remoteItem = remoteItem || {};
            let remoteLastModified = remoteItem.updated || 0;
            let actionGuid = remoteItem.action_guid || '';
            let entityGuid = remoteItem.entity_guid || '';

            let localKey = actionGuid + '_' + entityGuid;

            if (!localInfo.hasOwnProperty(localKey)) {
                updateLocalInfo = true;
            } else if (localInfo.hasOwnProperty(localKey) && ((localInfo[localKey] < remoteLastModified) || (remoteLastModified == 0))) {
                depends.push(this.cache.removeByTag(localKey));
                updateLocalInfo = true;
            }
            resultLocalInfo[localKey] = remoteLastModified;
        }

        // Проверяем удаленные записи
        let localKeys = Object.keys(localInfo);
        for (let i = 0; i < localKeys.length; i++) {
            let localKey = localKeys[i];
            if (!resultLocalInfo.hasOwnProperty(localKey)) {
                depends.push(this.cache.removeByTag(localKey));
                updateLocalInfo = true;
            }
        }
        if (updateLocalInfo) {
            depends.push(this.cache.set(RealtyService.LAST_MODIFIED_INFO_KEY, resultLocalInfo));
        }
        return Promise.all(depends);
    }

    // Публикация объявления на продающем сайте
    public salePublish(actionGuid: string, entityGuid: string, guid: string): Promise<IRealty> {
        return new Promise((resolve, reject) => {
            const url = RealtyUrls.salePublish(actionGuid, entityGuid, guid);

            this.httpService.put(url, null)
                .map((response: Response) => response.json())
                .map((response: IResponse<IRealty>) => response.data)
                .subscribe(
                    (data: IRealty) => {
                        this.realtyChannel.subject(RealtyService.ESB_REALTY_SALE_PUBLISHED_CHANGE).next({
                            actionGuid: actionGuid,
                            entityGuid: entityGuid,
                            guid: guid,
                            state: true
                        });
                        resolve(data);
                    },
                    reject
                );
        });
    }

    // Снятие публикации объявления с продающего сайта
    public saleUnPublish(actionGuid: string, entityGuid: string, guid: string): Promise<IRealty> {
        return new Promise((resolve, reject) => {
            const url = RealtyUrls.saleUnPublish(actionGuid, entityGuid, guid);

            this.httpService.delete(url)
                .map((response: Response) => response.json())
                .map((response: IResponse<IRealty>) => response.data)
                .subscribe(
                    (data: IRealty) => {
                        this.realtyChannel.subject(RealtyService.ESB_REALTY_SALE_PUBLISHED_CHANGE).next({
                            actionGuid: actionGuid,
                            entityGuid: entityGuid,
                            guid: guid,
                            state: false
                        });
                        resolve(data);
                    },
                    reject
                );
        });
    }

    // Отмена модерации объявления на продающем сайте
    public cancelSaleModeration(actionGuid: string, entityGuid: string, guid: string): Promise<IRealty> {
        return new Promise((resolve, reject) => {
            const url = RealtyUrls.cancelSaleModeration(actionGuid, entityGuid, guid);

            this.httpService.delete(url)
                .map((response: Response) => response.json())
                .map((response: IResponse<IRealty>) => response.data)
                .subscribe(
                    (data: IRealty) => {
                        this.realtyChannel.subject(RealtyService.ESB_REALTY_SALE_PUBLISHED_CHANGE).next({
                            actionGuid: actionGuid,
                            entityGuid: entityGuid,
                            guid: guid,
                            state: false
                        });
                        resolve(data);
                    },
                    reject
                );
        });
    }
}
