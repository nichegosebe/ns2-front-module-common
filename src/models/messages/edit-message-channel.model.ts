import {MESSAGE_RCPT_CHANNEL} from "./abstract/const";
import {AbstractEditMessage} from "./abstract/edit/abstract-edit-message.model";

/**
 * Модель редактирования сообщения в канале
 */
export class EditMessageChannel extends AbstractEditMessage {
    constructor() {
        super();
        this.header.rcpt = MESSAGE_RCPT_CHANNEL;
    }
}