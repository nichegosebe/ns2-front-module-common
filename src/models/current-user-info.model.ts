import {CurrentUserInfoInterface} from "../intefraces/current-user-info.interface";
import {RealtorModel} from "./realtor.model";

export class CurrentUserInfoModel extends RealtorModel implements CurrentUserInfoInterface {
    is_confirmed: boolean;
    roles: Array<{guid: string; name: string; title: string; description: string}>;
    token: {guid: string; user_guid: string; expired_at: string};
    session: {geo_guid: string};
    reject_reason: string;
}
