import {MessagePayload} from "../messages-payload.model";

/**
 * Модель полезной нагрузки сообщения: удаление сообщения
 */
export class DeleteMessagePayload extends MessagePayload {
    message: {
        guid: string,
        created?: number,
        updated?: number
    }
}