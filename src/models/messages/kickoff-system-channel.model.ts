import {AbstractMessage} from "./abstract/abstract-message.model";
import {
    MESSAGE_HEADER_TYPE_SYSTEM,
    MESSAGE_HEADER_ACTION_KICKOFF,
    MESSAGE_RCPT_CHANNEL
} from "./abstract/const";

/**
 * Модель уведомления о изгнании пользователя из канала
 */
export class KickoffSystemChannel extends AbstractMessage {
    payload: {
        from: string,
        to: string,
        kickedUserGuid: string
    };

    constructor() {
        super();
        this.header.type = MESSAGE_HEADER_TYPE_SYSTEM;
        this.header.action = MESSAGE_HEADER_ACTION_KICKOFF;
        this.header.rcpt = MESSAGE_RCPT_CHANNEL;
    }
}