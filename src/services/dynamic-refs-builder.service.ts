import { Inject, Injectable } from "@angular/core";

import { StorageInterface } from "../intefraces/storage.interface";

import { HttpService } from "./http.service";
import { DynamicRefsService } from "./dynamic-refs.service";

/**
 * Сервис строителя динамических справочников.
 */
@Injectable()
export class DynamicRefsBuilderService {

    /**
     * Хранит список инстансов сервисов динамических справочников
     *
     * @type {{}}
     */
    private services: {[refName: string]: DynamicRefsService} = {};

    /**
     * End point для сервиса refs
     */
    protected refsEndPoint: string;

    constructor(protected httpService: HttpService, @Inject('StorageInterface') private cache: StorageInterface) {}

    /**
     * Устновка end point для справочников
     *
     * @param _refsEndPoint
     */
    public setRefsEndPoint(_refsEndPoint: string) {
        this.refsEndPoint = _refsEndPoint;
    }

    /**
     * Получить экземпляр динамического сервиса справочников.
     *
     * @param name имя справочника, например geo, spec
     * @returns {DynamicRefsService} экземпляр сервиса DynamicRefsService с инициализированными refName и endPoint
     */
    public getInstance(name: string): DynamicRefsService {
        if (!this.services.hasOwnProperty(name)) {
            const refService = new DynamicRefsService(this.httpService, this.cache);

            refService.refName = name;
            refService.setRefsEndPoint(this.refsEndPoint);

            this.services[name] = refService;
        }

        return this.services[name];
    }
}
