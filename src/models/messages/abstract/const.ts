export const MESSAGE_RCPT_DIRECT = 'direct';
export const MESSAGE_RCPT_CHANNEL = 'channel';
export const MESSAGE_RCPT_GROUP = 'group';
export const MESSAGE_RCPT_REALTY = 'realty';

export const MESSAGE_HEADER_TYPE_MESSAGE = 'message';
export const MESSAGE_HEADER_TYPE_SYSTEM = 'system';
export const MESSAGE_HEADER_TYPE_SYSTEM_USER = 'system.user';
export const MESSAGE_HEADER_TYPE_MODERATION = 'moderation';
export const MESSAGE_HEADER_TYPE_REALTY_MODERATION = 'realty.moderation';

export const MESSAGE_HEADER_ACTION_RULES = 'rules';
export const MESSAGE_HEADER_ACTION_UICTL = 'uiCtl';
export const MESSAGE_HEADER_ACTION_LOCK_USER = 'lockUser';
export const MESSAGE_HEADER_ACTION_LOCK_REALTY = 'lockRealty';
export const MESSAGE_HEADER_ACTION_INVITE = 'invite';
export const MESSAGE_HEADER_ACTION_STATUS = 'status';
export const MESSAGE_HEADER_ACTION_JOIN = 'join';
export const MESSAGE_HEADER_ACTION_LEAVE = 'leave';
export const MESSAGE_HEADER_ACTION_KICKOFF = 'kickoff';
export const MESSAGE_HEADER_ACTION_REMOVE = 'remove';
export const MESSAGE_HEADER_ACTION_ACCEPT = 'accept';
export const MESSAGE_HEADER_ACTION_DECLINE = 'decline';

export const MESSAGE_HEADER_ACTION_SEND = 'send';
export const MESSAGE_HEADER_ACTION_REPLY = 'reply';
export const MESSAGE_HEADER_ACTION_FORWARD = 'forward';
export const MESSAGE_HEADER_ACTION_EDIT = 'edit';
export const MESSAGE_HEADER_ACTION_DELETE = 'delete';
export const MESSAGE_HEADER_ACTION_SEEN = 'seen';
export const MESSAGE_HEADER_ACTION_TYPING = 'typing';

export const MESSAGE_HEADER_ACTION_REALTY_CREATE = 'create';
export const MESSAGE_HEADER_ACTION_REALTY_UPDATE = 'update';
export const MESSAGE_HEADER_ACTION_REALTY_DELETE = 'delete';
export const MESSAGE_HEADER_ACTION_REALTY_MODERATE = 'moderate';
