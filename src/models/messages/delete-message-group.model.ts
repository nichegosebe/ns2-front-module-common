import {MESSAGE_RCPT_GROUP} from "./abstract/const";
import {AbstractDeleteMessage} from "./abstract/delete/abstract-delete-message.model";

/**
 * Модель удаления сообщения из группы
 */
export class DeleteMessageGroup extends AbstractDeleteMessage {
    constructor() {
        super();
        this.header.rcpt = MESSAGE_RCPT_GROUP;
    }
}