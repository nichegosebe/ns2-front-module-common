/**
 * Модель для отправки данных на сервер для создания чата (группы\канала)
 */
export class ChatCreateInfoModel {
    guid?: string;         // Идентификатор записи
    title: string;         // Заголовок чата
    description?: string;  // Описание чата
    is_private?: boolean;  // Признак приватности чата
    rules?: string;        // Правила чата
    logo_guid?: string;    // Идентификатор логотипа
}
