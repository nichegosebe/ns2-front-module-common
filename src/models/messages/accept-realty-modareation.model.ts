import { AbstractMessage } from './abstract/abstract-message.model';
import {
    MESSAGE_HEADER_TYPE_REALTY_MODERATION,
    MESSAGE_HEADER_ACTION_ACCEPT
} from './abstract/const';

// Модель уведомления об успешной модерации объявления
export class AcceptRealtyModeration extends AbstractMessage {
    payload: {
        guid: string,
        id: string,
        moderation: {
            created: string,
            decision_date: string,
            guid: string,
            realty_guid: string,
            reason: string,
            status: string
        }
    };

    constructor() {
        super();
        this.header.type = MESSAGE_HEADER_TYPE_REALTY_MODERATION;
        this.header.action = MESSAGE_HEADER_ACTION_ACCEPT;
    }
}
