import Rxmq from "rxmq";
import {WebsocketService, WS_RXMQ_CHANNEL} from './websocket.service';
import {async} from "@angular/core/testing";
import Spy = jasmine.Spy;

describe('WebsocketService', () => {

  let wsService: WebsocketService;
  let channel: any;

  beforeEach(async(() => {
    wsService = new WebsocketService();
    channel = Rxmq.channel(WS_RXMQ_CHANNEL);
  }));

  it('Отправка сообщения в веб-сокет отправляет данные в канал RxMQ', () => {
    let expectedAction = 'test';
    let expectedData = {property: 'value'};
    let evDict: MessageEventInit = {
      data: JSON.stringify({action: expectedAction, data: expectedData})
    };
    let ev: MessageEvent = new MessageEvent('test', evDict);

    wsService.connect('ws://fake.org');
    wsService.getWebSocket().onmessage(ev);

    channel.subject(expectedAction).subscribe((data) => {
      expect(data).toEqual(expectedData);
    })
  });

});