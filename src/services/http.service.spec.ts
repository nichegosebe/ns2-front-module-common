import {TestBed, inject} from '@angular/core/testing';
import {async} from "@angular/core/testing";
import {MockBackend, MockConnection} from "@angular/http/testing";
import {HttpModule, RequestOptions} from "@angular/http";

import {HttpService} from "./http.service";
import {CurrentUserService} from "./current-user.service";
import {Injector} from "@angular/core";
import {SessionStorageService} from "./session-storage.service";
import {LocalStorageService} from "./local-storage.service";

describe('HttpService', () => {

  beforeEach(async(() => {

    TestBed.configureTestingModule({
      imports: [HttpModule],
      providers: [
        { provide: HttpService, useFactory: (injector: Injector, mockBackend: MockBackend, requestOptions: RequestOptions) => {
          return new HttpService(injector, mockBackend, requestOptions);
        }, deps: [Injector, MockBackend, RequestOptions] },
        MockBackend,
        SessionStorageService,
        LocalStorageService,
        {
          provide: CurrentUserService,
          useFactory: (injector: Injector, localStorage: LocalStorageService, sessionStorage: SessionStorageService) => {
            return new CurrentUserService(injector, localStorage, sessionStorage);
          },
          deps: [Injector, LocalStorageService, SessionStorageService]
        }
      ]
    });

  }));

  it('при отправке HTTP-запроса добавляется заголовок X-User-Token',
    inject([MockBackend, CurrentUserService, HttpService], (mockBackend, currentUser, httpService) => {
      const token = 'Token';

      mockBackend.connections.subscribe((conn: MockConnection) => {
        expect(conn.request.headers.get('X-User-Token')).toEqual(token);
      });

      currentUser.setToken(token);
      httpService.request('/path');
    })
  );

  it('при отправке GET HTTP-запроса добавляется заголовок X-User-Token',
    inject([MockBackend, CurrentUserService, HttpService], (mockBackend, currentUser, httpService) => {
      const token = 'Token';

      mockBackend.connections.subscribe((conn: MockConnection) => {
        expect(conn.request.headers.get('X-User-Token')).toEqual(token);
      });

      currentUser.setToken(token);
      httpService.get('/path');
    })
  );

  it('при отправке POST HTTP-запроса добавляется заголовок X-User-Token',
    inject([MockBackend, CurrentUserService, HttpService], (mockBackend, currentUser, httpService) => {
      const token = 'Token';

      mockBackend.connections.subscribe((conn: MockConnection) => {
        expect(conn.request.headers.get('X-User-Token')).toEqual(token);
      });

      currentUser.setToken(token);
      httpService.post('/path', {});
    })
  );

  it('при отправке PUT HTTP-запроса добавляется заголовок X-User-Token',
    inject([MockBackend, CurrentUserService, HttpService], (mockBackend, currentUser, httpService) => {
      const token = 'Token';

      mockBackend.connections.subscribe((conn: MockConnection) => {
        expect(conn.request.headers.get('X-User-Token')).toEqual(token);
      });

      currentUser.setToken(token);
      httpService.put('/path', {});
    })
  );

  it('при отправке PATCH HTTP-запроса добавляется заголовок X-User-Token',
    inject([MockBackend, CurrentUserService, HttpService], (mockBackend, currentUser, httpService) => {
      const token = 'Token';

      mockBackend.connections.subscribe((conn: MockConnection) => {
        expect(conn.request.headers.get('X-User-Token')).toEqual(token);
      });

      currentUser.setToken(token);
      httpService.patch('/path', {});
    })
  );

  it('при отправке DELETE HTTP-запроса добавляется заголовок X-User-Token',
    inject([MockBackend, CurrentUserService, HttpService], (mockBackend, currentUser, httpService) => {
      const token = 'Token';

      mockBackend.connections.subscribe((conn: MockConnection) => {
        expect(conn.request.headers.get('X-User-Token')).toEqual(token);
      });

      currentUser.setToken(token);
      httpService.delete('/path', {});
    })
  );

  it('при отправке OPTIONS HTTP-запроса добавляется заголовок X-User-Token',
    inject([MockBackend, CurrentUserService, HttpService], (mockBackend, currentUser, httpService) => {
      const token = 'Token';

      mockBackend.connections.subscribe((conn: MockConnection) => {
        expect(conn.request.headers.get('X-User-Token')).toEqual(token);
      });

      currentUser.setToken(token);
      httpService.options('/path', {});
    })
  );

  it('при отправке HEAD HTTP-запроса добавляется заголовок X-User-Token',
    inject([MockBackend, CurrentUserService, HttpService], (mockBackend, currentUser, httpService) => {
      const token = 'Token';

      mockBackend.connections.subscribe((conn: MockConnection) => {
        expect(conn.request.headers.get('X-User-Token')).toEqual(token);
      });

      currentUser.setToken(token);
      httpService.head('/path', {});
    })
  );

});