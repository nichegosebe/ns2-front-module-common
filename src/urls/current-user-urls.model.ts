// URL's для работы с текущим пользователем
import { BasicUrls } from './basic-urls.model';

// URl's для текущего пользователя
// @dynamic
export class CurrentUserUrls extends BasicUrls {
	// Данные пользователя
	public static get self(): string {
		return `${CurrentUserUrls.endPoint}/self`;
    }

    // Модерация
	public static get moderation(): string {
		return `${CurrentUserUrls.self}/moderate`;
	}

	// Выход из приложения
	public static get logout(): string {
		return `${CurrentUserUrls.endPoint}/self/logout`;
	}

	// Город сессии пользователя
	public static get geoSessionCity(): string {
		return `${CurrentUserUrls.endPoint}/self/geo`;
	}
}
