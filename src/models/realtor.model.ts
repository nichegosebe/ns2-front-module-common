import { RealtorInterface } from "../intefraces/realtor.interface";
import { PhoneInterface } from "../intefraces/phone.interface";
import { SocialModel } from './social.model';

export class RealtorModel implements RealtorInterface {
    guid: string = '';
    firstname: string = '';
    lastname: string = '';
    email: string = '';
    experience: number = 0;
    avatar_guid: string = '';
    geo_guid: string = '';
    description: string = '';
    is_online: boolean = false;
    phones: Array<PhoneInterface> = [];
    specializations: string[] = [];
    districts: string[] = [];
    is_partner?: boolean;
    realty_count: any;
    is_agency: boolean = false;
    agency_position: string = '';
    agency_title: string = '';
    can_work_other_city: boolean = false;
    birthday: string | number = 0;
    site_url: string = '';
    socials: SocialModel[] = [];

    // Ранг на английском
    rank: string = '';

    // Ранг на русском
    rank_title: string = '';

    // Информация для покупателя
    customer_info: string = '';

    // На модерации
    on_moderate: boolean = false;
}
