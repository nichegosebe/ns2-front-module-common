import {MESSAGE_RCPT_DIRECT} from "./abstract/const";
import {AbstractSeenMessage} from "./abstract/seen/abstract-seen-message.model";

/**
 * Модель уведомления о просмотре сообщения в чате
 */
export class SeenMessageDirect extends AbstractSeenMessage {
    constructor() {
        super();
        this.header.rcpt = MESSAGE_RCPT_DIRECT;
    }
}