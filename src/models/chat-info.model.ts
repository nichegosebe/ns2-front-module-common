/**
 * Модель чатов
 */
export class ChatInfoModel {
    created: string;            // Дата/Время создания
    updated: string;            // Дата/Время обновления
    creator_guid: string;       // Идентификатор создателя
    updater_guid: string;       // Идентификатор последнего редактора
    chat_guid: string;          // Идентификатор
    chat_type: string;          // Тип
    city_guid: string;          // Идентификатор города
    owner_guid: string;         // Идентификатор владельца
    logo_guid: string;          // Идентификатор логотипа
    title: string;              // Название
    description: string;        // Описание
    rules: string;              // Правила
    is_private: boolean;        // Является ли чат приватным
    count_members: number;      // Количество участников
    is_me_member: boolean;      // Является ли текущий пользователь участником чата
    count_unread: number;       // Количество непрочитанных сообщений
    companion: string;          // Идентификатор компаньона (только для диалогов)
    last_message: {             // Информация о последнем сообщении в чате
        guid: string;               // Идентикатор записи последнего сообщения
        created: string;            // Дата/Время создания
        updated: string;            // Дата/Время обновления
        creator_guid: string;       // Идентификатор создателя
        updater_guid: string;       // Идентификатор последнего редактора
        message_guid: string;       // Идентификатор сообщения
        payload: any;               // Содержимое сообщения
        is_delivered: boolean;      // Доставлено ли сообщение
        is_seen: boolean;           // Просмотрено ли сообщение
    }
}