import {ChatCreateInfoModel} from '../models/chat-create-info.model';
import {ResponseListModel} from '../models/response-list.model';
import {Observable} from 'rxjs/Rx';
import {MessageSearchResultModel} from '../models/message-search-result.model';
import {ChatMessageModel} from '../models/chat-message.model';
import {ChatInfoModel} from '../models/chat-info.model';
import {MemberInfoModel} from '../models/member-info.model';

/**
 * Интерфейс для работы с каналами и группами
 */
export interface ChatsServiceInterface {
    /**
     * Создание чата (канала\группы)
     * @param {ChatCreateInfoModel} chatData Информация для создания чата
     * @returns {Promise<ChatInfoModel>}
     */
    create(chatData: ChatCreateInfoModel): Promise<ChatInfoModel>;

    /**
     * Получение списка чатов пользователя (каналов\групп)
     * @param {boolean} onlyMy Показывать только мои чаты
     * @param {string} lastChatGuid Идентфиикатор последнего чата
     * @param {string} stopChatGuid Идентификатор чата, до которого получить информацию
     * @param {any} query Дополнительные параметры
     * @returns {Promise<ResponseListModel<ChatInfoModel>>}
     */
    getList(onlyMy: boolean, lastChatGuid?: string, stopChatGuid?: string, query?: any): Promise<ResponseListModel<ChatInfoModel>>;

    /**
     * Просмотр информации о чате (канала группы)
     * @param {string} chatGuid Идентфикатор чата (канала\группы)
     * @returns {Promise<ChatInfoModel>}
     */
    getInfo(chatGuid: string): Promise<ChatInfoModel>;

    /**
     * Получение информации о статусе пользователей группы\канала
     * @param  {string} chatGuid Идентификатор чата
     * @param  {string[]} userGuids Массив идентификаторов пользователей чата (группы\канала)
     * @return {Promise<MemberInfoModel[]>}
     */
    getMembersInfo(chatGuid: string, userGuids: string[]): Promise<MemberInfoModel[]>;

    /**
     * Обновление информации о чате (канале\группе)
     * @param {string} chatGuid Идентификатор чата (канала\группы)
     * @param {ChatCreateInfoModel} params Параметры для обновления чата (канала\группы)
     * @returns {Promise<ChatInfoModel>}
     */
    update(chatGuid: string, params: ChatCreateInfoModel): Promise<ChatInfoModel>;

    /**
     * Удаление чата (канала\группы)
     * @param {string} chatGuid Идентификатор чата (канала\группы)
     * @returns {Promise<boolean>}
     */
    remove(chatGuid: string): Promise<boolean>;

    /**
     * Присоединиться к публичному чату (канала\группы)
     * @param {string} chatGuid Идентификатор чата (канала\группы)
     * @returns {Promise<boolean>}
     */
    join(chatGuid: string): Promise<boolean>;

    /**
     * Выход из чата (канала\группы)
     * @param {string} chatGuid Идентификатор чата (канала\группы)
     * @returns {Promise<boolean>}
     */
    leave(chatGuid: string): Promise<boolean>;

    /**
     * Принять приглашение на вступление в чат
     * @param {string} inviteGuid Идентификатор приглашения
     */
    accept(inviteGuid: string): Promise<boolean>;

    /**
     * Отклонить приглашение на вступление в чат
     * @param {string} inviteGuid Идентификатор приглашения
     */
    refuse(inviteGuid: string): Promise<boolean>;

    /**
     * Отправление приглашения вступления в чат (канал\группу)
     * @param {string} chatGuid Идентификатор чата, в которых приглашают
     * @param {string} userGuid Идентификатор пользователя, которого приглашают
     * @returns {Promise<any>}
     */
    invite(chatGuid: string, userGuid: string): Promise<MemberInfoModel>;

    /**
     * Поиск сообщений в чате
     * @param {string} chatGuid Идентификтаор чата
     * @param {string} query Строка поиска
     * @param {string} messageGuid Идентификатор сообщения до которого необходимо искать
     * @returns {Observable<MessageSearchResultModel>}
     */
    searchMessages(chatGuid: string, query: string, messageGuid?: string): Observable<MessageSearchResultModel>;

    /**
     * Поиск по чатам
     * @param {string} queryText Текст для поиска
     * @param {any} query Дополнительные параметры запроса
     */
    search(queryText: string, query?: any): Promise<ResponseListModel<ChatInfoModel>>;

    /**
     * Получение истории сообщений
     * @param {string} chatGuid Идентификатор чата (канала\группы)
     * @param {string} messageGuid Идентификатор сообщения, от которого получать историю
     * @param {string} startMessageGuid Идентификатор сообщения, до которого получать историю
     * @param {number} numBefore Количество сообщений которые надо добавить до начального сообщения
     * @param {any} query Дополнительные параметры
     * @returns {Promise<ResponseListModel<ChatMessageModel>>}
     */
    getHistory(chatGuid: string, messageGuid?: string, startMessageGuid?: string, numBefore?: number, query?: any): Promise<ResponseListModel<ChatMessageModel>>;

    /**
     * Установка URL для сервиса сообщений
     * @param messagesEndPoint
     */
    setMessagesEndPoint(messagesEndPoint: string): void;

    /**
     * Поиск по участникам канала
     * @param channelGuid   Индификатор канала в по списку участников которой производим поиск
     * @param queryText     Текст по которому ищем
     * @param query         Значение по которому производим поиск участников
     */
    searchMembers(channelGuid: string, queryText: string, query?: any): Promise<ResponseListModel<string>>;

    /**
     * Удаление участника группы
     * @param {string} groupGuid
     * @param {string} userGuid
     * @returns {Promise<boolean>}
     */
    removeMember(groupGuid: string, userGuid: string): Promise<boolean>;

    /**
     * Получаем список участников группы
     * @param {string} groupGuid  Индификатор группы
     * @param query               Дополнительные параметры
     */
    getMembers(groupGuid: string, query?: any): Promise<ResponseListModel<MemberInfoModel>>;
}
