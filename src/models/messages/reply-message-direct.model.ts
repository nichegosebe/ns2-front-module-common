import {MESSAGE_RCPT_DIRECT} from "./abstract/const";
import {AbstractReplyMessage} from "./abstract/reply/abstract-reply-message.model";

/**
 * Модель ответа на сообщение в чате
 */
export class ReplyMessageDirect extends AbstractReplyMessage {
    constructor() {
        super();
        this.header.rcpt = MESSAGE_RCPT_DIRECT;
    }
}