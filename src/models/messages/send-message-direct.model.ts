import {AbstractSendMessage} from "./abstract/send/abstract-send-message.model";
import {MESSAGE_RCPT_DIRECT} from "./abstract/const";

/**
 * Модель отправки сообщения в чат
 */
export class SendMessageDirect extends AbstractSendMessage {
    constructor() {
        super();
        this.header.rcpt = MESSAGE_RCPT_DIRECT;
    }
}