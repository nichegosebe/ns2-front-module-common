import {MESSAGE_RCPT_GROUP} from "./abstract/const";
import {AbstractStatusSystemUser} from "./abstract/user_status/abstract-status-system-user.model";

/**
 * Модель сообщения смены пользователя в группе
 */
export class StatusSystemUserGroup extends AbstractStatusSystemUser {
    constructor() {
        super();
        this.header.rcpt = MESSAGE_RCPT_GROUP;
    }
}