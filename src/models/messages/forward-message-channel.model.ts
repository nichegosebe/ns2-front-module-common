import {MESSAGE_RCPT_CHANNEL} from "./abstract/const";
import {AbstractForwardMessage} from "./abstract/forward/abstract-forward-message.model";

/**
 * Модель пересылки сообщения в канал
 */
export class ForwardMessageChannel extends AbstractForwardMessage {
    constructor() {
        super();
        this.header.rcpt = MESSAGE_RCPT_CHANNEL;
    }
}