export default {
    entry: 'dist/index.js',
    dest: 'dist/bundles/common.umd.js',
    sourceMap: false,
    format: 'umd',
    context: true,
    moduleName: 'ng.common',
    external: [
        '@angular/core',
        '@angular/http',
        'rxjs/Rx',
        'rxjs/Observable',
        'rxjs/operator/map',
        'rxjs/operator/toPromise',
        'rxmq',
        'ts-md5/dist/md5'
    ],
    globals: {
        '@angular/core': 'ng.core',
        '@angular/http': 'ng.http',
        'rxmq': 'Rxmq',
        'rxjs/Observable': 'Rx',
        'rxjs/ReplaySubject': 'Rx',
        'rxjs/add/operator/map': 'Rx.Observable.prototype',
        'rxjs/add/operator/mergeMap': 'Rx.Observable.prototype',
        'rxjs/add/observable/fromEvent': 'Rx.Observable',
        'rxjs/add/observable/of': 'Rx.Observable'
    }
}
