import {AbstractMessage} from "../abstract-message.model";
import {MESSAGE_HEADER_TYPE_MESSAGE, MESSAGE_HEADER_ACTION_EDIT} from "../const";
import {EditMessagePayload} from "./edit-message-payload.model";

/**
 * Базовая модель редактирования сообщения
 */
export abstract class AbstractEditMessage extends AbstractMessage {
    payload: EditMessagePayload = new EditMessagePayload();

    constructor() {
        super();
        this.header.type = MESSAGE_HEADER_TYPE_MESSAGE;
        this.header.action = MESSAGE_HEADER_ACTION_EDIT;
    }
}