import {AbstractMessage} from "./abstract/abstract-message.model";
import {
    MESSAGE_HEADER_TYPE_SYSTEM,
    MESSAGE_HEADER_ACTION_INVITE,
    MESSAGE_RCPT_CHANNEL
} from "./abstract/const";

/**
 * Модель уведомления о приглашении в канал
 */
export class InviteSystemChannel extends AbstractMessage {
    payload: {
        from: string,
        to: string,
        channel: {
            guid: string,
            title: string,
            icon: string
        },
        inviter: {
            guid: string,
            name: string,
            avatar: string
        }
    };

    constructor() {
        super();
        this.header.type = MESSAGE_HEADER_TYPE_SYSTEM;
        this.header.action = MESSAGE_HEADER_ACTION_INVITE;
        this.header.rcpt = MESSAGE_RCPT_CHANNEL;
    }
}