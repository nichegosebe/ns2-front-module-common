import {Injectable} from "@angular/core";
import {HttpService} from "./http.service";
import {HttpErrorInterface} from "../intefraces/http-error.interface";
import {CurrentUserService} from "./current-user.service";

@Injectable()
export class StorageService {
    private storageEndPoint: string;

    constructor(private userService: CurrentUserService, private http: HttpService) {

    }

    public setStorageEndPoint(url: string) {
        this.storageEndPoint = url;
    }

    /**
     * Удаление файла со стораджа
     *
     * @param guid
     * @returns {Promise<T>}
     */
    public deleteFile(guid: string): Promise<any> {
        return new Promise((resolve, reject) => {
            let url = this.storageEndPoint + 'files/' + guid;

            this.http.delete(url).subscribe((data) => {
                resolve()
            }, (err: HttpErrorInterface) => {
                reject(err)
            });
        });
    }

    /**
     * Post file to storage
     *
     * @param file
     * @returns {Promise<T>}
     */
    public postFile(file: File): Promise<any> {
        return new Promise((resolve, reject) => {
            let url = this.storageEndPoint + 'files';
            let formData: FormData = new FormData();
            let xhr: XMLHttpRequest = new XMLHttpRequest();

            formData.append('data', file, file.name);

            xhr.onreadystatechange = () => {
                if (xhr.readyState === 4) {
                    try {
                        if (xhr.status === 200) {
                            resolve(JSON.parse(xhr.response));
                        } else {
                            const error = JSON.parse(xhr.response);
                            reject(error && error.data ? error.data : error);
                        }
                    } catch (err) {
                        reject(err);
                    }
                }
            };

            xhr.open('POST', url, true);
            xhr.send(formData);
        });
    }

    /**
     * Заменить вложение
     *
     * @param guid
     * @param file
     * @returns {Promise<T>}
     */
    public changeAttachment(guid: string, file: File): Promise<any> {
        return new Promise((resolve, reject) => {
            let url = this.storageEndPoint + 'files/' + guid + '/change';
            let formData: FormData = new FormData();
            let xhr: XMLHttpRequest = new XMLHttpRequest();

            formData.append('data', file, file.name);

            xhr.onreadystatechange = () => {
                if (xhr.readyState === 4) {
                    try {
                        if (xhr.status === 200) {
                            resolve(JSON.parse(xhr.response));
                        } else {
                            const error = JSON.parse(xhr.response);
                            reject(error && error.data ? error.data : error);
                        }
                    } catch (err) {
                        reject(err);
                    }
                }
            };

            xhr.open('POST', url, true);
            xhr.setRequestHeader(CurrentUserService.USER_TOKEN_NAME, this.userService.getToken());
            xhr.send(formData);
        });
    }

    /**
     * Возвращает безопасный стиль
     * для масштабированной картинки
     *
     * @param {string} imageGuid
     * @param {number} width
     * @param {number} height
     * @param {string} mode
     * @param {number} quality
     * @returns {String}
     */
    public getScaledImageUrl(imageGuid: string, width: number, height: number, mode: string = 'outbound', quality: number = 95): string {
        let url = this.storageEndPoint + 'images/scale/' + imageGuid + '/' + width + '/' + height;
        url += mode ? '/' + mode : '';
        url += quality ? '/' + quality : '';

        return url;
    }

    /**
     * Возвращает путь до картинки на storage
     *
     * @param {string} imageGuid
     * @returns {string}
     */
    public getImageUrl(imageGuid: string): string {
        return this.storageEndPoint + 'files/' + imageGuid;
    }
}
