import {Injectable} from "@angular/core";
import {IRealtyFilter} from "../intefraces/realty-filter.interface";
import {RealtyFilterOperation} from "../models/realty-filter-operation.model";
import {RealtyFilterLogicalOperation} from "../models/realty-filter-logical-operation.model";

/**
 * Класс для построения объектов фильтрации объявлений
 */
@Injectable()
export class RealtyFiltersBuilder {
    /**
     * Формирование части запроса ИЛИ.
     * Параметры метода будут использованы в запросе через оператор ИЛИ.
     * @param filter Компоненты фильтра
     * @returns {RealtyFilterLogicalOperation}
     */
    public or(filter: IRealtyFilter|IRealtyFilter[]): IRealtyFilter {
        return new RealtyFilterLogicalOperation('or', filter);
    }

    /**
     * Формирование части запроса И.
     * Параметры метода будут использованы в запросе через оператор И.
     * @param filter Компоненты фильтра
     * @returns {RealtyFilterLogicalOperation}
     */
    public and(filter: IRealtyFilter|IRealtyFilter[]): IRealtyFilter {
        return new RealtyFilterLogicalOperation('and', filter);
    }

    /**
     * Проверка на равенство указанного поля с переданным значением
     * @param field Имя поля
     * @param value Значение
     * @returns {RealtyFilterOperation}
     */
    public eq(field: string, value: number|string|boolean): IRealtyFilter {
        return new RealtyFilterOperation('eq', field, value);
    }

    /**
     * Проверка на неравенство указанного поля с переданным значением
     * @param field Имя поля
     * @param value Значение
     * @returns {RealtyFilterOperation}
     */
    public ne(field: string, value: number|string|boolean): IRealtyFilter {
        return new RealtyFilterOperation('ne', field, value);
    }

    /**
     * Проверка что значение указанного поля больше переданного значения
     * @param field Имя поля
     * @param value Значение
     * @returns {RealtyFilterOperation}
     */
    public gt(field: string, value: number): IRealtyFilter {
        return new RealtyFilterOperation('gt', field, value);
    }

    /**
     * Проверка что значение указанного поля больше или равно переданному значению
     * @param field Имя поля
     * @param value Значение
     * @returns {RealtyFilterOperation}
     */
    public gte(field: string, value: number): IRealtyFilter {
        return new RealtyFilterOperation('gte', field, value);
    }

    /**
     * Проверка что значение указанного поля меньше переданного значения
     * @param field Имя поля
     * @param value Значение
     * @returns {RealtyFilterOperation}
     */
    public lt(field: string, value: number): IRealtyFilter {
        return new RealtyFilterOperation('lt', field, value);
    }

    /**
     * Проверка что значение указанного поля меньше или равно переданному значению
     * @param field Имя поля
     * @param value Значение
     * @returns {RealtyFilterOperation}
     */
    public lte(field: string, value: number): IRealtyFilter {
        return new RealtyFilterOperation('lte', field, value);
    }

    /**
     * Проверка что значение указанного поля входит в массив переданных значений
     * @param field Имя поля
     * @param value Значение
     * @returns {RealtyFilterOperation}
     */
    public in(field: string, value: number[]|string[]|boolean[]): IRealtyFilter {
        return new RealtyFilterOperation('in', field, value);
    }

    /**
     * Проверка что значение указанного поля не входит в массив переданных значений
     * @param field Имя поля
     * @param value Значение
     * @returns {RealtyFilterOperation}
     */
    public nin(field: string, value: number[]|string[]|boolean[]): IRealtyFilter {
        return new RealtyFilterOperation('nin', field, value);
    }

    /**
     * Проверка что значение указанного поля(точки на карте) входит в прямоугольную область описываемую параметрами leftBottomPoint и rightTopPoint
     * @param field Имя поля
     * @param leftBottomPoint Координаты левой-нижней точки прямоугольника
     * @param rightTopPoint Координаты правой-верхней точки прямоугольника
     * @returns {RealtyFilterOperation}
     */
    public geoIn(field: string, leftBottomPoint: [number, number], rightTopPoint: [number, number]): IRealtyFilter {
        return new RealtyFilterOperation('geoin', field, [[leftBottomPoint[0], leftBottomPoint[1]], [rightTopPoint[0], rightTopPoint[1]]]);
    }

    /**
     * Проверка что значение поля входит в диапазон между minValue и maxValue (включая крайние точки)
     * @param field Имя поля
     * @param minValue Минимальное значение
     * @param maxValue Максимальное значение
     * @returns {RealtyFilterOperation}
     */
    public between(field: string, minValue: number|string, maxValue: number|string): IRealtyFilter {
        return new RealtyFilterOperation('between', field, [minValue, maxValue]);
    }

    /**
     * Проверка существования поля
     * @param field Имя поля
     * @param value Должно поле существовать или нет
     * @returns {RealtyFilterOperation}
     */
    public exists(field: string, value: boolean) {
        return new RealtyFilterOperation('exists', field, value);
    }
}