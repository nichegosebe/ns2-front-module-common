import {MessagePayload} from "../messages-payload.model";

/**
 * Модель полезной нагрузки сообщения: просмотр сообщения
 */
export class SeenMessagePayload extends MessagePayload {
    message: {
        guid: string,
        created?: number,
        updated?: number
    }
}