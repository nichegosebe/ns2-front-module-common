import {MESSAGE_RCPT_DIRECT} from "./abstract/const";
import {AbstractForwardMessage} from "./abstract/forward/abstract-forward-message.model";

/**
 * Модель пересылки сообщения в чат
 */
export class ForwardMessageDirect extends AbstractForwardMessage {
    constructor() {
        super();
        this.header.rcpt = MESSAGE_RCPT_DIRECT;
    }
}