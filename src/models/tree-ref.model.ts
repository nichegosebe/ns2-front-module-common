import {AbstractRefModel} from "./abstract-ref.model";
export abstract class TreeRefModel extends AbstractRefModel {
    pid: string;
    trail: string;
    level: number;
}