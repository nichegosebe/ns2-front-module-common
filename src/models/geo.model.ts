import {TreeRefModel} from "./tree-ref.model";
export class GeoModel extends TreeRefModel {
    phone_prefix: string;
    flag_guid: string;
    coordinate: string;
    visible: boolean;
}