// URl's для текущего пользователя
export { CurrentUserUrls } from './current-user-urls.model';

// Url`s для риэлторов
export { RealtorsUrls } from './realtors-urls.model';

// Url's для уведомлений
export { NotificationUrls } from './notification-urls.model';

// Url's для каналов
export { ChannelsUrls } from './channel-urls.model';

// Url's для групп
export { GroupsUrls } from './group-urls.model';

// Url`s для объявлений
export { RealtyUrls } from './realty-urls.model';
