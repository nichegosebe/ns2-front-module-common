import {MESSAGE_RCPT_DIRECT} from "./abstract/const";
import {AbstractDeleteMessage} from "./abstract/delete/abstract-delete-message.model";

/**
 * Модель удаления сообщения из чата
 */
export class DeleteMessageDirect extends AbstractDeleteMessage {
    constructor() {
        super();
        this.header.rcpt = MESSAGE_RCPT_DIRECT;
    }
}