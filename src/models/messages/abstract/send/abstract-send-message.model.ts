import {AbstractMessage} from "../abstract-message.model";
import {SendMessagePayload} from "./send-message-payload.model";
import {MESSAGE_HEADER_TYPE_MESSAGE, MESSAGE_HEADER_ACTION_SEND} from "../const";

/**
 * Базовая модель отправки сообщения
 */
export abstract class AbstractSendMessage extends AbstractMessage {
    payload: SendMessagePayload = new SendMessagePayload();

    constructor() {
        super();
        this.header.type = MESSAGE_HEADER_TYPE_MESSAGE;
        this.header.action = MESSAGE_HEADER_ACTION_SEND;
    }
}