import {RealtorInterface} from "./realtor.interface";

export interface CurrentUserInfoInterface extends RealtorInterface {
    is_confirmed: boolean;
    roles: Array<{
        guid: string;
        name: string;
        title: string;
        description: string
    }>;
    token: {
        guid: string;
        user_guid: string;
        expired_at: string
    },
    session: {
        geo_guid: string
    }

    reject_reason: string;
}
