import {Injectable, Inject} from "@angular/core";
import {HttpService} from "./http.service";
import {StorageInterface} from "../intefraces/storage.interface";
import {Response} from "@angular/http";

@Injectable()
/**
 * Класс для работы со справочниками
 */
export class RefsService {
    public static readonly REFS_LAST_MODIFIED_INFO_KEY = 'refs_last_modified_info';
    protected refsEndPoint: string;

    constructor(protected httpService: HttpService, @Inject('StorageInterface') private cache: StorageInterface) {
    }

    public setRefsEndPoint(_refsEndPoint: string) {
        this.refsEndPoint = _refsEndPoint;
    }

    /**
     * Синхронизирует справочники с бэкендом
     * @returns {Promise<T>}
     */
    public synchronize(): Promise<any> {
        return new Promise((resolve) => {
            let url = this.refsEndPoint + 'refs/last-modified';

            this.httpService.get(url)
                .map((response: Response) => response.json())
                .map((response: any) => response.data)
                .subscribe(
                    (data: any) => {
                        let process = (localInfo: any) => {
                            this.processLastModified(localInfo, data).then(resolve).catch(resolve);
                        };
                        this.cache.get(RefsService.REFS_LAST_MODIFIED_INFO_KEY)
                            .then((localInfo) => {
                                process(localInfo);
                            })
                            .catch(() => {
                                process({});
                            });
                    },
                    () => {
                        resolve();
                    }
                )
        });
    }

    /**
     * Сравнивает локальные справочники и удаленные.
     *      При обнаружении измененных, добавленных или удаленных справочников - удаляет все записи из кеша относящиеся к этим справочникам
     *
     * @param {any} localInfo Объект с локальными справочниками, где ключ - имя справочника, значение - timestamp последнего изменения
     * @param {any[]} remoteInfo Массив объектов с информацией о справочниках на сервере
     * @returns {Promise<[]>}
     */
    private processLastModified(localInfo: any, remoteInfo: any[]): Promise<any> {
        let depends: Promise<any>[] = [];
        let resultLocalInfo: any = {};
        let updateLocalInfo: boolean = false;
        localInfo = localInfo || {};

        // Проверяем измененные и добавленные справочники
        for (let remoteItem of remoteInfo) {
            remoteItem = remoteItem || {};
            let remoteLastModified = remoteItem.updated || 0;
            let refName = remoteItem.class_name || null;
            if (!localInfo.hasOwnProperty(refName)) {
                updateLocalInfo = true;
            } else if (localInfo.hasOwnProperty(refName) && (localInfo[refName] < remoteLastModified)) {
                depends.push(this.cache.removeByTag(refName));
                updateLocalInfo = true;
            }
            resultLocalInfo[refName] = remoteLastModified;
        }

        // Проверяем удаленные справочники
        let localKeys = Object.keys(localInfo);
        for (let i = 0; i < localKeys.length; i++) {
            let localRefName = localKeys[i];
            if (!resultLocalInfo.hasOwnProperty(localRefName)) {
                depends.push(this.cache.removeByTag(localRefName));
                updateLocalInfo = true;
            }
        }
        if (updateLocalInfo) {
            depends.push(this.cache.set(RefsService.REFS_LAST_MODIFIED_INFO_KEY, resultLocalInfo));
        }
        return Promise.all(depends);
    }
}