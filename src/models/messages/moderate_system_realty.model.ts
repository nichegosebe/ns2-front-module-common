import {AbstractMessage} from "./abstract/abstract-message.model";
import {MESSAGE_HEADER_TYPE_SYSTEM, MESSAGE_HEADER_ACTION_REALTY_MODERATE, MESSAGE_RCPT_REALTY} from "./abstract/const";

/**
 * Модель уведомления о создании объявления и отправке на модерации для публикации на НС
 */
export class ModerateSystemRealty extends AbstractMessage {
    payload: {
        guid: string
    };

    constructor() {
        super();
        this.header.type = MESSAGE_HEADER_TYPE_SYSTEM;
        this.header.action = MESSAGE_HEADER_ACTION_REALTY_MODERATE;
        this.header.rcpt = MESSAGE_RCPT_REALTY;
    }
}
