import { AbstractRefsService } from "./abstract-refs.service";

/**
 * Динамический справочник.
 *
 * Используется для простых операций с однотипными справочниками. Получать экземпляр сервиса удобно через
 * сервис DynamicRefsFactoryService:
 *
 * @Component({
 *  providers: [DynamicRefsFactoryService]
 * })
 * export class MyComponent {
 *  constructor(private refsFactory: DynamicRefsFactoryService) {}
 *
 *  onInit() {
 *    this.refsFactory.getInstance('geo').getAll({page: 1}).then((response) => {
 *      // process response
 *    }).catch((err) => {
 *      // process error
 *    });
 *  }
 * }
 *
 * Данный сервис нельзя получить через Injector.
 *
 * @see DynamicRefsBuilderService
 */
export class DynamicRefsService extends AbstractRefsService {
    refName: string;
}
