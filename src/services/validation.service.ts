import { Injectable } from "@angular/core";

@Injectable()
export class ValidationService {

    /**
     * Валидация email-адреса
     *
     * @param email
     * @returns {boolean}
     */
    public email(email: string): boolean {
        return /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i
            .test(email);
    }
}