import {MESSAGE_RCPT_DIRECT} from "./abstract/const";
import {AbstractStatusSystemUser} from "./abstract/user_status/abstract-status-system-user.model";

/**
 * Модель сообщения смены пользователя в чате
 */
export class StatusSystemUserDirect extends AbstractStatusSystemUser {
    constructor() {
        super();
        this.header.rcpt = MESSAGE_RCPT_DIRECT;
    }
}