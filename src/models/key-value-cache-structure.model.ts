/**
 * Модель структуры данных для кеширования в хранилищах
 */
export class KeyValueCacheStructure {
    private _expiredAt: number = 0;
    private _tags: string[] = [];
    private _value: any = null;
    private _hits: number = 0;

    constructor(expiration ?: number, _tags?: string[], _value?: any) {
        this.expiredAt = expiration ? (Math.floor((new Date()).getTime()) + expiration) : 0;
        this.tags = _tags || [];
        this.value = _value || null;
        this.hits = 0;
    }

    get expiredAt(): number {
        return this._expiredAt;
    }

    set expiredAt(value: number) {
        this._expiredAt = value;
    }

    get hits(): number {
        return this._hits;
    }

    set hits(value: number) {
        this._hits = value;
    }

    get tags(): string[] {
        return this._tags;
    }

    set tags(value: string[]) {
        this._tags = value;
    }

    get value(): any {
        return this._value;
    }

    set value(value: any) {
        this._value = value;
    }

    serialize(): string {
        let obj = {
            expiredAt: this.expiredAt,
            tags: this.tags,
            value: this.value,
            hits: this.hits
        };
        return JSON.stringify(obj);
    }

    isExpired(): boolean {
        return ((this.expiredAt > 0) && (this.expiredAt > Math.floor((new Date()).getTime())));
    }

    public static unserialize(json: string) {
        let obj = JSON.parse(json);
        let expiredAt = obj.expiredAt || 0;
        let tags = obj.tags || [];
        let value = obj.value || null;
        let instance: KeyValueCacheStructure = new KeyValueCacheStructure(expiredAt, tags, value);
        instance.hits = obj.hits || 0;
        return instance;
    }
}