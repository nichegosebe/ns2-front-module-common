import {AbstractMessage} from "../abstract-message.model";
import {MESSAGE_HEADER_TYPE_MESSAGE, MESSAGE_HEADER_ACTION_SEEN} from "../const";
import {SeenMessagePayload} from "./seen-message-payload.model";

/**
 * Базовая модель просмотра сообщения
 */
export abstract class AbstractSeenMessage extends AbstractMessage {
    payload: SeenMessagePayload = new SeenMessagePayload();

    constructor() {
        super();
        this.header.type = MESSAGE_HEADER_TYPE_MESSAGE;
        this.header.action = MESSAGE_HEADER_ACTION_SEEN;
    }
}